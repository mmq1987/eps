<!--[if (gt IE 9)|(gt IEMobile 7)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Exclusive Private Sale Inc-View Profile</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- http://davidbcalhoun.com/2010/viewport-metatag -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<!-- http://www.kylejlarson.com/blog/2012/iphone-5-web-design/ and http://darkforge.blogspot.fr/2010/05/customize-android-browser-scaling-with.html -->
	<meta name="viewport" content="user-scalable=0, initial-scale=1.0, target-densitydpi=115">
	<!-- For all browsers -->
	<link rel="stylesheet" href="http://gspedia.com/exclusiveprivatesale/css/reset.css?v=1">
	<link rel="stylesheet" href="http://gspedia.com/exclusiveprivatesale/css/style.css?v=1">

	<link rel="stylesheet" media="print" href="http://gspedia.com/exclusiveprivatesale/css/print.css?v=1">
	<!-- For progressively larger displays -->
	<link rel="stylesheet" media="only all and (min-width: 480px)" href="http://gspedia.com/exclusiveprivatesale/css/480.css?v=1">
	<link rel="stylesheet" media="only all and (min-width: 768px)" href="http://gspedia.com/exclusiveprivatesale/css/768.css?v=1">
	<link rel="stylesheet" media="only all and (min-width: 992px)" href="http://gspedia.com/exclusiveprivatesale/css/992.css?v=1">
	<link rel="stylesheet" media="only all and (min-width: 1200px)" href="http://gspedia.com/exclusiveprivatesale/css/1200.css?v=1">
<link rel="stylesheet" href="http://gspedia.com/exclusiveprivatesale/css/styles/form.css?v=1">
	<link rel="stylesheet" href="http://gspedia.com/exclusiveprivatesale/css/styles/switches.css?v=1">

    <link rel="stylesheet" href="http://gspedia.com/exclusiveprivatesale/css/styles/agenda.css">

    <div class="float-left margin-right margin-bottom">
						<h6>Expandable&nbsp;list<br>
						<small>For long values</small></h6>
						<p class="button-height">
							<select class="select expandable-list anthracite-gradient glossy" style="width:100px" tabindex="2">
								<option value="1">First very long value</option>
								<option value="2">Second value even longer than the first one</option>
								<option value="3" selected="selected">Expand</option>
								<option value="4" disabled>Disabled value</option>
							</select>
						</p>
					</div>