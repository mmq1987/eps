<?php
class Main_model extends CI_Model {
	public function __construct(){
		$this->load->database();
	}
    /*Function to send mails*/
   	public function email_send1($from_emailid, $subject, $email, $emaildata) {
   	    $this->load->library('email');
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $config['protocol'] = 'sendmail';
        $this->email->initialize($config);
        $this->email->from($from_emailid,'Exclusive Private Sale.Inc');
        $this->email->to($email);
        $this->email->subject($subject);
        $html_email = $this->load->view('email-views.php', $emaildata, true);
        $this->email->message($html_email);
        $this->email->send();
        return true;
	}
   public function email_send($from_emailid, $subject, $email, $emaildata) {
		$this -> load -> library('email');
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$config['protocol'] = 'sendmail';
		$this -> email -> initialize($config);
		$this -> email -> from($from_emailid, 'Exclusive Private Sale.Inc');
		$this -> email -> to($email);
		$this -> email -> subject($subject);
		$html_email = $this -> load -> view('email-views.php', $emaildata, true);
		$this -> email -> message($html_email);
		$this -> email -> send();
	}
  public function resize_image($pic,$loc,$new_name,$new_w,$new_h,$image_type)
	{
		if ($image_type == 'image/jpeg' || $image_type == 'image/jpg' || $image_type == 'image/pjpeg') {
		$im = imagecreatefromjpeg($pic);
		} elseif ($image_type == 'image/gif') {
		$im = imagecreatefromgif($pic);
		} elseif ($image_type == 'image/png' || $image_type == 'image/x-png') {
		$im = imagecreatefrompng($pic);
		}
		//$im = imagecreatefromjpeg($pic);
		$orwidth=imagesx($im); // get original width
		$orheight=imagesy($im); //get original height
		if($orwidth>$orheight)
		{
		if($orwidth>$new_w){
		$ratio = $orwidth/$orheight; // calc image ratio
		$new_h = round($new_w/$ratio,0); // calc new height keeping ratio of original
		}
		else{
		$new_w=$orwidth;
		$new_h=$orheight;
		}
		$new_image=ImageCreateTrueColor($new_w,$new_h);
		imagecopyresampled($new_image,$im,0,0,0,0,$new_w,$new_h,$orwidth,$orheight);
		imagejpeg($new_image,$loc.$new_name,100);
		imagedestroy($new_image);
		}
		else{
		if($orheight>$new_h){
		$ratio = $orheight/$orwidth; // calc image ratio
		$new_w = round($new_h/$ratio,0); // calc new height keeping ratio of original
		}
		else{
		$new_w=$orwidth;
		$new_h=$orheight;
		}
		$new_image=ImageCreateTrueColor($new_w,$new_h);
		imagecopyresampled($new_image,$im,0,0,0,0,$new_w,$new_h,$orwidth,$orheight);
		imagejpeg($new_image,$loc.$new_name,100);
		imagedestroy($new_image);
		}
    }
    public function user_data($user_id){
    	$this -> load -> helper('url');
    	$this -> db -> select('*');
    	$this -> db -> from('registration');
    	$this -> db -> where('registration_id = ' . "'" . $user_id . "'");
    	$result= $this -> db -> get();
    		if ($result -> num_rows() > 0) {
			$retrieved = $result -> result_array();
			return $retrieved;
		} else {
			return FALSE;
		}
    }
    /*Function to fetch user details*/
     public function alluserdetails()
     {
        $all_users_id='';
        $sql=("SELECT *
FROM registration
WHERE STATUS = 'VERIFIED' and usertype<> 'admin'
ORDER BY `usertype` = 'sub_admin',`usertype` = 'auto_brand', `usertype` = 'account_managers', `usertype` = 'dealership' , registration_id desc ");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
            $returnvalue= $query->result_array();
         return $returnvalue;
        }
        else
        {
            return FALSE;
        }
    }
    /*Function to fetch dealer details*/
     public function dealeruserdetails()
     {
        $all_users_id='';
        $sql=("SELECT *
FROM registration
WHERE STATUS = 'VERIFIED' and usertype='dealership'
ORDER BY registration_id desc ");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
            $returnvalue= $query->result_array();
         return $returnvalue;
        }
        else
        {
            return FALSE;
        }
    }
         public function sortuserdetails()
     {
        $member_user_type=$this->input->post('member_type');
        $condition='';
        if($member_user_type=='All')
        {
                $all_users_id='';
        $sql=("SELECT *
FROM registration
WHERE STATUS = 'VERIFIED' and usertype<> 'admin'
ORDER BY `usertype` = 'auto_brand', `usertype` = 'account_managers', `usertype` = 'dealership' ,registration_id desc ");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
            $returnvalue= $query->result_array();
         return $returnvalue;
        }
        else
        {
            return FALSE;
        }
        }
        else
        {
             if($member_user_type=='dealership')
            {
                $condition="and usertype='dealership'";
            }
            else if($member_user_type=='account_managers')
            {
                $condition="and usertype='account_managers'";
            }
             else if($member_user_type=='auto_brand')
            {
                $condition="and usertype='auto_brand'";
            }else if($member_user_type=='sub_admin'){
                $condition="and usertype='sub_admin'";
            }
            $sql=("SELECT *
            FROM registration
            WHERE STATUS = 'VERIFIED' and usertype<> 'admin'  $condition
            ORDER BY registration_id desc ");
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
                $returnvalue= $query->result_array();
                return $returnvalue;
            }
            else
            {
                return FALSE;
            }
        }
    }
        public function property_delete($property_id){
        $query = $this->db->where('registration_id',$property_id);
        $query = $this->db->delete('registration');
    }
    //Function for country selectoin
    public function CountrySelection ()
    {
    $CountryArray = array("Afghanistan"=>"Afghanistan","Albania"=>"Albania","Algeria"=>"Algeria","AmericanSamoa"=>"American Samoa","Andorra"=>"Andorra","Angola"=>"Angola","Anguilla"=>"Anguilla","Antigua"=>"Antigua","Barbuda"=>"Barbuda","Argentina"=>"Argentina","Armenia"=>"Armenia","Aruba"=>"Aruba","Australia"=>"Australia","Austria"=>"Austria","Azerbaijan"=>"Azerbaijan","Bahamas"=>"Bahamas","Bahrain"=>"Bahrain","Bangladesh"=>"Bangladesh","Barbados"=>"Barbados","Belarus"=>"Belarus","Belgium"=>"Belgium","Belize"=>"Belize","Benin"=>"Benin","Bermuda"=>"Bermuda","Bhutan"=>"Bhutan","Bolivia"=>"Bolivia","BosniaAndHerzegovina"=>"Bosnia & Herzegovina","Botswana"=>"Botswana","Brazil"=>"Brazil","BruneiDarussalam"=>"Brunei Darussalam","Bulgaria"=>"Bulgaria","BurkinaFaso"=>"Burkina Faso","Burundi"=>"Burundi","Cambodia"=>"Cambodia","Cameroon"=>"Cameroon","Canada"=>"Canada","CapeVerde"=>"Cape Verde","CaymanIslands"=>"Cayman Islands","CentralAfricanRepublic"=>"Central African Republic","Chad"=>"Chad","Chile"=>"Chile","China"=>"China","Colombia"=>"Colombia","Comoros"=>"Comoros","Congo"=>"Congo","CongoDRC"=>"Congo (DRC)","Cook"=>"Cook","Islands"=>"Islands","CostaRica"=>"Costa Rica","CotedIvoire"=>"C&ocirc;te d'Ivoire","Croatia"=>"Croatia","Cuba"=>"Cuba","Cyprus"=>"Cyprus","CzechRepublic"=>"Czech Republic","Denmark"=>"Denmark","Djibouti"=>"Djibouti","Dominica"=>"Dominica","DominicanRepublic"=>"Dominican Republic","EastTimor"=>"East Timor","Ecuador"=>"Ecuador","Egypt"=>"Egypt","ElSalvador"=>"El Salvador","EquatorialGuinea"=>"Equatorial Guinea","Eritrea"=>"Eritrea","Estonia"=>"Estonia","Ethiopia"=>"Ethiopia","FaeroeIslands"=>"Faeroe Islands","FalklandIslands"=>"Falkland Islands","Fiji"=>"Fiji","Finland"=>"Finland","France"=>"France","FrenchGuiana"=>"French Guiana","French"=>"French","Polynesia"=>"Polynesia","Gabon"=>"Gabon","Gambia"=>"Gambia","Georgia"=>"Georgia","Germany"=>"Germany","Ghana"=>"Ghana","Gibraltar"=>"Gibraltar","Greece"=>"Greece","Greenland"=>"Greenland","Grenada"=>"Grenada","Guadeloupe"=>"Guadeloupe","Guam"=>"Guam","Guatemala"=>"Guatemala","Guinea"=>"Guinea","Guinea-Bissau"=>"Guinea-Bissau","Guyana"=>"Guyana","Haiti"=>"Haiti","HolySee"=>"Holy See","Honduras"=>"Honduras","HongKong"=>"Hong Kong","Hungary"=>"Hungary","Iceland"=>"Iceland","India"=>"India","Indonesia"=>"Indonesia","Iran"=>"Iran","Iraq"=>"Iraq","Ireland"=>"Ireland","Israel"=>"Israel","Italy"=>"Italy","Jamaica"=>"Jamaica","Japan"=>"Japan","Jordan"=>"Jordan","Kazakhstan"=>"Kazakhstan","Kenya"=>"Kenya","Kiribati"=>"Kiribati","KoreaDPR"=>"Korea DPR","KoreaRepublic"=>"Korea Republic","Kuwait"=>"Kuwait","Kyrgyzstan"=>"Kyrgyzstan","Laos"=>"Laos","Latvia"=>"Latvia","Lebanon"=>"Lebanon","Lesotho"=>"Lesotho","Liberia"=>"Liberia","Libya"=>"Libya","Liechtenstein"=>"Liechtenstein","Lithuania"=>"Lithuania","Luxembourg"=>"Luxembourg","Macau"=>"Macau","Macedonia"=>"Macedonia","Madagascar"=>"Madagascar","Malawi"=>"Malawi","Malaysia"=>"Malaysia","Maldives"=>"Maldives","Mali"=>"Mali","Malta"=>"Malta","MarshallIslands"=>"Marshall Islands","Martinique"=>"Martinique","Mauritania"=>"Mauritania","Mauritius"=>"Mauritius","Mexico"=>"Mexico","Micronesia"=>"Micronesia","Moldova"=>"Moldova","Monaco"=>"Monaco","Mongolia"=>"Mongolia","Montserrat"=>"Montserrat","Morocco"=>"Morocco","Mozambique"=>"Mozambique","Myanmar"=>"Myanmar","Namibia"=>"Namibia","Nauru"=>"Nauru","Nepal"=>"Nepal","Netherlands"=>"Netherlands","NetherlandsAntilles"=>"Netherlands Antilles","NewCaledonia"=>"New Caledonia","NewZealand"=>"New Zealand","Nicaragua"=>"Nicaragua","Niger"=>"Niger","Nigeria"=>"Nigeria","Niue"=>"Niue","NorfolkIsland"=>"Norfolk Island","Northern"=>"Northern","MarianaIsland"=>"Mariana Island","Norway"=>"Norway","Oman"=>"Oman","Pakistan"=>"Pakistan","Palau"=>"Palau","PalestinianTerritory"=>"Palestinian Territory","Panama"=>"Panama","PapuaNewGuinea"=>"Papua NewGuinea","Paraguay"=>"Paraguay","Peru"=>"Peru","Philippines"=>"Philippines","Pitcairn"=>"Pitcairn","Poland"=>"Poland","Portugal"=>"Portugal","PuertoRico"=>"Puerto Rico","Qatar"=>"Qatar","R&eacute;union"=>"R&eacute;union","Romania"=>"Romania","RussianFederation"=>"Russian Federation","Rwanda"=>"Rwanda","StHelena"=>"St Helena","StKittsAndNevis"=>"St Kitts and Nevis","StLucia"=>"St Lucia","StPierreMiquelon"=>"St Pierre Miquelon","StVincentGrenadines"=>"St Vincent Grenadines","Samoa"=>"Samoa","SanMarino"=>"San Marino","SaoTomePrincipe"=>"Sao Tome Principe","SaudiArabia"=>"Saudi Arabia","Senegal"=>"Senegal","Seychelles"=>"Seychelles","SierraLeone"=>"Sierra Leone","Singapore"=>"Singapore","Slovakia"=>"Slovakia","Slovenia"=>"Slovenia","SolomonIslands"=>"Solomon Islands","Somalia"=>"Somalia","SouthAfrica"=>"South Africa","Spain"=>"Spain","SriLanka"=>"Sri Lanka","Sudan"=>"Sudan","Suriname"=>"Suriname","Swaziland"=>"Swaziland","Sweden"=>"Sweden","Switzerland"=>"Switzerland","Syria"=>"Syria","TaiwanProvinceOfChina"=>"Taiwan Province of China","Tajikistan"=>"Tajikistan","Tanzania"=>"Tanzania","Thailand"=>"Thailand","Togo"=>"Togo","Tokelau"=>"Tokelau","Tonga"=>"Tonga","TrinidadTobagoTunisia"=>"Trinidad Tobago Tunisia","Turkey"=>"Turkey","Turkmenistan"=>"Turkmenistan","TurksCaicosIslands"=>"Turks Caicos Islands","Tuvalu"=>"Tuvalu","Uganda"=>"Uganda","Ukraine"=>"Ukraine","UnitedArabEmirates"=>"United Arab Emirates","UnitedKingdom"=>"United Kingdom","USA"=>"USA","Uruguay"=>"Uruguay","Uzbekistan"=>"Uzbekistan","Vanuatu"=>"Vanuatu","Venezuela"=>"Venezuela","VietNam"=>"Viet Nam","Virgin"=>"Virgin","IslandsBritish"=>"Islands British","VirginIslands"=>"Virgin Islands","WallisFutunaIslands"=>"Wallis Futuna Islands","WesternSahara"=>"Western Sahara","Yemen"=>"Yemen","Yugoslavia"=>"Yugoslavia","Zambia"=>"Zambia","Zimbabwe"=>"Zimbabwe");
    return $CountryArray;
    }
    public function random_generator($digits){
        srand ((double) microtime() * 10000000);
        //Array of alphabets
        $input = array ("A", "B", "C", "D", "E","F","G","H","I","J","K","L","M","N","O","P","Q",
        "R","S","T","U","V","W","X","Y","Z");
        $random_generator="";// Initialize the string to store random numbers
        for($i=1;$i<$digits+1;$i++){ // Loop the number of times of required digits
        if(rand(1,2) == 1){// to decide the digit should be numeric or alphabet
        // Add one random alphabet
        $rand_index = array_rand($input);
        $random_generator .=$input[$rand_index]; // One char is added
        }else{
        // Add one numeric digit between 1 and 10
        $random_generator .=rand(1,10); // one number is added
        } // end of if else
        } // end of for loop
        return $random_generator;
        }
         public function dealerdetails($user_type,$user_id){
        $this->load->helper('url');
        $this -> db -> select('*');
        $this -> db -> from('registration');
        $this -> db -> where('registration_id = ' . "'" . $user_id . "'");
        $this -> db -> where('usertype = ' . "'" . $user_type . "'");
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	 return $retrieved;
        }
        else{
            return FALSE;
        }
    }
    //get details with user id
         public function dealerfulldetails($user_id)
         {
        $this->load->helper('url');
        $this -> db -> select('*');
        $this -> db -> from('registration');
        $this -> db -> where('registration_id = ' . "'" . $user_id . "'");
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	 return $retrieved;
        }
        else{
            return FALSE;
        }
    }
    /*Get all dealers under the account manager*/
    public function manager_dealerlist($user_id){
        $this->db->select('*');
        $this->db->from('user_setting');
        $this -> db -> where('user_id',$user_id);
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	     return $retrieved;
        }
        else{
            return FALSE;
        }
    }
    public function getdealerslisting($user_id){
        $details=$this ->manager_dealerlist($user_id);
        $ij=0;
        if(isset($details)!='' && is_array($details)&& !empty($details)){
            $query="SELECT * FROM registration WHERE usertype='dealership' and (";
            foreach($details as $row){
                if($row!='')
                {
                if($ij>0){
                    $query.="or ";
                }
                $query.="registration_id= $row[dealers_id] ";
                $ij++;
            }
            }
            $query.=")";
            $result=$this->db->query($query);
            if($result -> num_rows() >0){
                $retrieved=$result->result_array();
                return $retrieved;
            }
            else{
                return FALSE;
            }
        }
        else{
            return FALSE;
        }
    }
    /*Get all dealers under the account manager*/
    public function account_manager_information($user_id){
        $this->db->select('*');
        $this->db->from('user_setting');
        $this -> db -> where('dealers_id',$user_id);
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	     return $retrieved;
        }
        else{
            return FALSE;
        }
    }
    public function getaccountmanagerdetaild($user_id){
        $details=$this ->account_manager_information($user_id);
        $ij=0;
        if(isset($details)!='' && is_array($details)&& !empty($details)){
            $query="SELECT * FROM registration WHERE usertype='account_managers' and (";
            foreach($details as $row){
                if($row!='')
                {
                if($ij>0){
                    $query.=" or ";
                }
                $query.="registration_id= $row[user_id]";
                $ij++;
            }
            }
            $query.=")";
            $result=$this->db->query($query);
            if($result -> num_rows() >0){
                $retrieved=$result->result_array();
                return $retrieved;
            }
            else{
                return FALSE;
            }
        }
        else{
            return FALSE;
        }
    }
    //getting all dealers under a autobrand delaers
public function getauto_brand_dealers($user_id){
    //get master brand of particular auo_brand user
        $sql=("SELECT *
        FROM registration
        WHERE STATUS = 'VERIFIED' and `usertype` = 'auto_brand' and registration_id='$user_id'
        ");
        $query=$this->db->query($sql);
        $masterbrand_auto_brand_delaers='';
        if($query -> num_rows() > 0)
        {
            $returnvalue= $query->result_array();
             foreach($returnvalue as $assigned_dealer_id)
             {
                $masterbrand_auto_brand_delaers=$assigned_dealer_id['masterbrand'];
             }
             if($masterbrand_auto_brand_delaers!='')
             {
                //get all the dealers under that particular auto brand dealers
                   $sql_getquery=("SELECT *
                    FROM registration
                    WHERE STATUS = 'VERIFIED' and `usertype` = 'dealership' and masterbrand='$masterbrand_auto_brand_delaers'
                    ");
                    $query_result=$this->db->query($sql_getquery);
                    if($query_result -> num_rows() > 0)
                    {
                       $result= $query_result->result_array();
                    }
                    else
                    {
                        $result='FALSE';
                    }
             }
              else
                {
                    $result='FALSE';
                }
        }
        else
        {
            $result='FALSE';
        }
        return $result;
    }
    //function to get the usertype
   public function get_usertype($user_id){
        $this->db->select('usertype');
        $this->db->from('registration');
        $this -> db -> where('registration_id',$user_id);
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
        foreach ($result->result() as $row)
           {
              $retrieved=$row->usertype;
           }
      	     return $retrieved;
        }
        else{
            return FALSE;
        }
    }
    //function to encrpt and decrypt
    function ProtectData ($DataString,$Method)
{
	$FinalDataToReturn = '';
	if ($Method=='ENCODE')
	{
		//encrypt data
		$EncryptedData = base64_encode($DataString);
		//split encrypted data
		$EncryptedDataSplit = str_split($EncryptedData);
		//reversed array
		$EncryptedDataReversedArray = array_reverse($EncryptedDataSplit, true);
		foreach ($EncryptedDataReversedArray as $Key=>$Value)
			{
				$FinalDataToReturn .= $Value;
			}
		// encrypt the fina take
		$FinalDataToReturn = base64_encode($FinalDataToReturn);
	}elseif ($Method=='DECODE')
		{
		// decrypt the final encode
		$DataString = base64_decode($DataString);
		//split encrypted data
		$EncryptedDataSplit = str_split($DataString);
		//reversed array
		$EncryptedDataReversedArray = array_reverse($EncryptedDataSplit, true);
		foreach ($EncryptedDataReversedArray as $Key=>$Value)
			{
				$FinalDataToReturn .= $Value;
			}
		//encrypt data
		$FinalDataToReturn = base64_decode($FinalDataToReturn);
		}
	return $FinalDataToReturn;
}
        public function getusstates()
         {
        $this->load->helper('url');
        $this -> db -> select('*');
        $this -> db -> from('states');
        $this->db->order_by("state", "asc");
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	 return $retrieved;
        }
        else{
            return FALSE;
        }
    }
       public function getuscities($states)
         {
        $sql_qurey=("SELECT distinct city
        FROM  cities
        where  	state='$states'");
        $result=$this->db->query($sql_qurey);
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
 	      foreach($retrieved as $city_selected)
             {
                echo '<option value="'.$city_selected['city'].'">'.$city_selected['city'].'</option>';
             }
        }
        else{
            echo '<option value="">No data available</option>';
        }
    }
    public function getuserselectedcity($states,$city)
         {
       $sql_qurey=("SELECT distinct city
        FROM  cities
        where  	state='$states'");
        $result=$this->db->query($sql_qurey);
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
        echo '<label for="small-label-1" class="label">City</label>';
         echo '<select id="city_select" name="city" class="select" style="text-align: left;" onchange="postcode_display()">';
 	      foreach($retrieved as $city_selected)
             {
                if($city!='')
                {
                   if($city_selected['city']==$city)
                   {
                    echo '<option value="'.$city_selected['city'].'" selected>'.$city_selected['city'].'</option>';
                   }
                }
                echo '<option value="'.$city_selected['city'].'">'.$city_selected['city'].'</option>';
             }
        }
        else{
            echo '<option value="">No data available</option>';
        }
        echo '</select>';
    }
     public function Canadian_provinces ()
    {
    $CountryArray = array("alberta"=>"Alberta"," british_columbia"=>"British Columbia","manitoba"=>"Manitoba","new_brunswick"=>"New Brunswick","newfoundland_and_Labrador"=>"Newfoundland and Labrador","nova_scotia"=>"Nova Scotia","northwest_territories"=>"Northwest Territories","nunavut"=>"Nunavut","ontario"=>"Ontario","prince_edward_island"=>"Prince Edward Island","quebec"=>"Quebec","saskatchewan"=>"Saskatchewan","yukon"=>"Yukon");
    return $CountryArray;
    }
    public function makes_models(){

         $sql_make=("SELECT * from manufacturer
        order by make asc");
        $result=$this->db->query($sql_make);
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
        return $retrieved;
        }else{
            return false;
        }
    }
    //Email function
    function HTMLemail($to, $from, $cc='', $subject='Trial Email', $message='Trial Message')
    {
    	$headers = "From: " . $from . "\r\n";
    	$headers .= "Reply-To: ". $from . "\r\n";
    	$headers .= "CC: ".$cc."\r\n";
    	$headers .= "MIME-Version: 1.0\r\n";
    	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    	$maildelivery = mail($to, $subject, $message, $headers);
    	if ($maildelivery)
    	{
    		$status = TRUE;
    	}else
    		{
    		$status = FALSE;
    		}
    	return $status;
    }
    function get_createrid($loggedinid){
     $this->db->select('created_id');
        $this->db->from('registration');
        $this -> db -> where('registration_id',$loggedinid);
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
        foreach ($result->result() as $row)
           {
              $retrieved=$row->created_id;
           }
      	     return $retrieved;
        }
        else{
            return FALSE;
        }
    }
       function displaycustomerdata(){
     $this->db->select('*');
        $this->db->from(' vehicle_data');

        $result=$this -> db -> get();
       if($result -> num_rows() >0){
        $retrieved=$result->result_array();
        return $retrieved;
        }else{
            return false;
        }
    }

//get all the assigned dealers and remaining un assign ed dealers
public function subadmindisplay($loggedin_id)
{

       $sql_qurey=("SELECT registration_id
FROM registration
WHERE STATUS = 'VERIFIED' and usertype<> 'admin' and `usertype` <>'sub_admin'
ORDER BY `usertype` = 'auto_brand', `usertype` = 'account_managers', `usertype` = 'dealership' , registration_id desc");
    $query_delaers=$this->db->query($sql_qurey);
    if($query_delaers-> num_rows() > 0)
    {
        $dealers_id_get= $query_delaers->result_array();
        if(isset($dealers_id_get) && $dealers_id_get!='')
    {
         foreach($dealers_id_get as $assigned_dealer_id)
             {
                $detaler_detaild[]=$assigned_dealer_id['registration_id'];
             }


    }
    }
      else
      {
        $detaler_detaild='';
      }
    $sql=("SELECT registration_id
FROM registration
WHERE STATUS = 'VERIFIED' and
`usertype` = 'sub_admin' and created_id=$loggedin_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
       	    $returnvalue= $query->result_array();
           if(isset($returnvalue) && $returnvalue!='')
            {
             foreach($returnvalue as $dealer_id)
             {
                if($dealer_id!='')
                {
                $detaler_detaild[]=$dealer_id['registration_id'];
                }
             }
            }

}


if(isset($detaler_detaild) && $detaler_detaild!='')
{
     $ij=0;
     $sql_query_details="SELECT *
    FROM  registration where
    status='VERIFIED'  and (";
    foreach($detaler_detaild as $alldealersget)
       {
        if($alldealersget!='')
        {
    if($ij>0){
           $sql_query_details.="or ";
          }
           $sql_query_details.="registration_id= $alldealersget ";
        $ij++;
        }
        }

         $sql_query_details.=" )";
          $sql_query_details.="ORDER BY `usertype` = 'sub_admin',`usertype` = 'auto_brand', `usertype` = 'account_managers', `usertype` = 'dealership' , registration_id desc";
    $query_result=$this->db->query($sql_query_details);
    if($query_result-> num_rows() > 0)
    {
        $delaer_result= $query_result->result_array();
        $dealer_full_dealer_details=$delaer_result;
          return $dealer_full_dealer_details;
    }
    else
    {
          return false;
    }
}
    }


public function get_lead_mining_presets($event_id)
{
    $event_id=$this->session->userdata('event_id_get');
    $sql=("Select lead_mining_presets from epsadvantage_campaign where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $returnvalue= $query->result_array();
    foreach($returnvalue as $value){
        
    
    $return=$value['lead_mining_presets'];  
    }
    }
    else{
    $return='';
    }
    return $return;
}

 public function get_dealer_logo($loggedin_id='')
{
$sql_getquery=("SELECT masterbrand
                    FROM  registration
                    WHERE STATUS = 'VERIFIED'  and registration_id='$loggedin_id' and (usertype='dealership' or usertype='auto_brand')
                    ");
                    $query_result=$this->db->query($sql_getquery);
                    if($query_result -> num_rows() > 0)
                    {
                       $result= $query_result->result_array();
                       foreach($result as $dealer_id)
                        {
                                $sql_getquery_logo=("SELECT make_image
                            FROM   vehiclemodelyear
                            WHERE make = '$dealer_id[masterbrand]' order by id limit 1
                            ");
                            $query_result_logo=$this->db->query($sql_getquery_logo);
                            if($query_result_logo -> num_rows() > 0)
                            {
                                $result_logo= $query_result_logo->result_array();

                                return $result_logo;
                            }
                            else
                            {
                                return false;
                            }
                        }

                    }
                    else
                    {
                        return false;
                    }
}
   public function customerdata($user_id)
         {
        $this->load->helper('url');
        $this -> db -> select('*');
        $this -> db -> from('pbs_customer_data');
        $this -> db -> where('dealership_id', $user_id);
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	 return $retrieved;
        }
        else{
            return FALSE;
        }
    }
    
       public function customerdatafulldetails($dataid)
         {
       $sql_getquery_logo=("SELECT *
                            FROM    pbs_customer_data
                            WHERE 	id 	= '$dataid' order by id limit 1
                            ");
       $result=$this->db->query($sql_getquery_logo);
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	 return $retrieved;
        }
        else{
            return FALSE;
        }
    }
       public function dealercompanynameget($dealer_id)
         {
       $sql_getquery_logo=("SELECT company_name
                            FROM     registration
                            WHERE 	registration_id 	= '$dealer_id' order by registration_id limit 1
                            ");
       $result=$this->db->query($sql_getquery_logo);
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	 return $retrieved;
        }
        else{
            return FALSE;
        }
    }
    public function password_update($user_id){
        $password_get=$this->input->post('password');
        $password=$this ->ProtectData($password_get,'ENCODE');
        $this -> db -> set('password', $password);
        $this->db->where('registration_id',$user_id);
        $this->db->update('registration');
    }


    public function customerdatalist($user_id,$startlimit,$endlimit,$leadlist_id) {
    $sql_get_query=("SELECT *
    FROM    pbs_customer_data
    WHERE 	dealership_id 	= '$user_id' order by id asc limit $startlimit,$endlimit
    ");
    $result=$this->db->query($sql_get_query);
    if($result -> num_rows() >0){
    $retrieved=$result->result_array();
    return $retrieved;
    }
    else{
    return FALSE;
        }
      return FALSE;
    }
     public function customerdatalist_equityscrap($user_id,$startlimit,$endlimit,$leadlist_id,$event_id) {
     $sql_date_range=("Select past_vehicle_purchase_date_from_range,past_vehicle_purchase_date_to_range from  epsadvantage_campaign where event_id=$event_id");
    $query_date_range=$this->db->query($sql_date_range);
    if($query_date_range -> num_rows() > 0){
    $returnvalue_get_date_range=$query_date_range->result_array();
    foreach($returnvalue_get_date_range as $purchase_date_range){
        $purchase_date_range_from=$purchase_date_range['past_vehicle_purchase_date_from_range'];  
        $purchase_date_range_to=$purchase_date_range['past_vehicle_purchase_date_to_range'];  
    }
    }
    $date=time();
    $new_purchase_to_range = $purchase_date_range_to*12;
    $newdate_purchase_range_to_date_compare = strtotime ( '-'.$new_purchase_to_range.' month' , $date ) ;   
    $new_purchase_from_range =$purchase_date_range_from*12;
    $newdate_purchase_range_from_date_compare = strtotime ( '-'.$new_purchase_from_range.' month' , $date  ) ; 
    $sql_get_query=("SELECT *
    FROM    eps_data
    WHERE 	dealership_id 	= '$user_id' AND 
    first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
    order by id asc limit $startlimit,$endlimit
    ");
    $result=$this->db->query($sql_get_query);
    if($result -> num_rows() >0){
    $retrieved=$result->result_array();
    return $retrieved;
    }
    else{
    return FALSE;
        }
      return FALSE;
    }

    public function delete_eventcampign($event_id){
        $query = $this->db->where('event_id',$event_id);
        $query = $this->db->delete('events');
        $query = $this->db->where('event_id',$event_id);
        $query = $this->db->delete('epsadvantage_campaign');
        $query = $this->db->where('event_id',$event_id);
        $query = $this->db->delete('select_customer_leadlist');
        $query = $this->db->where('event_id',$event_id);
        $query = $this->db->delete('select_mailer_options');
    }
    /*
       public function customerdatalist_model_breakdown($user_id,$startlimit,$endlimit,$vehicletype,$pastdate_to,$pastdate_from) {

   	if($vehicletype!='') {
    $sql_get_query=("SELECT * FROM pbs_vehicledata_data as vd,pbs_customer_data as cd ,pbs_financial_data as fd WHERE
   	(vd.customer_id=cd.id) and
    cd.dealership_id=$user_id and 
    (fd.contract_date BETWEEN '$pastdate_to' AND '$pastdate_from') and 
	vd.vehicletype = '$vehicletype' GROUP BY cd.id limit $startlimit,$endlimit");
  
	}else{
 	$sql_get_query=("SELECT * FROM pbs_vehicledata_data as vd,pbs_customer_data as cd ,pbs_financial_data as fd WHERE
	(vd.customer_id=cd.id) and
	dealership_id=$user_id and
	(vd.vehicletype != 'Car' or
	vd.vehicletype != 'SUV' or
	vd.vehicletype != 'Truck' or
	vd.vehicletype != 'Van') and
    (fd.contract_date BETWEEN '$pastdate_to' AND '$pastdate_from') GROUP BY cd.id limit $startlimit,$endlimit");
	}

    $result=$this->db->query($sql_get_query);
    if($result -> num_rows() >0){
    /*$retrieved=$result->result_array();
    $ij=0;
    $sql=("select * from  pbs_customer_data where(");
    foreach($retrieved as $values){
    if($values!=''){
     if($ij>0){
                $sql.="or ";
             }
                $sql.="(id=$values[customer_id])";
                $ij++;
    }
    }
    $sql.=")";

        $quer = $this->db->query($sql);
        $returnvalue_get= $quer->result_array();
		
		$returnvalue_get=$result->result_array();
    }
    else{
    $returnvalue_get='';
        }
        return $returnvalue_get;
    }
*/
    public function customerdatalist_model_breakdown($user_id,$startlimit,$endlimit,$vehicletype,$group_id,$event_id) {
     $returnvalue_get=''; 
     $condition='';
     //getting purchase to and from range
    $sql_date_range=("Select past_vehicle_purchase_date_from_range,past_vehicle_purchase_date_to_range from  epsadvantage_campaign where event_id=$event_id");
    $query_date_range=$this->db->query($sql_date_range);
    if($query_date_range -> num_rows() > 0){
    $returnvalue_get_date_range=$query_date_range->result_array();
    foreach($returnvalue_get_date_range as $purchase_date_range){
        $purchase_date_range_from=$purchase_date_range['past_vehicle_purchase_date_from_range'];  
        $purchase_date_range_to=$purchase_date_range['past_vehicle_purchase_date_to_range'];  
    }
    }
    $date=time();
    $new_purchase_to_range = $purchase_date_range_to*12;
    $newdate_purchase_range_to_date_compare = strtotime ( '-'.$new_purchase_to_range.' month' , $date ) ;   
    $new_purchase_from_range =$purchase_date_range_from*12;
    $newdate_purchase_range_from_date_compare = strtotime ( '-'.$new_purchase_from_range.' month' , $date  ) ; 
     if($group_id!=''){
        $condition.="id  NOT IN ($group_id) AND ";  
     } 
     $condition.='('; 
   	if($vehicletype!='') {
    $condition.='('; 
    if($vehicletype=='Car'){
    $condition.="(vehicletype='Car' and vehiclesize='Large') OR (vehicletype='Car' and vehiclesize='Midsize') OR (vehicletype='Car' and vehiclesize='Smallsize') OR (vehicletype='Green Cars')";
    }
    else if($vehicletype=='SUV'){
    $condition.="(vehicletype='SUV' OR vehicletype='Crossovers')";    
    }
    else if($vehicletype=='Trucks'){
    $condition.="vehicletype='Truck'";    
    }
    
     else if($vehicletype=='Vans'){
    $condition.="vehicletype='Van'";    
    }
    else{
      $condition.="vehicletype='Vans'";   
    }
    $condition.=')';
    $condition.=" AND dealership_id='$user_id' AND 
    first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare" ;
    $condition.=')';
    $sql_get_query=("SELECT  * FROM eps_data  WHERE
   	$condition  
    GROUP BY  buyer_address  order by id ASC limit $startlimit,$endlimit");
    
  	}else{
    $sql_get_query=("SELECT  * FROM eps_data  WHERE (
	(vehicletype!='Car' and 
    vehicletype!='Car' and 
    vehicletype!='Car' and 
    vehicletype!='Green Cars' and
	vehicletype != 'SUV' and
    vehicletype != 'Crossovers' and
    vehicletype != 'Truck' and
	vehicletype != 'Van') AND (dealership_id='$user_id'))
    GROUP BY  buyer_address  order by id ASC limit $startlimit,$endlimit");
	}

    $result=$this->db->query($sql_get_query);
    if($result -> num_rows() >0){
    /*$retrieved=$result->result_array();
    $ij=0;
    $sql=("select * from  pbs_customer_data where(");
    foreach($retrieved as $values){
    if($values!=''){
     if($ij>0){
                $sql.="or ";
             }
                $sql.="(id=$values[customer_id])";
                $ij++;
    }
    }
    $sql.=")";

        $quer = $this->db->query($sql);
        $returnvalue_get= $quer->result_array();
		*/
		$returnvalue_get=$result->result_array();
    }
    else{
    $returnvalue_get='';
        }
        return $returnvalue_get;
    }
    function get_campaine_purchased_dates($event_id)
    {
    $event_id=$this->session->userdata('event_id_get');
    $sql=("Select  past_vehicle_purchase_date_from_range,past_vehicle_purchase_date_to_range from epsadvantage_campaign where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
     $return=$returnvalue;  
    }
    else
    {
        $return='';
    }
    return $return;
    }
    //fuel type
    public function customerdatalist_fuel_type($user_id,$startlimit,$endlimit,$vehicletype,$group,$leadlist_id,$event_id) {
    //getting purchase to and from range
    $sql_date_range=("Select past_vehicle_purchase_date_from_range,past_vehicle_purchase_date_to_range from  epsadvantage_campaign where event_id=$event_id");
    $query_date_range=$this->db->query($sql_date_range);
    if($query_date_range -> num_rows() > 0){
    $returnvalue_get_date_range=$query_date_range->result_array();
    foreach($returnvalue_get_date_range as $purchase_date_range){
        $purchase_date_range_from=$purchase_date_range['past_vehicle_purchase_date_from_range'];  
        $purchase_date_range_to=$purchase_date_range['past_vehicle_purchase_date_to_range'];  
    }
    }
    $date=time();
    $new_purchase_to_range = $purchase_date_range_to*12;
    $newdate_purchase_range_to_date_compare = strtotime ( '-'.$new_purchase_to_range.' month' , $date ) ;   
    $new_purchase_from_range =$purchase_date_range_from*12;
    $newdate_purchase_range_from_date_compare = strtotime ( '-'.$new_purchase_from_range.' month' , $date  ) ; 
     $condition='';
     if($leadlist_id!=''){
        $condition.="id  NOT IN ($leadlist_id) AND ";  
     }
     $condition.='(';
     $condition.='(';   
    if($group=='1'){
       $condition.='fuel_efficiency BETWEEN 0.1 AND 8.4';
    }
     else if($group=='2'){
        $condition.='fuel_efficiency >= 8.4';
     }
    else if($group=='3'){
        $condition.='fuel_efficiency BETWEEN 0.1 AND 10.5';
    }
     else if($group=='4'){
        $condition.='fuel_efficiency < 13.5';
    }
     else if($group=='5'){
        $condition.='fuel_efficiency < 13.5';
    }
     else if($group=='6'){
        $condition.='fuel_efficiency > 13.5';
     }
     $condition.=')'; 
    $condition.=" AND dealership_id='$user_id' AND 
    first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare" ;
    $condition.=')'; 
    $sql_get_query=("SELECT  * FROM eps_data  WHERE
   	
    $condition
    GROUP BY  buyer_address  order by id ASC limit $startlimit,$endlimit");
$result=$this->db->query($sql_get_query);
    if($result -> num_rows() >0){
 
		$returnvalue_get=$result->result_array();
    }
    else{
    $returnvalue_get='';
        }
        return $returnvalue_get;
    }
    public function warrant_scarp($event_id,$user_id,$startlimit,$endlimit,$group,$group_id){
    
    $result_get_array=array();
    $return_result='';
    if($user_id!='198'){
    $condition='';
        $id_get='';
        $sql=("Select  event_start_date from events where event_id=$event_id");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0){
        $returnvalue_get=$query->result_array();
        foreach($returnvalue_get as $event_date){
        $event_date_select=$event_date['event_start_date'];   
        }
         if($group_id!=''){
        $condition.="id  NOT IN ($group_id) AND ";  
        }
        $query_financial=("SELECT id,first_payment_date,sold_vehicle_make from  eps_data where $condition (dealership_id='$user_id') 
       	");
        
        $sql_get_query=$this->db->query($query_financial);
        if($sql_get_query -> num_rows() > 0)
        {
        $returnvalue_customer_data=$sql_get_query->result_array();
        foreach($returnvalue_customer_data as $values_customer_data) {
        $sql_warranty_query=("SELECT  * FROM warranty_manufacture  WHERE
        manufacturer='$values_customer_data[sold_vehicle_make]'
        ");
        $sql_warranty_manufacture=$this->db->query($sql_warranty_query);
        if($sql_warranty_manufacture -> num_rows() > 0)
        {
        $returnvalue_warranty_manufacture=$sql_warranty_manufacture->result_array();
        foreach($returnvalue_warranty_manufacture as $value_warranty){
        $powertrain_months=$value_warranty['powertrain_months'];
        $basic_months=$value_warranty['basic_months'];    
        }
        }
        else{
        $powertrain_months='';
        $basic_months='';     
        }
        $purcahse_date=strtotime($values_customer_data['first_payment_date']);
        $difference = $event_date_select - $purcahse_date;
        $months = floor($difference / 86400 / 30 );
        $customer_id_get=$values_customer_data['id'];
                            if($months>0){
                                if($group==1){
                                if($powertrain_months!=''){
                                    if($months>$powertrain_months){
                                        $id_get[]= $customer_id_get; 
                                        //echo $months.'-'.$values_customer_data['sold_vehicle_make'].'-'.$powertrain_months."<br />";
                                    }
                                }
                                }
                                if($group==2){
                                    if($powertrain_months!=''){
                                        if($months>($powertrain_months-6)){
                                          $id_get[]= $customer_id_get; 
                                         }
                                    }
                                }
                                 if($group==3){
                                 if($basic_months!=''){
                                    if($months>($basic_months)){
                                        
                                            $id_get[]=$customer_id_get; 
                                        
                                    }
                                 }
                                 }
                                 if($group==4){
                                     if($basic_months!=''){    
                                         if($months<($basic_months-6)){
                                            
                                            $id_get[]=$customer_id_get; 
                                            
                                          }
                                     }
                                 }
                                   if($group==5){
                                       if($basic_months!=''){
                                       if($months>($basic_months-6)){
                                           
                                                $id_get[]=$customer_id_get; 
                                            
                                          }
                                     }
                                 }
                                }
                            } 
             $ij=0;
        $sql_warranty='';
        if(!empty($id_get)){
        $sql_warranty=("select * from  eps_data where dealership_id='$user_id' ");
        $sql_warranty.=" AND ";
        $sql_warranty.="(";
        foreach($id_get as $values_id){
        if($values_id!=''){
        if($ij>0){
        $sql_warranty.=" or ";
        }
        $sql_warranty.="(id=$values_id)";
        $ij++;
        }
        }
        $sql_warranty.=")";
        $query_warranty = $this->db->query($sql_warranty);
        $return_result= $query_warranty->result_array();
        }
        else{
         $return_result='';   
        }
        }
        }
        }
        return $return_result;
  
  
  }
    function get_vehicle_warrant($make){
    $sql_get_query=("SELECT  * FROM warranty_manufacture  WHERE
    manufacturer='$make'
   	");
    $result=$this->db->query($sql_get_query);
    if($result -> num_rows() >0){
 
		$returnvalue_get=$result->result_array();
    }
    else{
    $returnvalue_get='';
        }
        return $returnvalue_get;
    }
  public function customerdatalist_model_breakdown_count($user_id,$startlimit,$endlimit,$vehicletype){
     	if($vehicletype!='') {
    $sql_get_query=("SELECT count(*) as count FROM pbs_vehicledata_data as vd,pbs_customer_data as cd  WHERE
   	(vd.customer_id=cd.id) and
    cd.dealership_id=$user_id and 
   	vd.vehicletype = '$vehicletype' 
    GROUP BY cd.id limit $startlimit,$endlimit");
  
	}else{
 	$sql_get_query=("SELECT count(*) as count FROM pbs_vehicledata_data as vd,pbs_customer_data as cd WHERE
	(vd.customer_id=cd.id) and
	dealership_id=$user_id and
	(vd.vehicletype != 'Car' or
	vd.vehicletype != 'SUV' or
	vd.vehicletype != 'Truck' or
	vd.vehicletype != 'Van') 
    GROUP BY cd.id limit $startlimit,$endlimit");
	}
    $result=$this->db->query($sql_get_query);
    if($result -> num_rows() >0){
        $returnvalue_get=$query->result_array();
        foreach($returnvalue_get as $count){
            $return =$count['count'];
        }
    }
    else{
        $return ='';
        
    }
}
function total_leadcount_display($lead_mining_presets,$dealer_id_upload_data,$event_insert_id){
$return='';
$group_id='';  
if($lead_mining_presets=='equity_scrape')
{
$customer_data=$this -> customerdatalist_equityscrap($dealer_id_upload_data,0,200,$group_id,$event_insert_id);
$return=count($customer_data)*5;
}
else if($lead_mining_presets=='model_breakdown'){
$customer_data_group_1=$this ->customerdatalist_model_breakdown($dealer_id_upload_data,0,500,'Car',$group_id,$event_insert_id);
if(isset($customer_data_group_1) && $customer_data_group_1!=''){
foreach($customer_data_group_1 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
 $customer_data_group_2=$this  -> customerdatalist_model_breakdown($dealer_id_upload_data,0,500,'SUVS',$group_id,$event_insert_id);
if(isset($customer_data_group_2) && $customer_data_group_2!=''){
foreach($customer_data_group_2 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_3=$this-> customerdatalist_model_breakdown($dealer_id_upload_data,0,500,'Trucks',$group_id,$event_insert_id);
if(isset($customer_data_group_3) && $customer_data_group_3!=''){
foreach($customer_data_group_3 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_4=$this-> customerdatalist_model_breakdown($dealer_id_upload_data,0,500,'Vans',$group_id,$event_insert_id);
if(isset($customer_data_group_4) && $customer_data_group_4!=''){
foreach($customer_data_group_4 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
 $customer_data_group_5=$this -> customerdatalist_model_breakdown($dealer_id_upload_data,0,500,'',$group_id,$event_insert_id);
if(isset($customer_data_group_5) && $customer_data_group_5!=''){
foreach($customer_data_group_5 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}

if(!empty($customer_data_group_1)){
 $count_group1=count($customer_data_group_1);   
}
else{
 $count_group1=0;   
}
if(!empty($customer_data_group_2)){
 $count_group2=count($customer_data_group_2);   
}
else{
 $count_group2=0;   
}

if(!empty($customer_data_group_3)){
 $count_group3=count($customer_data_group_3);   
}
else{
 $count_group3=0;   
}
if(!empty($customer_data_group_4)){
 $count_group4=count($customer_data_group_4);   
}
else{
 $count_group4=0;   
}
if(!empty($customer_data_group_5)){
 $count_group5=count($customer_data_group_5);   
}
else{
 $count_group5=0;   
}
$return=$count_group1+$count_group2+$count_group3+$count_group4+$count_group5;
}
else if($lead_mining_presets=='effiecency'){
$customer_data_group_1=$this -> customerdatalist_fuel_type($dealer_id_upload_data,0,50,'Car',1,$group_id,$event_insert_id);
if(isset($customer_data_group_1) && $customer_data_group_1!=''){
foreach($customer_data_group_1 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_2=$this  -> customerdatalist_fuel_type($dealer_id_upload_data,0,50,'Car',2,$group_id,$event_insert_id);
if(isset($customer_data_group_2) && $customer_data_group_2!=''){
foreach($customer_data_group_2 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_3=$this -> customerdatalist_fuel_type($dealer_id_upload_data,0,50,'SUV',3,$group_id,$event_insert_id);
if(isset($customer_data_group_3) && $customer_data_group_3!=''){
foreach($customer_data_group_3 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_4=$this  -> customerdatalist_fuel_type($dealer_id_upload_data,0,50,'SUV',4,$group_id,$event_insert_id);
if(isset($customer_data_group_4) && $customer_data_group_4!=''){
foreach($customer_data_group_4 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_5=$this -> customerdatalist_fuel_type($dealer_id_upload_data,0,50,'Truck',5,$group_id,$event_insert_id);
if(isset($customer_data_group_5) && $customer_data_group_5!=''){
foreach($customer_data_group_5 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_6=$this -> customerdatalist_fuel_type($dealer_id_upload_data,0,50,'Truck',6,$group_id,$event_insert_id);
if(isset($customer_data_group_6) && $customer_data_group_6!=''){
foreach($customer_data_group_6 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
 if(!empty($customer_data_group_1))
 {
    $count_group1=count($customer_data_group_1);
 }
 else{
    $count_group1=0;
 }
  if(!empty($customer_data_group_2))
 {
    $count_group2=count($customer_data_group_2);
 }
 else{
    $count_group2=0;
 }
  if(!empty($customer_data_group_3))
 {
    $count_group3=count($customer_data_group_3);
 }
 else{
    $count_group3=0;
 }
  if(!empty($customer_data_group_4))
 {
    $count_group4=count($customer_data_group_4);
 }
 else{
    $count_group4=0;
 }
  if(!empty($customer_data_group_5))
 {
    $count_group5=count($customer_data_group_5);
 }
 else{
    $count_group5=0;
 }
  if(!empty($customer_data_group_6))
 {
    $count_group6=count($customer_data_group_6);
 }
 else{
    $count_group6=0;
 }

 $return=$count_group1+$count_group2+$count_group3+$count_group4+$count_group5+$count_group6;
}
else if($lead_mining_presets=='warranty_scrape'){
$group_id='';
$customer_data_group_1=$this -> main_model -> warrant_scarp($event_insert_id,$dealer_id_upload_data,0,5000,1,$group_id='');
if(isset($customer_data_group_1) && $customer_data_group_1!=''){
foreach($customer_data_group_1 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_2=$this -> main_model -> warrant_scarp($event_insert_id,$dealer_id_upload_data,0,5000,2,$group_id);
if(isset($customer_data_group_2) && $customer_data_group_2!=''){
foreach($customer_data_group_2 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_3=$this -> main_model -> warrant_scarp($event_insert_id,$dealer_id_upload_data,0,5000,3,$group_id);
if(isset($customer_data_group_3) && $customer_data_group_3!=''){
foreach($customer_data_group_3 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_4=$this -> main_model -> warrant_scarp($event_insert_id,$dealer_id_upload_data,0,5000,4,$group_id);
if(isset($customer_data_group_4) && $customer_data_group_4!=''){
foreach($customer_data_group_4 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_5=$this -> main_model -> warrant_scarp($event_insert_id,$dealer_id_upload_data,0,5000,5,$group_id);
if(isset($customer_data_group_5) && $customer_data_group_5!=''){
foreach($customer_data_group_5 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
 if(!empty($customer_data_group_1))
 {
    $count_group1=count($customer_data_group_1);
 }
 else{
    $count_group1=0;
 }
  if(!empty($customer_data_group_2))
 {
    $count_group2=count($customer_data_group_2);
 }
 else{
    $count_group2=0;
 }
  if(!empty($customer_data_group_3))
 {
    $count_group3=count($customer_data_group_3);
 }
 else{
    $count_group3=0;
 }
  if(!empty($customer_data_group_4))
 {
    $count_group4=count($customer_data_group_4);
 }
 else{
    $count_group4=0;
 }
  if(!empty($customer_data_group_5))
 {
    $count_group5=count($customer_data_group_5);
 }
else{
    $count_group5=0;
 }
$return=$count_group1+$count_group2+$count_group3+$count_group4+$count_group5;
}
return $return;
}
function get_total_count_advance_options($event_insert_id,$dealer_id_upload_data,$group_id=''){
$return='';
$group_id='';
$customer_data_group_1=$this -> settings_model -> get_advanced_option_group_details($event_insert_id,1,$dealer_id_upload_data,$group_id);
if(isset($customer_data_group_1) && $customer_data_group_1!=''){
foreach($customer_data_group_1 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_2=$this -> settings_model -> get_advanced_option_group_details($event_insert_id,2,$dealer_id_upload_data,$group_id); 
if(isset($customer_data_group_2) && $customer_data_group_2!='' && is_array($customer_data_group_2)){
foreach($customer_data_group_2 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_3=$this -> settings_model -> get_advanced_option_group_details($event_insert_id,3,$dealer_id_upload_data,$group_id); 
if(isset($customer_data_group_3) && $customer_data_group_3!='' ){

foreach($customer_data_group_3 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
}
 
}
$customer_data_group_4=$this -> settings_model -> get_advanced_option_group_details($event_insert_id,4,$dealer_id_upload_data,$group_id); 
if(isset($customer_data_group_4) && $customer_data_group_4!='' && is_array($customer_data_group_4)){
foreach($customer_data_group_4 as $value){
 if($group_id!=null){
    $group_id.=',';
 }
 $group_id.= $value['id'];  
} 
}
$customer_data_group_5=$this -> settings_model -> get_advanced_option_group_details($event_insert_id,5,$dealer_id_upload_data,$group_id); 
if(!empty($customer_data_group_1)){
 $count_group1=count($customer_data_group_1);   
}
else{
 $count_group1=0;   
}
if(!empty($customer_data_group_2)){
 $count_group2=count($customer_data_group_2);   
}
else{
 $count_group2=0;   
}

if(!empty($customer_data_group_3)){
 $count_group3=count($customer_data_group_3);   
}
else{
 $count_group3=0;   
}
if(!empty($customer_data_group_4)){
 $count_group4=count($customer_data_group_4);   
}
else{
 $count_group4=0;   
}
if(!empty($customer_data_group_5)){
 $count_group5=count($customer_data_group_5);   
}
else{
 $count_group5=0;   
}
$return=$count_group1+$count_group2+$count_group3+$count_group4+$count_group5;
 return $return; 
}
function getpurchaesdate($vehicle_stock){
 $sql=("Select  first_payment_date,contract_date  from pbs_financial_data where vehicle_stock='$vehicle_stock'");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
        $returnvalue_get=$query->result_array();
        foreach($returnvalue_get as $contract_date){
        if($contract_date['first_payment_date']!=''){
        $purchase_date=$contract_date['first_payment_date'];
        }
        else {
          $purchase_date=$contract_date['contract_date'];  
        }
    }
        $return =$purchase_date;
    } 
    else{
        $return ='';
    } 
    return $return ; 
}
public function get_lead_mining_details($event_id)
{
    
    $sql=("Select lead_mining_presets from epsadvantage_campaign where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $returnvalue= $query->result_array();
    foreach($returnvalue as $value){
        
    
    $return=$value['lead_mining_presets'];  
    }
    }
    else{
    $return='';
    }
    return $return;
}
    /*Function to fetch user details*/
     public function getdealerfoldername($dealername)
     {
        $all_users_id='';
        $sql=("SELECT folder_name
        FROM registration
        WHERE `company_name` like '$dealername%'
        ");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
            $returnvalue= $query->result_array();
            foreach($returnvalue as $folder_name){
             $folder_name_send= $folder_name['folder_name']; 
            }
         return $folder_name_send;
        }
        else
        {
            return FALSE;
        }
    }
 
      public function userdetails_with_foldername($dealername)
     {
        $all_users_id='';
        $sql=("SELECT *
        FROM registration
        WHERE `company_name` like '$dealername%'
        ");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
            $returnvalue= $query->result_array();
            return $returnvalue;
        }
        else
        {
            return FALSE;
        }
    } 
 public function ftp_file_feed_details($filename)
     {
        $all_users_id='';
        $sql=("SELECT *
        FROM  ftp_feed_details
        WHERE `filename` ='$filename'
        ");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
            $returnvalue= $query->result_array();
            return $returnvalue;
        }
        else
        {
            return FALSE;
        }
    }   
}
?>