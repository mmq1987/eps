<?php
class Settings_model extends CI_Model {
	public function __construct(){
		$this->load->database();
	}
    public function select_dealers(){
        $this -> load ->helper('url');
        $this -> db ->select('*');
        $this -> db ->from('registration');
        $this -> db -> where('usertype = '. "'dealership'");
        $result = $this -> db ->get();
        if($result -> num_rows()>0){
            $retrieved = $result -> result_array();
			return $retrieved;
        }else{
          return FALSE;  
        }
    }
    public function dealer_check($dealer_name){
        $this -> load ->helper('url');
        $this -> db -> select('*');
        $this -> db -> from('user_setting');
        $this -> db -> where('dealers_id = ' . "'" . $dealer_name . "'");
        $result= $this -> db ->get();
        if($result -> num_rows()>0){
             $retrieved = $result -> result_array();
			return FALSE;
                }
                 return TRUE;  
            }
    /*Function to inser dealer settings*/
    public function insert_dealerdetails($user_id,$dealer_name){
        $this -> load ->helper('url');
        $this -> dealer_check($dealer_name);
        if(isset($dealer_name)&& !empty($dealer_name)){
            foreach($dealer_name as $name){
                $timestamp=date('Y-m-d',time());
                $data=array('user_id'=>$user_id,
                            'dealers_id'=>$name,
                            'time_stamp'=>$timestamp
                            );
                $this->db->insert('user_setting',$data);
            }
        }
    }
        public function managers_assigned_dealers($manager_id){ 
        $this -> load ->helper('url');
        $sql=("Select dealers_id from user_setting where  user_id=$manager_id");
        $query=$this->db->query($sql);
         if($query -> num_rows() > 0)
       	   {
       	    $returnvalue= $query->result_array();
             foreach($returnvalue as $dealer_id)
             {
                $detaler_detaild[]=$dealer_id['dealers_id'];
             }
              return $detaler_detaild;
            } 
            else
            {
                 return $detaler_detaild='';
            } 
            }
         public function alldealwithassigned($manager_id){ 
        $this -> load ->helper('url');
        $sql=("Select dealers_id from user_setting  where  user_id=$manager_id");
        $query=$this->db->query($sql);
         if($query -> num_rows() > 0)
       	   {
       	    $returnvalue= $query->result_array();
             foreach($returnvalue as $dealer_id)
             {
                $detaler_detaild[]=$dealer_id['dealers_id'];
             }
            }
            else
             {
                $detaler_detaild='';
             }
}
public function assigned_alldealers($mangers_id)
{
       $sql=("SELECT registration_id
    FROM  registration
    WHERE  	status='VERIFIED' AND usertype='dealership' 
    AND registration_id not in(select dealers_id FROM user_setting
    WHERE `user_id` =$mangers_id)");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
       	    $returnvalue= $query->result_array();
           if(isset($returnvalue) && $returnvalue!='')
            {        
             foreach($returnvalue as $dealer_id)
             {
                if($dealer_id!='')
                {
                $detaler_detaild[]=$dealer_id['registration_id'];
                }
             }
            }
               else
                {
            $detaler_detaild='';
                }
}       
      else
      {
      $detaler_detaild='';
      }


if(isset($detaler_detaild) && $detaler_detaild!='')
{
     $ij=0;      
     $sql_query_details="SELECT *
    FROM  registration where 
    status='VERIFIED' and usertype='dealership' and (";
    foreach($detaler_detaild as $alldealersget)
       {
        if($alldealersget!='')
        {
    if($ij>0){
           $sql_query_details.="or ";
          } 
           $sql_query_details.="registration_id= $alldealersget ";
        $ij++;
        }
        }
         $sql_query_details.=")";
    $query_result=$this->db->query($sql_query_details);
    if($query_result-> num_rows() > 0)
    {
        $delaer_result= $query_result->result_array();
        $dealer_full_dealer_details=$delaer_result;
          return $dealer_full_dealer_details;
    }
    else
    {
          return false;
    }
} 
}
//get all the assigned dealers and remaining un assign ed dealers
public function assigned_dealer_with_alldealers($mangers_id)
{
    $sql=("SELECT registration_id
    FROM  registration
    where  	status='VERIFIED' and usertype='dealership' and registration_id not in(select dealers_id from user_setting)");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
       	    $returnvalue= $query->result_array();
           if(isset($returnvalue) && $returnvalue!='')
            {        
             foreach($returnvalue as $dealer_id)
             {
                if($dealer_id!='')
                {
                $detaler_detaild[]=$dealer_id['registration_id'];
                }
             }
            }
               else
                {
            $detaler_detaild='';
                }
}       
      else
      {
      $detaler_detaild='';
      }
       $sql_qurey=("SELECT dealers_id
    FROM  user_setting
    where  	user_id='$mangers_id'"); 
    $query_delaers=$this->db->query($sql_qurey);
    if($query_delaers-> num_rows() > 0)
    { 
        $dealers_id_get= $query_delaers->result_array();
        if(isset($dealers_id_get) && $dealers_id_get!='')
    {
         foreach($dealers_id_get as $assigned_dealer_id)
             {
                $detaler_detaild[]=$assigned_dealer_id['dealers_id'];
             }
     sort($detaler_detaild);        
    }
    }
if(isset($detaler_detaild) && $detaler_detaild!='')
{
     $ij=0;      
     $sql_query_details="SELECT *
    FROM  registration where 
    status='VERIFIED' and usertype='dealership' and (";
    foreach($detaler_detaild as $alldealersget)
       {
        if($alldealersget!='')
        {
    if($ij>0){
           $sql_query_details.="or ";
          } 
           $sql_query_details.="registration_id= $alldealersget ";
        $ij++;
        }
        }
         $sql_query_details.=")";
    $query_result=$this->db->query($sql_query_details);
    if($query_result-> num_rows() > 0)
    {
        $delaer_result= $query_result->result_array();
        $dealer_full_dealer_details=$delaer_result;
          return $dealer_full_dealer_details;
    }
    else
    {
          return false;
    }
}
}
public function delete_dealers($user_id){
  $delete_query=("DELETE FROM user_setting where user_id='$user_id'");  
  $this->db->query($delete_query);
}
public function delete_dealers_seperate($dealeruser_id,$newdealer_id){
    if(isset($newdealer_id)){
    foreach($newdealer_id as $dealers_id){
  $delete_query=("DELETE FROM user_setting where user_id='$dealeruser_id' and  dealers_id =$dealers_id");  
  $delete_assigneddealer=$this->db->query($delete_query);
  }
  }
}
    //add dealers to account managers
    public function update_dealerdetails($user_id,$dealer_name){
    if(isset($dealer_name)){
        foreach($dealer_name as $dealers_id){                
            $sql_dealers=("SELECT dealers_id from user_setting where dealers_id='$dealers_id' and user_id='$user_id'");
            $query_delaers=$this->db->query($sql_dealers);
                if($query_delaers-> num_rows() > 0)
                { 
                }else{        
                    $timestamp=date('Y-m-d',time());
                    $data=array('user_id'=>$user_id,
                    'dealers_id'=>$dealers_id,
                    'time_stamp'=>$timestamp
                    );
                    $insert=$this->db->insert('user_setting',$data);
                }
            }            
        }
    }
    //Show all the assigned dealers
    public function new_removeddealers($mangers_id){
    $sql_qurey=("SELECT dealers_id
    FROM  user_setting
    where  	user_id='$mangers_id'"); 
    $query_delaers=$this->db->query($sql_qurey);
        if($query_delaers-> num_rows() > 0)
            { 
            $dealers_id_get= $query_delaers->result_array();
                if(isset($dealers_id_get) && $dealers_id_get!='')
                {
                    foreach($dealers_id_get as $assigned_dealer_id)
                    {
                        $detaler_detaild[]=$assigned_dealer_id['dealers_id'];
                    }
                sort($detaler_detaild);        
                }
            }
        if(isset($detaler_detaild) && $detaler_detaild!='')
            {
            $ij=0;      
            $sql_query_details="SELECT *
            FROM  registration where 
            status='VERIFIED' and usertype='dealership' and (";
                foreach($detaler_detaild as $alldealersget)
                {
                    if($alldealersget!='')
                    {
                        if($ij>0){
                            $sql_query_details.="or ";
                        } 
                    $sql_query_details.="registration_id= $alldealersget ";
                    $ij++;
                    }
                }
            $sql_query_details.=")";
            $query_result=$this->db->query($sql_query_details);
                if($query_result-> num_rows() > 0)
                {
                    $delaer_result= $query_result->result_array();
                    $dealer_full_dealer_details=$delaer_result;
                    return $dealer_full_dealer_details;
                }
                else
                {
                    return false;
                }
            }    
    }
    //function to get accountmanager details by passing dealer id
    function dealers_assigned_managers($dealer_id){
         $this -> load ->helper('url');
        $sql=("Select user_id from user_setting where dealers_id=$dealer_id");
        $query=$this->db->query($sql);
         if($query -> num_rows() > 0)
       	   {
       	    $returnvalue= $query->result_array();
            return $returnvalue;
            } 
            else
            {
                 return 0;
            } 
            
    }
    function reopen_incomplete_event($dealer_id)
    {
        $advetise_option=$this->input->post('advertising_option');
         $sql=("Select * from events where user_id=$dealer_id and creation_status='incomplete' and event_creation!='minedata'");
         $query=$this->db->query($sql);
         if($query -> num_rows() > 0)
	   {
       	    $returnvalue= $query->result_array();
            
            $return=$returnvalue;
        }
        else
        {
            $return='';
        }
        return $return;
    }
    function get_campign_editdetails($event_id)
    {
         $sql=("Select * from events where event_id=$event_id");
         $query=$this->db->query($sql);
         if($query -> num_rows() > 0)
	   {
       	    $returnvalue= $query->result_array();
            
            $return=$returnvalue;
        }
        else
        {
            $return='';
        }
        return $return;
    }
    
    function insert_event($id)
    {        
        $event_start_date=strtotime($this->input->post('event_start_date'));
        $event_end_date=strtotime($this->input->post('event_end_date'));
        $date_formate=time();
        $advetise_option=$this->input->post('advertising_option');
         $sql=("Select * from events where user_id=$id and creation_status='incomplete' and event_creation!='minedata'");
         $query=$this->db->query($sql);
         if($query -> num_rows() > 0)
       	   {
       	    $returnvalue= $query->result_array();
            foreach($returnvalue as $values)
            {
                $select_event_id=$values['event_id'];
                $updatestatus_status="UPDATE events set 
                event_start_date='$event_start_date',
                event_end_date='$event_end_date',
               	advertising_option='$advetise_option',
                user_id='$id'
                where event_id=$select_event_id";
                $result = $this -> db -> query($updatestatus_status);
                 return $select_event_id;
            }
       	    }
            else{
                $data=array(
                'event_start_date'=>$event_start_date,
                'event_end_date'=>$event_end_date,
                'advertising_option'=>$this->input->post('advertising_option'),
                'event_insert_date'=>$date_formate,
                'user_id'=>$id,
                'event_step'=>'1'
           );
        $this->db->insert("events", $data);
        $last_id = $this->db->insert_id();
        return $last_id;
        }
    }
    function insert_event_minedata($id)
    {        
        $event_start_date=time();
        $event_end_date=time();
        $date_formate=time();
       
        $data=array(
             'event_start_date'=>$event_start_date,
             'event_end_date'=>$event_end_date,
             'event_insert_date'=>$date_formate,
             'user_id'=>$id,
             'event_creation'=>'minedata',
             'event_step'=>'1'
        );
        $this->db->insert("events", $data);
        $last_id = $this->db->insert_id();
        return $last_id;
        
    }
    //function to get event details
    public function get_event_details($user_id){
       $this->load->helper('url'); 
	   $this -> db -> select('*');
	   $this -> db -> from('events');
	   $this -> db -> where('user_id',$user_id);
	   $this -> db -> order_by('event_id','desc');
       $this -> db -> limit(1);
       $result=$this -> db -> get();
        if($result -> num_rows() >0){
			$retrieved=$result->result_array(); 
      	 return $retrieved;
        }
        else
        {
        return 0;
        }
    }
    //sevent setep selection
    function choose_advert_event_select($dealer_id,$event_id){
    $this -> load ->helper('url');
    $sql=("Select event_step from events where event_id=$event_id and  	user_id='$dealer_id'");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
    foreach($returnvalue as $values)
    {
       $event_step_select= $values['event_step'];
    } 
    }
    else
    {
    $event_step_select=0;
    } 
     return $event_step_select;       
    }
    //get dealer brand
    function getdealerbrands($dealer_id){
    $sql=("Select masterbrand from registration where registration_id=$dealer_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
    return $returnvalue;
    }
    else
    {
    return false;
    } 
        
    }
    //insert campine step1
    function insert_campaign_step1($event_id)
    {
        $configuredcamp=$this->input->post('configuredcamp');
        $daterange_from=$this->input->post('daterange_from');
        $daterange_to=$this->input->post('daterange_to');
        $event_insert_id=$this->input->post('event_insert_id');
        $sql=("Select * from epsadvantage_campaign where event_id=$event_id");
        $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
    foreach($returnvalue as $values)
    {
    $campine_id_insert= $values['id'];
   $updatestatus_status="UPDATE epsadvantage_campaign set 
    lead_mining_presets='$configuredcamp',
    past_vehicle_purchase_date_from_range='$daterange_from',
   	past_vehicle_purchase_date_to_range='$daterange_to'
    where id=$campine_id_insert";
    $result = $this -> db -> query($updatestatus_status);
    return $campine_id_insert;  
    }
    }
    
    else
    {
       
        $data=array(
                'lead_mining_presets'=>$configuredcamp,
                'past_vehicle_purchase_date_from_range'=>$daterange_from,
                'past_vehicle_purchase_date_to_range'=>$daterange_to,
                'event_id'=>$event_id,
                'step1_select'=>'1'
    );
        $this->db->insert("epsadvantage_campaign", $data);
        $last_id = $this->db->insert_id();
        return $last_id; 
    }
    }
     //insert campine step1
    function insert_campaign_step1_mine_data($event_id)
    {
        $configuredcamp=$this->input->post('leadmining_type');
      
        $sql=("Select * from epsadvantage_campaign where event_id=$event_id");
        $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
    foreach($returnvalue as $values)
    {
    $campine_id_insert= $values['id'];
   $updatestatus_status="UPDATE epsadvantage_campaign set 
    lead_mining_presets='$configuredcamp'
    where id=$campine_id_insert";
    $result = $this -> db -> query($updatestatus_status);
    return $campine_id_insert;  
    }
    }
    
    else
    {
       
        $data=array(
                'lead_mining_presets'=>$configuredcamp,
                'event_id'=>$event_id,
                'step1_select'=>'1'
    );
        $this->db->insert("epsadvantage_campaign", $data);
        $last_id = $this->db->insert_id();
        return $last_id; 
    }
    }
    function get_campaine_insertvalue()
    {
        $event_id=$this->session->userdata('event_id_get');
          $sql=("Select * from epsadvantage_campaign where event_id=$event_id");
        $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
     $return=$returnvalue;  
    }
    else
    {
        $return='';
    }
    return $return;
    }
    function update_campaign_step1($campign_id)
    {
    $report_type=$this->input->post('report_type');
    $report_fieldname=$this->input->post('report_fieldname');
    
    $report_fieldname1=$this->input->post('report_fieldname_value');
    $updatestatus_status="UPDATE epsadvantage_campaign set advancedoption_select='1',report_type='$report_type',report_fieldname='$report_fieldname',
    report_fieldname1='$report_fieldname1' where id=$campign_id";
    $result = $this -> db -> query($updatestatus_status);
    return true;

    }
    //inseert step 3 of campaine page
     function update_campaign_step3($campine_insert_id)
    {
    $configuredcamp_insert_id=$this->input->post('campaine_insert_id');
    $manufacurer_interesr_rate=$this->input->post('manufacurer_interesr_rate');
    $best_sub_prime_rate=$this->input->post('best_sub_prime_rate');
    $factory_rebate=$this->input->post('factory_rebate');
    $dealership_incentives=$this->input->post('dealership_incentives');
    $excess_vehicle=$this->input->post('excess_vehicle');
    $dealership_promos=$this->input->post('dealership_promos');
    $updatestatus_status="UPDATE epsadvantage_campaign set step3_select='1',
    manufacurer_interesr_rate='$manufacurer_interesr_rate',
    best_sub_prime_rate='$best_sub_prime_rate',
   	factory_rebate='$factory_rebate',
    dealership_incentives='$dealership_incentives',
    excess_vehicle='$excess_vehicle',
    dealership_promos='$dealership_promos',
    step3_select='1'
    where id=$campine_insert_id";
    $result = $this -> db -> query($updatestatus_status);
    return $campine_insert_id;
    }
    function camapign_select($event_id)
    {
    $sql=("Select *  from epsadvantage_campaign where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
    
    }
    else
    {
    $returnvalue='';
    } 
     return $returnvalue;       
    }
    function insertleadlistselction($event_id){
    $equity_scrap=$this->input->post('equity_scrap');
    $model_break_down=$this->input->post('model_break_down');
    $fuel_effciency=$this->input->post('fuel_effciency');
    $wrraenty_scrap=$this->input->post('wrranty_scrap');
    $custom_campain=$this->input->post('custom_campain');
    $fuel_report6=$this->input->post('fuel_report6');
    $lead_mining_presets_select=$this->input->post('lead_mining_presets_select');
    $sql=("Select *  from select_customer_leadlist where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
        $returnvalue= $query->result_array();
        foreach($returnvalue as $values){
        $lead_id=$values['customer_leadlist_id'];   
        $updatestatus_status="UPDATE select_customer_leadlist set equity_scrap='$equity_scrap',
        model_break_down='$model_break_down',
        fuel_effciency='$fuel_effciency',
       	wrranty_scrap='$wrraenty_scrap',
        custom_campain='$custom_campain',
        fuel_efficiency_report6='$fuel_report6',
        lead_mining_presets='$lead_mining_presets_select'
        where event_id=$event_id";
        $result = $this -> db -> query($updatestatus_status); 
        return $lead_id;  
        }
    }
    else{
         $data=array(
                'equity_scrap'=>$equity_scrap,
                'model_break_down'=>$model_break_down,
                'fuel_effciency'=>$fuel_effciency,
                'wrranty_scrap'=>$wrraenty_scrap,
                'custom_campain'=>$custom_campain,
                'fuel_efficiency_report6'=>$fuel_report6,
                'lead_mining_presets'=>$lead_mining_presets_select,
                'event_id'=>$event_id
                );
        $this->db->insert("select_customer_leadlist", $data);
        $last_id = $this->db->insert_id();
        return $last_id; 
    }
    }
    

    function leadsection_select($event_insert_id)
    {
     $sql=("Select *  from  select_customer_leadlist where event_id=$event_insert_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
    $returnvalue= $query->result_array();
    
    }
    else
    {
    $returnvalue=0;
    } 
     return $returnvalue;  
    }
    //mailer setp1 insert
    function insertmailetsetp1($event_id){
    $mailer_size=$this->input->post('mailer_size');
    $smallinvitecost=$this->input->post('smallinvitecost');
    $largeinvitecost=$this->input->post('largeinvitecost'); 
    $invitecost='';
    if($mailer_size=='smallinvites') {
      $invitecost=$smallinvitecost; 
    }
    else{
      $invitecost=$largeinvitecost;   
    }
    $sql=("Select id from select_mailer_options where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
    foreach($returnvalue as $values)
    { 
       $mailer_id=$values['id']; 
       $updatestatus_status="UPDATE select_mailer_options set mailer_size='$mailer_size', invitecost='$invitecost'
        where id=$mailer_id and event_id=$event_id" ;
        $result = $this -> db -> query($updatestatus_status);
        return $mailer_id;
       
    }
    }
    else
    {
    
    $data=array(
                'mailer_size'=>$mailer_size,
                'step1_select'=>1,
                'invitecost'=>$invitecost,
                 'event_id'=>$event_id
                
    );
        $this->db->insert("select_mailer_options", $data);
        $last_id = $this->db->insert_id();
        return $last_id; 
    }
    }
     function insertmailetsetp2($event_id){
     if($this->session->userdata('mailer_id')!=''){
        $data=array(
        'versioning'=>1,
        'step2_select'=>1
        );
        $this->db->insert("select_mailer_options", $data);
        $last_id = $this->db->insert_id();
        return $last_id; 
     } 
     }
    //update mailer step 2
    function updatemailetsetp2($mailer_id){
       $versioning=$this->input->post('versioning'); 
     $updatestatus_status="UPDATE select_mailer_options set versioning='$versioning',
    step2_select='1'
    where id=$mailer_id";
    $result = $this -> db -> query($updatestatus_status);
    return true; 
     }
     function updatemailetsetp3($mailer_id){
     $auto_pen=$this->input->post('auto_pen');
    $insert_cardstock=$this->input->post('insert_cardstock');
    $insert_paperstock=$this->input->post('insert_paperstock');
    $variable_image=$this->input->post('variable_image');
    $colored_envelop=$this->input->post('colored_envelop');
    $data['event_insert_id']=$this->session->userdata('event_id_get');
    $updatestatus_status="UPDATE select_mailer_options set autopen='$auto_pen',
    insert_cardstock='$insert_cardstock',
    insert_paperstock='$insert_paperstock',
    variable_imaging='$variable_image',
    colored_envelopes='$colored_envelop',
    step3_select='1'
    where id=$mailer_id";
     $result = $this -> db -> query($updatestatus_status);
    return true; 
    }
      function updatemailetsetp4($mailer_id){
      $upgrade_package=$this->input->post('upgrade_package');
      $data['event_insert_id']=$this->session->userdata('event_id_get');
      $updatestatus_status="UPDATE select_mailer_options set  upgrader_package='$upgrade_package',
      step4_select='1'
      where id=$mailer_id";
      $result = $this -> db -> query($updatestatus_status);
      return true; 
      }
    function get_campign_details($event_id,$user_id){
    $sql=("Select * from epsadvantage_campaign where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
        $returnvalue= $query->result_array();
    }else{
        $returnvalue=0;
    } 
        return $returnvalue;   
    }    
    function get_mailout_option_details($event_id){
    $sql=("Select * from select_mailer_options where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
        $returnvalue= $query->result_array();
    }else
    {
        $returnvalue=0;
    } 
        return $returnvalue;   
    } 
   function update_event_complete($event_id){ 
    $updatestatus_status="UPDATE events set 
   	creation_status='complete'
    where event_id=$event_id";
    $result = $this -> db -> query($updatestatus_status);
    return $event_id;
    } 
    //insert lead customer details
    function lead_customer_data_insert($lead_id,$report_type,$customer_id){
        $sql=("Select * from leadlist_customer_data where customer_leadlist_id=$lead_id and lead_customer_id='$customer_id' and lead_type='$report_type'");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
          return false;  
        }
        else
        {
        $data=array(
                    'lead_customer_id'=>$customer_id,
                    'customer_leadlist_id'=>$lead_id,
                    'lead_type'=>$report_type
                   );
            $this->db->insert("leadlist_customer_data", $data);
            $last_id = $this->db->insert_id();
            return $last_id; 
        }
    } 
    function leadlistdelete($lead_id)
    {
       $sql_delete=("Delete from leadlist_customer_data where customer_leadlist_id=$lead_id");
       $query=$this->db->query($sql_delete);
       return true; 
    }
    //get dealers selected events
    function get_dealer_events($dealer_id){
    $this -> load ->helper('url');
    $sql=("SELECT * FROM events, epsadvantage_campaign WHERE events.event_id=epsadvantage_campaign.event_id AND events.user_id=$dealer_id order by event_start_date desc");
     $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
        $returnvalue= $query->result_array();
        return $returnvalue;
    }
    else{
        return false; 
    } 
         
    }
    function get_dealer_events_details($dealer_id){
    $this -> load ->helper('url');
    $sql=("SELECT * FROM events where user_id=$dealer_id and event_creation!='minedata' order by event_start_date desc");
    $query=$this->db->query($sql);
        if($query -> num_rows() > 0){
            $returnvalue= $query->result_array();
            return $returnvalue;
        }
        else{
            return false; 
        } 
    
    }
    //get customer details
    function get_customer_details($event_id,$lead_type){
    $this -> load ->helper('url');
    if($lead_type==1){
        $condition='AND leadlist_customer_data.lead_type=1';
    }
    else if($lead_type==2){
        $condition='AND  leadlist_customer_data.lead_type=2';
    }
    else if($lead_type==3){
        $condition='AND  leadlist_customer_data.lead_type=3';
    }
     else if($lead_type==4){
        $condition='AND  leadlist_customer_data.lead_type=4';
    }
     else if($lead_type==5){
        $condition='AND  leadlist_customer_data.lead_type=5';
    }
     else if($lead_type==6){
        $condition='AND  leadlist_customer_data.lead_type=6';
    }
    else{
        $condition='';
    }
    $sql=("SELECT * FROM  select_customer_leadlist, leadlist_customer_data WHERE select_customer_leadlist.customer_leadlist_id=leadlist_customer_data.customer_leadlist_id AND  select_customer_leadlist.event_id=$event_id $condition");
     $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
        $returnvalue= $query->result_array();
        return $returnvalue;
    }
    else{
        return false; 
    } 
         
    }
    //function to get the description of the particular report type
   function get_report_description($report_type){
        $this -> load ->helper('url');
        $sql=("Select description from  report_description where field_name='$report_type'");
        $query=$this->db->query($sql);
            if($query -> num_rows() > 0){
            $row = $query->row_array();
            return $row['description'];
            }
            else{
            return false; 
            } 
        }
   //function to get the field values
   function get_reportfieldvalues($report_type,$id){
    $this -> load ->helper('url');
        $sql=("Select * from  epsadvantage_campaign where report_type='$report_type' and id=$id");
        $query=$this->db->query($sql);
            if($query -> num_rows() > 0){
            $row = $query->result_array();
            return $row;
            }
            else{
            return false; 
            } 
        }
   
   function get_advertising_option($options){
   switch ($options){
   case "1":
   $return ='Conquest';
   break;
   case "2":
   $return ='Eps Advantage';
   break;
   case "3":
   $return ='Upgrader';
   break;
    }
 return $return;
 } 
    function get_report_type_name($options=''){
   switch ($options){
   case "drive_type":
   $return ='Drive Type';
   break;
   case "2":
   $return ='Eps Advantage';
   break;
   case "3":
   $return ='Upgrader';
   break;
   default:
   $return ='Unknown';
   break;
    }
 return $return;
 } 
   function get_lead_type_name($options=''){
   switch ($options){
   case "custom_campaign":
   $return ='Advanced Options';
   break;
   case "equity_scrape":
   $return ='Equity Scrape';
   break;
   case "model_breakdown":
   $return ='Model Breakdown';
   break;
   case "effiecency":
   $return ='Fuel Efficiency';
   break;
   case "warranty_scrape":
   $return ='Warranty Scrape';
   break;
   default:
   $return ='Unknown';
   break;
    }
 return $return;
 }
    function get_vehicle_class_feild_values($options){
   switch ($options){
   case "full_size_cars":
   $return ='Full-size cars';
   break;
   case "mid_size_cars":
   $return ='mid-size cars';
   break;
   case "small_cars":
   $return ='small cars';
   break;
   case "suvs":
   $return ='SUV';
   break;
   case "crossovers":
   $return ='Crossovers';
   break;
   case "trucks":
   $return ='Truck';
   break;
   case "vans":
   $return ='Van';
   break;
   case "green_cars":
   $return ='Green Cars';
   break;
   default:
   $return ='';
   break;
    }
 return $return;
 }
     function get_vehicle_class_feild_values_demo_dealer($options){
   switch ($options){
   case "full_size_cars":
   $return ='Full-size cars';
   break;
   case "mid_size_cars":
   $return ='mid-size cars';
   break;
   case "small_cars":
   $return ='small cars';
   break;
   case "suvs":
   $return ='SUVS';
   break;
   case "crossovers":
   $return ='Crossovers';
   break;
   case "trucks":
   $return ='Trucks';
   break;
   case "vans":
   $return ='Vans';
   break;
   case "green_cars":
   $return ='Green Cars';
   break;
   default:
   $return ='';
   break;
    }
 return $return;
 }  
 function getleadcount($event_id,$lead_type){
    $this -> load ->helper('url');
 
    $query=("SELECT COUNT(*) AS `numrows` FROM  select_customer_leadlist, leadlist_customer_data WHERE select_customer_leadlist.customer_leadlist_id=leadlist_customer_data.customer_leadlist_id AND  select_customer_leadlist.event_id=$event_id AND leadlist_customer_data.lead_type='$lead_type'");
    $query_count=$this->db->query($query);
    $count= $query_count->row ()->numrows;
    return $count;
 }
 //function to get the count of customer lead
 function get_customer_leadcount($event_id){
     $this -> load ->helper('url');
    $query=("SELECT COUNT(*) AS `numrows` FROM  select_customer_leadlist, leadlist_customer_data WHERE select_customer_leadlist.customer_leadlist_id=leadlist_customer_data.customer_leadlist_id AND  select_customer_leadlist.event_id=$event_id ");
    $query_count=$this->db->query($query);
    $count_query= $query_count->row ()->numrows;
    return $count_query; 
 }
  function mailout_option_select($event_insert_id)
  {
  $sql=("Select *  from  select_mailer_options where event_id=$event_insert_id");
  $query=$this->db->query($sql);
  if($query -> num_rows() > 0){
    $returnvalue= $query->result_array();
  }
  else{
  $returnvalue=0;
  } 
  return $returnvalue;  
  }
  function getreporttype($report_type)
  {
    switch ($report_type){
   case "vehicle_class":
   $return ='Vehicle Class';
   break;
   case "drive_type":
   $return ='Drive Type';
   break;
   case "fuel_economy":
   $return ='Fuel Economy';
   break;
   case "trade_in_value":
   $return ='Trade In Value';
    break;
   case "out_warranty":
   $return ='Vehicle Out of Warranty ';
   break;
   case "finance_rate":
   $return ='Finance Rate (APR)';
   break;
   case "monthly_payment":
   $return ='Monthly Payment Range';
   break;
   case "specific_model":
   $return ='Specific Model Pull  ';
   break;
   case "power_focus":
   $return ='Power Focus ';
   break;
   case "fue_type":
   $return ='Fuel Type ';
   break;
   case "local_town":
   $return ='Local or Out of town ';
   break;
   case "used_new_purchaser":
   $return ='Used vs. New Vehicle Purchase ';
   break;
   case "dealership_brand":
   $return ='Competitors Vehicle Owners';
   break;
   default:
   $return ='Unknown';
   break;
    }
   return $return;
  }
    function getreporttype_report_generation_type($report_type)
  {
    switch ($report_type){
   case "vehicle_class":
   $return ='VehicleClass';
   break;
   case "drive_type":
   $return ='DriveType';
   break;
   case "fuel_economy":
   $return ='FuelEconomy';
   break;
   case "trade_in_value":
   $return ='TradeInValue';
    break;
   case "out_warranty":
   $return ='Vehicle-Warranty ';
   break;
   case "finance_rate":
   $return ='FinanceRate';
   break;
   case "monthly_payment":
   $return ='MonthlyPaymentRange';
   break;
   case "specific_model":
   $return ='SpecificModelPull  ';
   break;
   case "power_focus":
   $return ='PowerFocus ';
   break;
   case "fue_type":
   $return ='FuelType ';
   break;
   case "local_town":
   $return ='Local-Outoftown ';
   break;
   case "used_new_purchaser":
   $return ='Used-New-VehiclePurchase ';
   break;
   case "dealership_brand":
   $return ='CompetitorsVehicleOwners';
   break;
   default:
   $return ='Unknown';
   break;
    }
   return $return;
  }
  function get_campign_id($event_id){
    $sql=("select id from  epsadvantage_campaign where event_id=$event_id");
    $result=$this->db->query($sql);
    if($result -> num_rows() > 0){
       $row = $result->row_array();
            return $row['id'];
        }else{
            return 0;
        }
    
  }
  public function get_report_field_values($event_id){
    $campine_event_get=$this->camapign_select($event_id);
    
    if(isset($campine_event_get) && $campine_event_get!=''){
    foreach($campine_event_get as $values){
        $report_name=$values['report_type'];
       
    if($report_name=='vehicle_class'){
        $field_name='Vehicle Class';
        if($values['report_fieldname']=='null'){
          $value1='N/A';
        }else{
        $get_value1=ucwords($values['report_fieldname']);
        $value1 = str_replace("_"," ", $get_value1);
        }
        $field_name2='';
        $value2='';
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }elseif($report_name=='drive_type'){
        $field_one='yes';
        $field_name='Drive Type';
        $value1=ucfirst($values['report_fieldname']);
        $field_name2='';
        $value2='';
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }elseif($report_name=='fuel_economy'){
        $field_name='From';
        if($values['report_fieldname']=='' || $values['report_fieldname']=='undefined'){
            $value1='N/A';
        }else{
        $value1=$values['report_fieldname'];
        }
        $field_name2='To :';
        if($values['report_fieldname1']=='' || $values['report_fieldname1']=='undefined'){
            $value2='N/A';
        }else{
        $value2=$values['report_fieldname1'];
        }
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }
    elseif($report_name=='trade_in_value'){
        $field_name='From';
         if($values['report_fieldname']=='' || $values['report_fieldname']=='undefined'){
            $value1='N/A';
        }else{
        $value1=$values['report_fieldname'];
        }
        $field_name2='To :';
        if($values['report_fieldname1']=='' || $values['report_fieldname1']=='undefined'){
            $value2='N/A';
        }else{
        $value2=$values['report_fieldname1'];
        }
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }
    elseif($report_name=='finance_rate'){
        $field_name='Max';
         if($values['report_fieldname']=='' || $values['report_fieldname']=='undefined'){
            $value1='N/A';
        }else{
        $value1=$values['report_fieldname'];
        }
        if($values['report_fieldname1']=='' || $values['report_fieldname1']=='undefined'){
            $value2='N/A';
        }else{
        $value2=$values['report_fieldname1'];
        }
        $field_name2='Min :';
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }
    elseif($report_name=='power_focus'){
        $field_name='Power Focus';
        if($values['report_fieldname']=='null'){
            $value1='N/A';
        }else{
        $get_value1=ucwords($values['report_fieldname']);
        $value1 = str_replace("_"," ", $get_value1);
        }
        $field_name2='';
        $value2='';
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }elseif($report_name=='fue_type'){
        $field_name='Fuel Type';
        if($values['report_fieldname']=='undefined' || $values['report_fieldname']==''){
            $value1='N/A';
        }else{
        $value1=ucfirst($values['report_fieldname']);
        }
        if($values['report_fieldname1']=='null'){
            $value2='N/A';
        }else{
        $get_value2=ucwords($values['report_fieldname1']);
        $value2 = str_replace("_"," ", $get_value2);
        }
        $field_name2='Field name :';
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }
    elseif($report_name=='local_town'){
        $field_name='Local vs Out of Town';
        $get_value=$values['report_fieldname'];
        if($get_value=='undefined'){
            $value1='N/A';
        }else{
        if($get_value=='out_of_town'){
          $value1='Out of Town';
        }
        elseif($get_value=='local'){
            $value1='Local';
        }
        }
        $field_name2='Field name : ';
        if($values['report_fieldname1']=='null'){
            $value2='N/A';
        }else{
        $get_value2=ucwords($values['report_fieldname1']);
        $value2 = str_replace("_"," ", $get_value2);
        }
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }elseif($report_name=='used_new_purchaser'){
        $field_name='Used vs New Purchaser';
        $value1=ucfirst($values['report_fieldname']);
        $field_name2='Field name : ';
        if($values['report_fieldname1']=='null'){
            $value2='N/A';
        }else{
        $get_value2=ucwords($values['report_fieldname1']);
        
        $value2 = str_replace("_"," ", $get_value2);
        }
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
       }
       elseif($report_name=='monthly_payment'){
        $field_name='Min';
        if($values['report_fieldname']=='null' || $values['report_fieldname']=='undefined'){
            $value1='N/A';
        }else{
        $value1=$values['report_fieldname'];
        }
         if($values['report_fieldname1']=='null' || $values['report_fieldname1']=='undefined'){
            $value2='N/A';
        }else{
        $value2=$values['report_fieldname1'];
        }
        $field_name2='Max :';
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }
   	elseif($report_name=='dealership_brand'){
        $field_name='Competitors Vehicle Owners';
        $value1='';
        $value2='';
        $field_name2='Field name :';
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
       }
       elseif($report_name=='specific_model'){
        $field_name='Specific Model Pull';
        $value1='';
        $value2='';
        $field_name2='Field name :';
        $result=$field_name .' : '. $value1 .'<br/>'. $field_name2 . $value2;
    }
    else{
      $result=0;  
    }
    }
    return $result;
    }else{
        return 0;
    }
    
  }
    //get customer id
    function get_lead_customer_id($lead_id,$lead_type){
    $sql=("SELECT lead_customer_id FROM  leadlist_customer_data WHERE  customer_leadlist_id =$lead_id and lead_type=$lead_type");
     $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
        $returnvalue= $query->result_array();
        return $returnvalue;
    }
    else{
        return false; 
    } 
       
    }
  //get pbs customer data 
   function get_pbs_details($customer_id){
    
    $sql=("select * from  eps_data where id=$customer_id");
    $result=$this->db->query($sql);
    if($result -> num_rows() > 0){
       $row = $result->result_array();
            return $row;
        }else{
            return 0;
        }
    }
    
    function get_leadlist_details_with_event_id($event_id){
    $sql_leadlist=("SELECT lead_customer_id FROM  select_customer_leadlist, leadlist_customer_data WHERE select_customer_leadlist.customer_leadlist_id=leadlist_customer_data.customer_leadlist_id AND  select_customer_leadlist.event_id=$event_id");
    $query_leadlist=$this->db->query($sql_leadlist);
    if($query_leadlist -> num_rows() > 0){
    $returnvalue= $query_leadlist->result_array();
        $ij=0; 
        $sql=("select * from  pbs_customer_data where(");
        foreach($returnvalue as $values){
        if($values!=''){
            if($ij>0){
                $sql.="or ";
            } 
                $sql.="id=$values[lead_customer_id] ";
                $ij++;  
        }
        }
        $sql.=")";
        $quer = $this->db->query($sql);
        $returnvalue= $quer->result_array();
   } 
   else{
    $returnvalue='';
   }
   return $returnvalue;
   }
   function getleadgrouptitle($leadtype){
   switch ($leadtype){
       case "model_breakdown":
       $model_break_down=array("Report Group 1 (Vehicle Class: All cars (full, mid, economy, green/hybrid))","Report Group 2 (Vehicle Class: SUVs & Crossovers)","Report Group 3 (Vehicle Class: Trucks)","Report Group 4 (Vehicle Class: Vans (including minivans if it is separate))","All remaining leads");
       $return =$model_break_down;
       break;
       case "effiecency":
       $effiecency=array("Report Group 1 (High efficiency cars)","Report Group 2 (Low efficiency cars)","Report Group 3 (High Efficiency SUVs, Vans, Crossovers)","Report Group 4 (Low Efficiency SUVs, Vans, Crossovers)","Report Group 5 (High Efficiency Trucks)","Report Group 6");
       $return =$effiecency;
       break;
       case "warranty_scrape":
       $warranty_scrape=array("Report Group 1(Powertrain Warranty = Expired)","Report Group 2 (Powertrain warranty = <6 months remaining)","Report Group 3 (Basic vehicle warranty = Expired)","Report Group 4 (Basic Vehicle Warranty = <6 months remaining)","Report Group 5 (Basic vehicle warranty = >6 months remaining)");
       $return =$warranty_scrape;
       break;
       case "custom_campaign":
       $custom_campaign=array("Report Group 1","Report Group 2 ","Report Group 3 ","Report Group 4 ","Report Group 5 ");
       $return =$custom_campaign;
       break;
       case "equity_scrape":
       $equity_scrape=array("Report Group 1 (Positive Equity, No Payments Remaining)","Report Group 2 (Positive Equity, Sub-prime financing)","Report Group 3 (Negative Equity, Sub Prime Financing)","Report Group 4 (Positive Equity, Regular Interest Financing)","Group 5 (Negative Equity. Regular Interest Financing)");
       $return =$equity_scrape;
       break;
       default:
       $return ='';
       break;
    }
    return $return;
   }
      function getleadgrouptitle_report($leadtype){
   switch ($leadtype){
       case "model_breakdown":
       $model_break_down=array("Report1-VehicleClass-Allcars","Report2-VehicleClass-SUVs&Crossovers)","ReportGroup3-VehicleClass-Trucks","ReportGroup4-VehicleClass-Vans)","Allremainingleads");
       $return =$model_break_down;
       break;
       case "effiecency":
       $effiecency=array("Report1-Highefficiencycars","Report2-Lowefficiencycars","Report3-HighEfficiency-SUVs-Vans-Crossovers","Report4-LowEfficiency-SUVs-Vans-Crossovers","Report5-HighEfficiency-Trucks","ReportGroup6");
       $return =$effiecency;
       break;
       case "warranty_scrape":
       $warranty_scrape=array("Report1-Expired-PowertrainWarranty","Report2-Powertrainwarranty-Lessthan-6months","Report3-Expired-Basicvehiclewarranty","Report4-BasicVehicleWarranty-Lessthan-6months","Report5-Basicvehiclewarranty-Greaterthan-6months");
       $return =$warranty_scrape;
       break;
       case "custom_campaign":
       $custom_campaign=array("ReportGroup 1","ReportGroup 2 ","ReportGroup 3 ","Report Group 4 ","Report Group 5 ");
       $return =$custom_campaign;
       break;
       case "equity_scrape":
       $equity_scrape=array("Report1-PositiveEquity)","Report2-PositiveEquity-Sub-prime financing","Report3-NegativeEquity-Sub Prime Financing","Report4PositiveEquity-RegularInterestFinancing","Report5-NegativeEquity-RegularInterestFinancing");
       $return =$equity_scrape;
       break;
       default:
       $return ='';
       break;
    }
    return $return;
   }
   function get_cost_of_smallinvites($lead_count){
    $price_array=array("100-199"=>"3.78","200-299"=>"2.50","300-399"=>"2.06","400-499"=>"2.06","500-599"=>"1.91","600-699"=>"1.84","700-799"=>"1.58","800-899"=>"1.58","900-999"=>"1.58","1000-1499"=>"1.34","1500-1999"=>"1.19","2000"=>"1.09");
    if($lead_count!=''){
        
        foreach ($price_array as $key=>$value){
            $price_range=explode('-',$key);
            if($price_range[0]!='' && $price_range[0]!='2000'){
                if($lead_count>=$price_range[0] && $lead_count<=$price_range[1]){
                $return =$value;
                return $return;
                
                }
            }
            else if($key=='2000' && $lead_count>='2000'){
                $return =$value;
                return $return;
            }
            else{
                $return =3.78;
                return $return;
            }
        }
    }
    else{
        $return ='';
        return $return;
     }
     
 }
    function get_cost_of_largerinvites($lead_count){
    $price_array=array("100-199"=>"3.88","200-299"=>"2.62","300-399"=>"2.19","400-499"=>"2.19","500-599"=>"1.98","600-699"=>"1.92","700-799"=>"1.72","800-899"=>"1.72","900-999"=>"1.72","1000-1499"=>"1.48","1500-1999"=>"1.33","2000"=>"1.23");
    if($lead_count!=''){
        foreach ($price_array as $key=>$value){
            $price_range=explode('-',$key);
            if($price_range[0]!='' && $price_range[0]!='2000'){
                if(($lead_count>=$price_range[0]) && ($lead_count<=$price_range[1])){
                $return =$value;
                return $return;
                }
            }
            else if($key=='2000' && $lead_count>='2000'){
                $return =$value;
                return $return;
            }
            else{
                $return =3.88;
                return $return;
            }
        }
    }
    else{
    $return ='';
    return $return;
    }
    
 }
 function get_cost_of_additional_options($options){
 $additional_options_array=array("colored_envelopes"=>"0.15","autopen"=>"0.36","insert_cardstock"=>"0.60","insert_paperstock"=>"0.55","variable_imaging"=>"0.20","versioning"=>"85.00");
  switch ($options){
       case "colored_envelopes":
       $option_price=$additional_options_array[$options];
       $return =$option_price;
       break;
       case "autopen":
       $option_price=$additional_options_array[$options];
       $return =$option_price;
       break;
       case "insert_cardstock":
       $option_price=$additional_options_array[$options];
       $return =$option_price;
       break;
       case "insert_paperstock":
       $option_price=$additional_options_array[$options];
       $return =$option_price;
       break;
       case "variable_imaging":
       $option_price=$additional_options_array[$options];
       $return =$option_price;
       break;
       case "versioning":
       $option_price=$additional_options_array[$options];
       $return =$option_price;
       break;
       default:
       $return ='';
       break;
       
  }
  return $return;
 }
  function report_field_settings($options){
    switch ($options){
       case "gas":
       $option_text='Gas';
       $return =$option_text;
       break;
       case "gas":
       $option_text='Gas';
       $return =$option_text;
       break;
       case "diesel":
       $option_text='Diesel';
       $return =$option_text;
       break;
       case "hybrid":
       $option_text='Other';
       $return =$option_text;
       break;
       case "local":
       $option_text='Local';
       $return =$option_text;
       break;
       case "out_of_town":
       $option_text=' Out Of Town';
       $return =$option_text;
       break;
       default:
       $return ='';
       break;
       
  }
  return $return;
 }
 public function get_lead_mining_presets_leadlist($event_id)
{
    $event_id=$this->session->userdata('event_id_get');
    $sql=("Select lead_mining_presets from select_customer_leadlist where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $returnvalue= $query->result_array();
    foreach($returnvalue as $value){
        
    
    $return=$value['lead_mining_presets'];  
    }
    }
    else{
    $return='';
    }
    return $return;
}
    function insert_advance_option_group_selection(){
    $report_name=$this->input->post('report_name'); 
    $value_get_1=$this->input->post('value_get_1');
    $insert='';
    $commoncheck=0; 
    if(is_array($value_get_1) && $value_get_1!=''){
    foreach($value_get_1 as $values){
        if($commoncheck>0){
        $insert.=',';
        }
         $insert.=$values;
         $commoncheck++;   
        }
        $feild_value_insert=$insert;
     }
     else{
        $feild_value_insert=$value_get_1;
     }   
    $value_get_2=$this->input->post('value_get_2');
    $feild_name_1=$this->input->post('feild_name_1');
    $feild_name_2=$this->input->post('feild_name_2');
    $event_id=$this->input->post('event_id');
    $group_select=$this->input->post('id');
    $sql=("Select id from  advance_options_group_selection where event_id=$event_id and group_name=$group_select");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
   {
    $returnvalue= $query->result_array();
    foreach($returnvalue as $values)
    { 
       $mailer_id=$values['id']; 
       $updatestatus_status="UPDATE advance_options_group_selection set report_type='$report_name',
       value1='$feild_value_insert',
       value2='$value_get_2',
       field_name1='$feild_name_1',
       field_name2='$feild_name_2'
       where  group_name=$group_select and event_id=$event_id";
       $result = $this -> db -> query($updatestatus_status);
       return $mailer_id;
       
    }
    }
    else
    {
    
    $data=array(
                'report_type'=>$report_name,
                'value1'=>$feild_value_insert,
                'value2'=>$value_get_2,
                'field_name1'=>$feild_name_1,
                'field_name2'=>$feild_name_2,
                'group_name'=>$group_select,
                'event_id'=>$event_id
                
    );
        $this->db->insert("advance_options_group_selection", $data);
        $last_id = $this->db->insert_id();
        return $last_id; 
    }
    
    }
public function getgroupname_advanced_option_with_event_id($event_id)
{
    
    $sql=("Select * from advance_options_group_selection where event_id=$event_id order by group_name asc");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $return= $query->result_array();
   }
    else{
    $return='';
    }
    return $return;
}
public function getgroupname_title_advanced_option_with_event_id($event_id)
{
    
    $sql=("Select report_type from advance_options_group_selection where event_id=$event_id order by group_name asc");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $return= $query->result_array();
    foreach($return as $value){
    $report_type=$this->getreporttype($value['report_type']);
    $report_title[]=$report_type; 
    }
    
   }
    else{
    $report_title='';
    }
    return $report_title;
}
public function getgroupname_advanced_option($event_id,$group)
{
    
    $sql=("Select * from advance_options_group_selection where event_id=$event_id and group_name='$group'");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $return= $query->result_array();
   }
    else{
    $return='';
    }
    return $return;
}
public function get_advanced_option_group_details($event_id,$group,$dealer_id_upload_data,$group_id)
{
   
   $return_result='';
   $purchase_date_range_from='';
   $purchase_date_range_to='';
   //getting purchase to and from range
   $sql_date_range=("Select past_vehicle_purchase_date_from_range,past_vehicle_purchase_date_to_range from  epsadvantage_campaign where event_id=$event_id");
    $query_date_range=$this->db->query($sql_date_range);
    if($query_date_range -> num_rows() > 0){
    $returnvalue_get_date_range=$query_date_range->result_array();
    foreach($returnvalue_get_date_range as $purchase_date_range){
        $purchase_date_range_from=$purchase_date_range['past_vehicle_purchase_date_from_range'];  
        $purchase_date_range_to=$purchase_date_range['past_vehicle_purchase_date_to_range'];  
    }
    }
    $date=time();
    $new_purchase_to_range = $purchase_date_range_to*12;
    $newdate_purchase_range_to_date_compare = strtotime ( '-'.$new_purchase_to_range.' month' , $date ) ;   
    $new_purchase_from_range =$purchase_date_range_from*12;
    $newdate_purchase_range_from_date_compare = strtotime ( '-'.$new_purchase_from_range.' month' , $date  ) ;   
    $sql=("Select * from advance_options_group_selection where event_id=$event_id and group_name='$group'");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $returnvalue= $query->result_array();
    foreach($returnvalue as $values){
    //vehicle cass
    if($values['report_type']=='vehicle_class'){
       $feild_value=$values['value1'];
       $condition='';
       $feild_values_explode=explode(',',$feild_value);
       $commoncheck=0;
       if(is_array($feild_values_explode) && $feild_values_explode!=''){
        if($group_id!=''){
       $condition.="id NOT IN ($group_id) AND ";  
       }
       $condition.='(';
       foreach($feild_values_explode as $value_select){
       if($commoncheck>0){
       $condition.=' OR ';
       }
       if($dealer_id_upload_data=='198'){
       $condition_name=$this -> get_vehicle_class_feild_values_demo_dealer($value_select);
       if($value_select=='full_size_cars' ){
       $condition.="(vehicletype='Full-size cars')";
       }
       else if($value_select=='mid_size_cars' ){
       $condition.="(vehicletype='mid-size cars')";   
       }
       else if($value_select=='small_cars'){
                      $condition.="(vehicletype='small cars')";   
       }
       else{
       $condition.='(vehicletype='."'".$condition_name."')";
       }
       $commoncheck++;   
       }
       else{
       $condition_name=$this -> get_vehicle_class_feild_values($value_select);
       if($value_select=='full_size_cars' ){
       $condition.="(vehicletype='Car' AND vehiclesize='Large')";
       }
       else if($value_select=='mid_size_cars' ){
       $condition.="(vehicletype='Car' AND vehiclesize='Midsize')";   
       }
       else if($value_select=='small_cars'){
       $condition.="(vehicletype='Car' AND vehiclesize='Compact')";   
       }
        else if($value_select=='vans' ){
            $condition.="(vehicletype='Minivan')";   
        }
       else{
       $condition.='(vehicletype='."'".$condition_name."')";
       }
       $commoncheck++; 
       } 
       }
       $condition.=')'; 
       $condition.=' AND '; 
       }
       else{
       $condition='';   
       }
       $sql_get_query=("SELECT  * FROM eps_data  WHERE
       $condition  dealership_id='$dealer_id_upload_data' AND (
       first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare OR 
       contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare)
        order by id ASC");
      //echo $sql_get_query;
       $result=$this->db->query($sql_get_query);
       if($result -> num_rows() >0){
       $return_result= $result->result_array();
       } 
       else{
       $return_result='';
       }
       
}
//drive type
    else if($values['report_type']=='drive_type'){
        $condition='';
        $feild_value=$values['value1'];
        if($group_id!=''){
        $condition.="id  NOT IN ($group_id)  AND ";  
        }
        $sql_get_query=("SELECT  * FROM eps_data  WHERE $condition
        (transmissiontype='$feild_value'  and dealership_id='$dealer_id_upload_data' AND (
        first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
        OR 
        contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare )) 
        order by id ASC ");
       	$result=$this->db->query($sql_get_query);
        if($result -> num_rows() >0){
        $return_result= $result->result_array();
        } 
        else{
        $return_result='';
        }   
    }
    //fuel ecconomy
    else if($values['report_type']=='fuel_economy'){
        $return_result='';
        $condition='';
        if($values['value1']!=''){
        $feild_value1=$values['value1'];
        }
        else{
        $feild_value1=0;  
        }
        if($values['value2']!=''){
        $feild_value2=$values['value2'];
        }
        else{
        $feild_value2=0;  
        }
        if($group_id!=''){
        $condition.=" id  NOT IN ($group_id) AND ";  
        }
        $sql_get_query=("SELECT  * FROM eps_data  WHERE $condition
        (littercombined between  $feild_value1  and $feild_value2 and dealership_id='$dealer_id_upload_data' AND (
        first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
        OR 
        contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare )) 
        order by id ASC ");
       	$result=$this->db->query($sql_get_query);
        if($result -> num_rows() >0){
        $return_result=$result->result_array();
        } 
        else{
        $return_result='';
        } 
        //echo $sql_get_query;  
    }
    //trade in value
    else if($values['report_type']=='trade_in_value'){
        $return_result='';
        $condition='';
        if($values['value1']!=''){
        $feild_value1=$values['value1'];
        }
        else{
        $feild_value1=0;  
        }
        if($values['value2']!=''){
        $feild_value2=$values['value2'];
        }
        else{
        $feild_value2=0;  
        }
        if($group_id!=''){
        $condition.="id  NOT IN ('$group_id') AND ";  
        }
        $sql_get_query=("SELECT  * FROM eps_data  WHERE $condition
        (tradeinvalue between  '$feild_value1'  and '$feild_value2' and dealership_id='$dealer_id_upload_data' AND (
        first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
        OR 
        contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare)) 
         order by id ASC ");
       	$result=$this->db->query($sql_get_query);
        if($result -> num_rows() >0){
        $return_result= $result->result_array();
        } 
        else{
        $return_result='';
        }
    }
    //finance rate    
    else if($values['report_type']=='finance_rate'){
        $return_result=''; 
        $condition='';   
        if($values['value1']!=''){
        $feild_value1=$values['value1'];
        }
        else{
        $feild_value1=0;  
        }
        if($values['value2']!=''){
        $feild_value2=$values['value2'];
        }
        else{
        $feild_value2=0;  
        }
        if($group_id!=''){
        $condition.="id  NOT IN ($group_id) AND ";  
        }
        $sql_get_query=("SELECT  * FROM eps_data  WHERE $condition
   	    (apr between  $feild_value1  and $feild_value2 and dealership_id='$dealer_id_upload_data' AND (
        first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
        OR 
        contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
        ))
        order by id ASC ");
      	$result=$this->db->query($sql_get_query);
        if($result -> num_rows() >0){
        $return_result= $result->result_array();
        } 
        else{
        $return_result='';
        }   
    }
    //monthly payment
    else if($values['report_type']=='monthly_payment'){
        $return_result='';
        $condition='';
        if($values['value1']!=''){
        $feild_value1=$values['value1'];
        }
        else{
        $feild_value1=0;  
            }
        if($values['value2']!=''){
        $feild_value2=$values['value2'];
        }
        else{
        $feild_value2=0;  
        }
        if($group_id!=''){
        $condition.="id   NOT IN ($group_id) AND ";  
        }
        $sql_get_query=("SELECT  * FROM eps_data  WHERE $condition
        monthly_payment  between  $feild_value1  and $feild_value2 and dealership_id='$dealer_id_upload_data' AND (
        first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
        OR 
        contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare)
        order by id ASC ");
       	$result=$this->db->query($sql_get_query);
        if($result -> num_rows() >0){
        $return_result= $result->result_array();
        } 
        else{
        $return_result='';
        }   
    }
    //fuel type
    else if($values['report_type']=='fue_type'){
        $return_result='';
        $feild_value=$values['value1'];
        $feild_name=$values['field_name1'];
        $condition='';
        $feild_values_explode='';
        if($feild_value!=''){
        $feild_values_explode=explode(',',$feild_value);
        }
       
        $commoncheck=0;
        if($group_id!=''){
        $condition.="id  NOT IN ($group_id) AND ";  
        }
        $condition.='(';
        if($dealer_id_upload_data=='198'){
        if($feild_name!='hybrid')
        {
        $condition.='buyer_first_name='."'".$feild_name."'";    
        }
        else{
        $condition.='buyer_first_name='."'Other/Unknown' ";
        }   
        }
        else{
        if($feild_name!='hybrid')
        {
            $condition.='enginefueltype='."'".$feild_name."' AND ";
        }
        else{
        $condition.="(enginefueltype!='gas' AND enginefueltype!='desel') AND ";
        }
        }
        if(is_array($feild_values_explode) && $feild_values_explode!=''){
        $condition.='(';
        foreach($feild_values_explode as $value_select){
        if($commoncheck>0){
        $condition.='OR ';
        }
        if($dealer_id_upload_data=='198'){
        $condition_name=$this -> get_vehicle_class_feild_values_demo_dealer($value_select);
        if($value_select=='full_size_cars' ){
        $condition.="(vehicletype='Full-size cars')";
        }
        else if($value_select=='mid_size_cars' ){
        $condition.="(vehicletype='mid-size cars')";   
        }
        else if($value_select=='small_cars'){
        $condition.="(vehicletype='small cars')";   
        }
        
        else{
        $condition.='(vehicletype='."'".$condition_name."')";
        }
        $commoncheck++;   
        }
        else{
        $condition_name=$this -> get_vehicle_class_feild_values($value_select);
        if($value_select=='full_size_cars' ){
        $condition.="(vehicletype='Car' and vehiclesize='Large')";
        }
        else if($value_select=='mid_size_cars' ){
        $condition.="(vehicletype='Car' and vehiclesize='Midsize')";   
        }
        else if($value_select=='small_cars' ){
        $condition.="(vehicletype='Car' and vehiclesize='Compact')";   
        }
         else if($value_select=='vans' ){
         $condition.="(vehicletype='Minivan')";   
         }
        else{
        $condition.='(vehicletype='."'".$condition_name."')";
        }
        $commoncheck++; 
        } 
        }
        $condition.=')';
        $condition.=" AND  ";
        }
        $condition.="dealership_id='$dealer_id_upload_data' AND (
        first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
        OR 
        contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare )";
        $condition.=')';
        $sql_get_query=("SELECT  * FROM eps_data  WHERE
        $condition  
         order by id ASC ");
        $result=$this->db->query($sql_get_query);
        if($result -> num_rows() >0){
        $return_result= $result->result_array();
        } 
        else{
        $return_result='';
        }  
    }
    //used and new purchase   
    else if($values['report_type']=='used_new_purchaser'){
        $return_result='';
        $feild_values_display='';
        $feild_value=$values['value1'];
        $feild_name=$values['field_name1'];
        $feild_values_explode='';
        $condition='';
        if($feild_value!=''){
        $feild_values_explode=explode(',',$feild_value);
        }
        $commoncheck=0;
        if($group_id!=''){
        $condition.="id  NOT IN ($group_id) AND ";  
        }
        $condition.='(';
         if($feild_name=='new'){
        $condition.="(new_used='N' OR new_used='NEW') AND ";
        }
        else{
        $condition.="(new_used='U' OR new_used='USED') AND ";
        }
        if(is_array($feild_values_explode) && $feild_values_explode!=''){
        $condition.='(';
        foreach($feild_values_explode as $value_select){
        if($commoncheck>0){
        $condition.='OR ';
        }
        if($dealer_id_upload_data=='198'){
        $condition_name=$this -> get_vehicle_class_feild_values_demo_dealer($value_select);
        if($value_select=='full_size_cars' ){
        $condition.="(vehicletype='Full-size cars' )";
        }
        else if($value_select=='mid_size_cars' ){
        $condition.="(vehicletype='mid-size cars' )";   
        }
        else if($value_select=='small_cars'){
        $condition.="(vehicletype='small cars')";   
        }
        
        else{
        $condition.='(vehicletype='."'".$condition_name."' )";
        }
        $commoncheck++;   
        }
        else{
        $condition_name=$this -> get_vehicle_class_feild_values($value_select);
        if($value_select=='full_size_cars' ){
        $condition.="(vehicletype='Car' and vehiclesize='Large')";
        }
        else if($value_select=='mid_size_cars' ){
        $condition.="(vehicletype='Car' and vehiclesize='Midsize') ";   
        }
        else if($value_select=='small_carsfull_size_cars' ){
        $condition.="(vehicletype='Car' and vehiclesize='Smallsize')";   
        }
         else if($value_select=='vans' ){
            $condition.="(vehicletype='Minivan')";   
         }
        else{
        $condition.='vehicletype='."'".$condition_name."'";
        }
        $commoncheck++; 
        }
        }
        $condition.=')';
        $condition.=" AND "; 
        }
        $condition.="dealership_id='$dealer_id_upload_data' AND (
        first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
        OR 
        contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
        )" ;
        $condition.=')';
        $sql_get_query=("SELECT  * FROM eps_data  WHERE
        $condition  
         order by id ASC ");
        $result=$this->db->query($sql_get_query);
        if($result -> num_rows() >0){
        $return_result= $result->result_array();
        } 
        else{
        $return_result='';
        }  
    }
    //local town
        else if($values['report_type']=='local_town'){
            $return_result='';
            $get_dealer_details=$this ->  main_model-> dealerfulldetails($dealer_id_upload_data);
            if(isset($get_dealer_details) && $get_dealer_details!=''){
            foreach($get_dealer_details as $values_dealer_details){
            $dealer_city= $values_dealer_details['city']; 
            }  
            }
            $feild_value=$values['value1'];
            $feild_name=$values['field_name1'];
            $condition='';
            $feild_values_explode='';
            if($feild_value!=''){
            $feild_values_explode=explode(',',$feild_value);
            }
            $commoncheck=0;
            $condition.='(';
             if($feild_name=='local')
            {
            $condition.='buyer_city='."'".$dealer_city."'";
            $condition.=' AND ';
            }
            else if($feild_name=='out_of_town'){
            $condition.='buyer_city!='."'".$dealer_city."'";
            $condition.=' AND ';
            }
            else{
            $condition.='';
            }
            if(is_array($feild_values_explode) && $feild_values_explode!=''){
            $condition.='(';
            foreach($feild_values_explode as $value_select){
            if($commoncheck>0){
            $condition.=' OR ';
            }
            $condition_name=$this -> get_vehicle_class_feild_values($value_select);
            if($value_select=='full_size_cars' ){
            $condition.="(vehicletype='Car' AND vehiclesize='Large')";
            }
            else if($value_select=='mid_size_cars' ){
            $condition.="(vehicletype='Car' AND vehiclesize='Midsize')";   
            }
            else if($value_select=='small_carsfull_size_cars' ){
            $condition.="(vehicletype='Car' AND vehiclesize='Smallsize')";   
            }
            else if($value_select=='vans' ){
            $condition.="(vehicletype='Minivan')";   
            }
            else{
            $condition.='(vehicletype='."'".$condition_name."')";
            }
            
            $commoncheck++; 
            } 
            $condition.=')';
            $condition.=" AND ";
            }
            
            $condition.="dealership_id='$dealer_id_upload_data' AND (
            first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
            OR 
            contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
            )";
            $condition.=')';
            if($group_id!=''){
            $condition.=" AND id  NOT IN ($group_id) ";  
            }
            $sql_get_query=("SELECT  * FROM eps_data  WHERE
            $condition  
             order by id ASC ");
              //echo $sql_get_query;
      	     $result=$this->db->query($sql_get_query);
            if($result -> num_rows() >0){
            $return_result= $result->result_array();
            } 
            else{
            $return_result='';
            }  
    }
       //dealership brand
       else if($values['report_type']=='dealership_brand'){
           $return_result='';
           $get_dealer_details=$this ->  main_model-> dealerfulldetails($dealer_id_upload_data);
           $dealer_manufacure_explode='';
           $condition='';
           $commoncheck=0;
           if(isset($get_dealer_details) && $get_dealer_details!=''){
            if($group_id!=''){
           $condition.="id  NOT IN ($group_id) AND ";  
           }
            $condition.='(';
           foreach($get_dealer_details as $values_dealer_details){
           $dealer_manufacure= $values_dealer_details['masterbrand']; 
           $dealer_manufacure_explode=explode(',',$dealer_manufacure);
           if(is_array($dealer_manufacure_explode)){
            foreach($dealer_manufacure_explode as $dealer_master_brand){
            if($commoncheck>0){
            $condition.=' AND ';
            }
            $condition.='(sold_vehicle_make!='."'".$dealer_master_brand."') ";
            $commoncheck++;
            }
            }
            } 
            $condition.="AND dealership_id='$dealer_id_upload_data' AND (
            first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
            OR 
            contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
            )";
            $condition.=')'; 
            }
            $sql_get_query=("SELECT  * FROM eps_data  WHERE
            $condition  
             order by id ASC ");
            $result=$this->db->query($sql_get_query);
           //echo $sql_get_query;
            if($result -> num_rows() >0){
            $return_result= $result->result_array();
            } 
            else{
            $return_result='';
            }  
       }
       //power focus
       else if($values['report_type']=='power_focus'){
           $return_result='';
           $feild_value=$values['value1'];
           $feild_name=$values['field_name1'];
           $condition='';
           $feild_values_explode=explode(',',$feild_value);
           $commoncheck=0;
           if($group_id!=''){
           $condition.="id NOT IN ($group_id) AND ";  
           }
           $condition.='(';
           if(is_array($feild_values_explode) && $feild_values_explode!=''){
           $condition.='(';
           $condition.="(powerratio<=12 AND powerratio!='')AND ";
           foreach($feild_values_explode as $value_select){
            if($commoncheck>0){
            $condition.=' OR ';
            }
            if($dealer_id_upload_data=='198'){
            $condition_name=$this -> get_vehicle_class_feild_values_demo_dealer($value_select);
            if($value_select=='full_size_cars' ){
            $condition.="(vehicletype='Full-size cars')";
            }
            else if($value_select=='mid_size_cars' ){
            $condition.="(vehicletype='mid-size cars')";   
            }
            else if($value_select=='small_cars'){
            $condition.="(vehicletype='small cars')";   
            }
            else{
            $condition.='(vehicletype='."'".$condition_name."')";
            }
            $commoncheck++;   
            }
            else{
            $condition_name=$this -> get_vehicle_class_feild_values($value_select);
            if($value_select=='full_size_cars' ){
            $condition.="(vehicletype='Car' and vehiclesize='Large')";
            }
            else if($value_select=='mid_size_cars' ){
            $condition.="(vehicletype='Car' and vehiclesize='Midsize')";   
            }
            else if($value_select=='small_cars' ){
            $condition.="(vehicletype='Car' and vehiclesize='Compact')";   
            }
             else if($value_select=='vans' ){
            $condition.="(vehicletype='Minivan')";   
         }
            else{
            $condition.='(vehicletype='."'".$condition_name."')";
            }
            $commoncheck++; 
            } 
            }
            $condition.=')';
            $condition.=' AND ';
            }
            else{
            $condition='';   
            }
            $condition.= " dealership_id='$dealer_id_upload_data' AND (
            OR 
            contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
            first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare )";
            $condition.=')';
            $sql_get_query=("SELECT  * FROM eps_data  WHERE
            $condition 
             order by id ASC");
            $result=$this->db->query($sql_get_query);
            if($result -> num_rows() >0){
            $return_result= $result->result_array();
            } 
            else{
            $return_result='';
            }  
    }
    //specific model
    else if($values['report_type']=='specific_model'){
        $return_result='';
        if($values['value1']!=''){
        $feild_value1=$values['value1'];
        }
        else{
        $feild_value1='';  
        }
        if($values['value2']!=''){
        $feild_value2=$values['value2'];
        }
        else{
        $feild_value2='';  
        }
        $condition='';
        if($group_id!=''){
        $condition.="id  NOT IN ($group_id) AND ";  
        }
        $condition.='(';
        if($feild_value1!=''){
        $condition.='sold_vehicle_make='."'".$feild_value1."'";
        }
        else{
         $condition.="sold_vehicle_make=''";  
        }
        if($feild_value2!=''){
        $condition.=' AND sold_vehicle_model='."'".$feild_value2."' AND "; 
        }
        else{
         $condition.="AND sold_vehicle_model='' AND ";  
        }
         $condition.="dealership_id='$dealer_id_upload_data' AND (
         first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
         OR 
         contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
         )";
         $condition.=')';
        $sql_get_query=("SELECT  * FROM eps_data  WHERE
        $condition  
         order by id ASC ");
        $result=$this->db->query($sql_get_query);
        if($result -> num_rows() >0){
        $return_result= $result->result_array();
        } 
        else{
        $return_result='';
        }  
    }
    else if($values['report_type']=='out_warranty'){
        if($dealer_id_upload_data!='198'){
        $condition='';
        $powertrain_months='';
        if($group_id!=''){
        $condition.="id NOT IN ($group_id) AND ";  
        }
        $return_result='';
        $id_get='';
        //get event details
        $sql=("Select event_start_date from events where event_id=$event_id");
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0){
        $returnvalue_get=$query->result_array();
        foreach($returnvalue_get as $event_date){
        $event_date_select=$event_date['event_start_date'];   
        }
        //get customer make,purchase date 
        $query_financial=("SELECT id,first_payment_date,sold_vehicle_make from  eps_data where $condition (dealership_id='$dealer_id_upload_data' AND (
        first_payment_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare 
        OR 
        contract_date BETWEEN $newdate_purchase_range_to_date_compare AND $newdate_purchase_range_from_date_compare
        )) 
        ");
        //echo $query_financial;
        $sql_get_query=$this->db->query($query_financial);
        if($sql_get_query -> num_rows() > 0)
        {
        $returnvalue_customer_data=$sql_get_query->result_array();
        foreach($returnvalue_customer_data as $customerdata) {
        $sql_warranty_query=("SELECT  * FROM warranty_manufacture  WHERE
        manufacturer='$customerdata[sold_vehicle_make]'
        ");
        $sql_warranty_manufacture=$this->db->query($sql_warranty_query);
        if($sql_warranty_manufacture -> num_rows() > 0)
        {
        $returnvalue_warranty_manufacture=$sql_warranty_manufacture->result_array();
        foreach($returnvalue_warranty_manufacture as $value_warranty){
        $powertrain_months=$value_warranty['powertrain_months'];
        $basic_months=$value_warranty['basic_months'];    
        }
        }
        else{
        $powertrain_months='';
        $basic_months='';     
        }
        $purcahse_date=strtotime($customerdata['first_payment_date']);
        //calculate month difference and get the customer id and save it in array for looping to get the detaiils of edch customer
        $difference = $event_date_select - $purcahse_date;
        $months = floor($difference / 86400 / 30 );
        if($months>0){
        if($months>$powertrain_months){
            if($powertrain_months!=''){
            $id_get[]= $customerdata['id'];  
            }
        }
        else if($months>($powertrain_months-6)){
            if($powertrain_months!=''){
            $id_get[]= $customerdata['id'];    
            }
        }
        else if($months>($basic_months)){
            if($basic_months!=''){
            $id_get[]= $customerdata['id']; 
            }   
        }
        else if($months<($basic_months-6)){
            if($basic_months!=''){
            $id_get[]= $customerdata['id'];    
        }
        }
        else if($months>($basic_months-6)){
            if($basic_months!=''){
            $id_get[]= $customerdata['id'];    
        }
        }
        }
        }
        $ij=0;
        $sql_warranty='';
        if(!empty($id_get)){
        $sql_warranty=("select * from  eps_data where dealership_id='$dealer_id_upload_data' ");
        $sql_warranty.=" AND ";
        $sql_warranty.="(";
        foreach($id_get as $values_id){
        if($values_id!=''){
        if($ij>0){
        $sql_warranty.=" or ";
        }
        $sql_warranty.="(id=$values_id)";
        $ij++;
        }
        }
        $sql_warranty.=")";
        $query_warranty = $this->db->query($sql_warranty);
        $return_result= $query_warranty->result_array();
        }
        else{
        $return_result='';     
        }
        }
        }
    }
    }
    else{
      $return_result='';  
    }
}
}
else{
$return_result='';
}
return $return_result;
}
public function getgroupname_advanced_option_report_type($event_id,$group)
{
    
    $sql=("Select report_type from advance_options_group_selection where event_id=$event_id and group_name='$group'");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $return= $query->result_array();
    foreach($return as $report_type){
       $report_type_get= $report_type['report_type'];
    }
    $return=$report_type_get;
   }
    else{
    $return='';
    }
    return $return;
}
public function getcountofmakemodel($year,$make,$model)
{
    
    $sql=("Select count(*) as count from pbs_customer_data where sold_vehicle_year=$year and sold_vehicle_make='$make' and sold_vehicle_model='$model'");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $return= $query->result_array();
    foreach($return as $report_type){
       $count= $report_type['count'];
    }
    $return=$count;
   }
    else{
    $return='';
    }
    return $return;
}
public function get_latest_eventdetails(){
    $sql=("select * from events where creation_status='complete' order by event_id desc limit 1");
    $query=$this->db->query($sql);
    if($query -> num_rows() >0){
        $return= $query->result_array();        
        $return=$return;
    }else{
        $return='';
    }
    return $return;
}
public function get_campignevent_details($event_id){
    $sql=("select * from epsadvantage_campaign where event_id='$event_id'");
    $query=$this->db->query($sql);
    if($query -> num_rows() >0){
        $return= $query->result_array();        
        $return=$return;
    }else{
        $return='';
    }
    return $return;
}
public function getmaster_details(){
    $sql=("select * from  eps_master_vehicle order by eps_master_vehicle_id asc");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){
    $return= $query->result_array();
    } else{
    $return='';
    }
    return $return;
}
}