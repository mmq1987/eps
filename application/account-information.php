<!-- JavaScript at the bottom for fast page loading -->
<!-- Scripts -->
<link rel="stylesheet" href="<?=base_url()?>js/libs/formValidator/developr.validationEngine.css?v=1">
<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>
<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>
<!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin" style="text-align: left;">
            <h1>View Profile</h1>
        </hgroup>
        <div style="color: red;text-align: center; padding-top: 13px;">
        <?php
        if(isset($error))
            {
            ?>
                <div style="color: red;"><?=$error;?></div>
            <?php
            }
        if(isset($success))
            {
            ?>
                <div style="color: green; border: 1px dashed green; width: 50%; margin: 0px auto; padding: 13px;"><?=$success;?></div>
            <?php 
            }
        ?>
        <script>
        $(document).ready(function() {	
            $('#country-select').trigger('change');
        });
        function cancel_form()
        {
            window.location.href = '<?=base_url()?>dashboard';
        }
        function update_city()
        {
            $('#states').trigger('change');
        }
        function select_city(states,city)
        {
            $('#cityhide').hide();
            $.post('<?=base_url()?>settings/getcitywithselected',{states :  states,city :  city},function(data){
            $('#show_city').html(data);
            });
        }
        function postcode_display(){
        var states=$('#states').val();
        var city=$('#city_select').val();
        if(states!='' || city!=''){
                $.post('<?=base_url()?>register/postcodedisplay',{states : states,city : city},function(data){
                $('#post_code').val(data);
                });
            }else{
                alert('Please select state and city to proceed');
            }
        }
        function changeupper()
        {
            $("#post_code").val(($("#post_code").val()).toUpperCase());
        }
        </script>
        <style>
        .select{
            width: 261px;
        }
        .drop-down{
            text-align: left;
            width: 256px; 
        }
        .select-value
        {
            text-align: left;
        }
        .drop-down > span, .drop-down > a
        {
            text-align: left;  
        }
        .field-block{
            padding: 0 30px 0 182px;
        }
        .selectMultiple > .drop-down{
            height: 134px;
        }
        </style>
        </div>
    <?php
    if($details[0]['usertype']=='dealership' ||  $details[0]['usertype']=='auto_brand')
        {
        ?>
            <form method="post" action="<?=base_url()?>profile/update/<?=$details[0]['registration_id']?>" title="Registration" id="formedit-login" >
                <div class="with-padding" style="margin-top: 15px;">
                    <div class="columns">
                        <div class="six-columns twelve-columns-tablet" style=" margin-left:236px">
                            <fieldset class="fieldset">
                            <?php
                            if($details[0]['usertype']=='account_managers')
                            {
                                $membership='Accounts Manager';
                            }
                            else if($details[0]['usertype']=='dealership')
                            {
                                $membership='Dealer';
                            }
                            else if($details[0]['usertype']=='auto_brand')
                            {
                                $membership='Auto Manufacturer';
                            }
                            else{
                                $membership='Admin';
                            }
                            ?>
                            <legend class="legend"> <?=ucfirst($details[0]['first_name'])?>&nbsp;(<?=$membership?>)</legend>
                            <div class="formheader">Dealership Info</div>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Dealership Name<font color="red">*</font></label>
                                    <input type="text" name="company_name" id="company_name" class="input validate[required]" value="<?=$details[0]['company_name']?>" data-tooltip-options='{"position":"right"}'/>
                                </p>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Address<font color="red">*</font></label>
                                    <input type="text" name="address" id="small-label-1" class="input small-margin-right" value="<?=$details[0]['address']?>"/>
                                </p>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Country<font color="red">*</font></label>
                                        <select id="country-select" name="country" class="select validate[required]" onchange="zipcodechange();">
                                            <option <?php echo $details[0]['country']=='Canada' ? ' selected ':''; ?> value="Canada">Canada</option>
                                            <option <?php echo $details[0]['country']=='USA' ? ' selected ':''; ?> value="USA">USA</option>
                                        </select>
                                </p>
                                <p class="inline-small-label button-height" id="canadian_state">
                                    <label for="small-label-1" class="label">Province<font color="red">*</font></label>
                                    <select id="states" name="canadastate" class="select validate[required]" style="text-align: left;" onchange="select_city(this.value);">
                                        <option value="">Please Select</option>
                                        <?php
                                        $ststes_fields=$this -> main_model -> Canadian_provinces();
                                        foreach($ststes_fields as $key=>$value)
                                        {
                                        ?>
                                            <option <?php echo $details[0]['state']==$key ? ' selected ':''; ?> value="<?=$key?>"><?=$value?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </p>
                                <p class="inline-small-label button-height" id="usa_state" style="display: none;">
                                    <label for="small-label-1" class="label">Province<font color="red">*</font></label>
                                    <select id="states" name="state" class="select1 validate[required]" onchange="select_city(this.value,'<?=$details[0]['city']?>');">
                                    <?php
                                    $ststes_fields=$this -> main_model -> getusstates();
                                        foreach($ststes_fields as $key=>$value){
                                        ?>
                                            <option <?php echo $details[0]['state']==$value['code'] ? ' selected ':''; ?>  value="<?=$value['code']?>"><?=$value['state']?></option>
                                        <?php
                                        }
                                    ?>
                                    </select>
                                </p>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">City</label>
                                    <input type="text" name="city" id="city" class="input " value="<?=$details[0]['city']?>" data-tooltip-options='{"position":"right"}'/>
                                </p>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label" id="postalcodelabel">Postal Code<font color="red">*</font></label>
                                    <label for="small-label-1" class="label" id="zipcodelabel" style="display: none;">Zip Code<font color="red">*</font></label>   
                                    <input type="text" name="zipcode" id="post_code" class="input validate[required]" value="<?=$details[0]['zipcode']?>" onblur="changeupper();"/>
                                </p>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Phone number<font color="red">*</font></label>
                                    <input type="text" name="company_phonenumber" id="company_phonenumber" class="input validate[required]" maxlength="11"  value="<?=$details[0]['company_phonenumber']?>"/>
                                </p>
                                <?php
                                if($details[0]['usertype']=='dealer')
                                {
                                ?>
                                    <p class="inline-small-label button-height">
                                        <label for="small-label-1" class="label">Dealership Email</label>
                                        <input type="text" name="dealership_email" class="input validate[custom[email]]" id="small-label-1" class="input"  value="<?=$details[0]['dealership_email']?>" value="" data-tooltip-options='{"position":"right"}'/>
                                    </p>
                                <?php
                                }
                                ?>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Website</label>
                                    <input type="text" name="company_website" id="small-label-1" class="input small-margin-right" value="<?=$details[0]['company_website']?>"/>
                                </p>
                                 <?php
                                if($membershiptype=='dealer')
                                {
                                ?>
                                <p class="inline-small-label button-height">
            						<label for="small-label-1" class="label">Data Source<font color="red">*</font></label>
            						<select id="states" name="data_source" class="select validate[required]" style="text-align: left;" >
                                      <option value="pbs_direct">PBS Direct</option>
                                       <option value="authenticom">Authenticom</option>
                                      </select>
            					</p>
                                <?php
                                }
                                ?>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Manufacturer<font color="red">*</font><br /><span style="font-size: 11px;">(Ctrl+click for multiple selection of manufacturer)</span></label>
                                        <select id="masterbrand" name="masterbrand[]" class="select validate[required] selectMultiple" style="text-align: left;overflow-y: scroll;" onchange="select_city(this.value);" multiple="">
                                        <?php
                                        $makes_details=$this->main_model->makes_models();
                                        if(isset($makes_details))
                                        {
                                            $manufactureType=explode(',',$details[0]['masterbrand']);
                                            foreach($makes_details as $makes){
                                                if(in_array($makes['make'],$manufactureType)) {
                                                    $selected='selected ';
                                                }
                                                else 
                                                {
                                                    $selected= ' ';
                                                }
                                            ?>
                                            <option <?=$selected?> value="<?=$makes['make']?>"><?=$makes['make']?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                
                                        <?php
                                        }
                                        ?>
                                </p>
                                <div class="formheader">Contact Info</div>
                                <p class="inline-small-label button-height">
                                    <label for="validation-required" class="label">First Name<font color="red">*</font></label>
                                    <input type="text" name="first_name" id="validation-required" class="input validate" value="<?=$details[0]['first_name']?>" data-tooltip-options='{"position":"right"}'/>
                                </p>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Last Name<font color="red">*</font></label>
                                    <input type="text" name="last_name" id="last_name" class="input validate[required]" value="<?=$details[0]['last_name']?>"/>
                                </p>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Contact Phone</label>
                                    <input type="text" name="contact_phone_number" id="contact_phone_number" class="input small-margin-right" value="<?=$details[0]['contact_phone_number']?>"/>
                                </p>
                                <?php
                                $user_id = $menu['logged_in']['registration_id'];
                                $get_usertype=$this -> main_model -> get_usertype($user_id);
                                if($get_usertype=='admin' || $menu['logged_in']['usertype']=='sub_admin'){
                                ?>
                                    <p class="inline-small-label button-height">
                                        <label for="small-label-1" class="label">Contact Email<font color="red">*</font></label>
                                        <input type="text" name="email" id="small-label-1" class="input validate[required,custom[email]]"  value="<?=$details[0]['email_id']?>" value="" data-tooltip-options='{"position":"right"}'/>
                                        <span style="font-size: 11px;color:grey;margin-left: 42px;">[Email will be saved as Username]</span>
                                    </p>
                                <?php
                                }else{
                                ?>               
                                    <p class="inline-small-label button-height">
                                        <label for="small-label-1" class="label">Email<font color="red">*</font></label>
                                        <input type="text"  id="small-label-1" class="input validate[required,custom[email]]" disabled="" value="<?=$details[0]['email_id']?>" value="" data-tooltip-options='{"position":"right"}'/>
                                        <input type="hidden" value="<?=$details[0]['email_id']?>" name="email"/>
                                    </p>
                                <?php
                                }
                            ?>
                            <?php
                            $password=$this -> main_model ->ProtectData($details[0]['password'],'DECODE'); 
                            if($menu['logged_in']['usertype']=='admin' || $menu['logged_in']['usertype']=='sub_admin' || $menu['logged_in']['usertype']=='account_managers' || $menu['logged_in']['usertype']=='auto_brand')
                            {
                            ?>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Password</label>
                                    <input type="text" name="password" id="small-label-1" class="input small-margin-right" value="<?=$password?>"/>
                                </p>
                            <?php
                            }else{ 
                            ?>              
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Password</label>
                                    <input type="password" name="password" id="small-label-1" class="input small-margin-right" value=""/>
                                </p>
                            <?php
                            }
                            ?>
                            <?php
                            if($details[0]['usertype']=='auto_brand')
                            {
                            ?>
                                <p class="inline-small-label button-height">
                                    <label for="small-label-1" class="label">Job Position<font color="red">*</font></label>
                                    <input type="text" name="job_position" id="job_position" class="input validate[required]" value="<?=$details[0]['job_position']?>" data-tooltip-options='{"position":"right"}'/>
                                </p>
                            <?php
                            }
                            ?>
                            <input type="hidden" name="membership" id="small-label-1" class="input small-margin-right" value="<?=$details[0]['usertype']?>"/>
                                <div class="field-block button-height">
                                    <button type="button" class="button glossy mid-margin-right" onclick="back_form();">
                                    <span class="button-icon green-gradient"><span class="icon-backward"></span></span>
                                    Back
                                    </button>
                                    <button type="submit" class="button glossy mid-margin-right">
                                    <span class="button-icon"><span class="icon-tick"></span></span>
                                    Save
                                    </button>
                                    <button type="button" class="button glossy" onclick="deletepoperty(<?=$details[0]['registration_id']?>)">
                                    <span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
                                    Delete
                                    </button>
                                </div>
                            </fieldset> 
                        </div>
                    </div>
                </div>
            </form>
        <?php
        }elseif($details[0]['usertype']=='account_managers' || $details[0]['usertype']=='sub_admin' || $details[0]['usertype']=='admin' ){
    ?>
    <form method="post" action="<?=base_url()?>profile/managerupdate/<?=$details[0]['registration_id']?>" title="Registration" id="formedit-login" >
    <div class="with-padding" style="margin-top: 15px;">
    <div class="columns">
    <div class="six-columns twelve-columns-tablet" style=" margin-left:236px">
    <fieldset class="fieldset">
    <?php
    if($details[0]['usertype']=='account_managers')
    {
    $membership='Accounts Manager';
    }
    else if($details[0]['usertype']=='dealership')
    {
    $membership='Dealer';
    }
    else if($details[0]['usertype']=='auto_brand')
    {
    $membership='Auto Manufacturer';
    }
    else if($details[0]['usertype']=='sub_admin')
    {
    $membership='Sub Admin';
    }
    else{
    $membership='Admin';
    }
    ?>
    <legend class="legend"> Register <?=$membership?></legend>
    <div class="formheader">Contact Info</div>
    <p class="inline-small-label button-height">
    <label for="validation-required" class="label">First Name<font color="red">*</font></label>
    <input type="text" name="first_name" id="validation-required" class="input validate" value="<?=$details[0]['first_name']?>" data-tooltip-options='{"position":"right"}'/>
    </p>
    <p class="inline-small-label button-height">
    <label for="small-label-1" class="label">Last Name<font color="red">*</font></label>
    <input type="text" name="last_name" id="last_name" class="input input validate[required]" value="<?=$details[0]['last_name']?>"/>
    </p>
    <p class="inline-small-label button-height">
    <label for="small-label-1" class="label">Contact Phone<font color="red">*</font></label>
    <input type="text" name="contact_phone_number" id="contact_phone_number" class="input small-margin-right" value="<?=$details[0]['contact_phone_number']?>" data-tooltip-options='{"position":"right"}'/>
    </p>
    <?php
    $user_id = $menu['logged_in']['registration_id'];
    $get_usertype=$this -> main_model -> get_usertype($user_id);
    if($get_usertype=='admin' || $menu['logged_in']['usertype']=='sub_admin'){
    if($details[0]['usertype']=='admin')
    {
    ?>
    <p class="inline-small-label button-height">
    <label for="small-label-1" class="label">User Name<font color="red">*</font></label>
    <input type="text" name="email" id="small-label-1" class="input validate[required]"  value="<?=$details[0]['email_id']?>" value="" data-tooltip-options='{"position":"right"}'/>
    </p>
    <?php
    }
    else
    {
    ?>
    <p class="inline-small-label button-height">
    <label for="small-label-1" class="label">Contact Email<font color="red">*</font></label>
    <input type="text" name="email" id="small-label-1" class="input validate[required,custom[email]]"  value="<?=$details[0]['email_id']?>" value="" data-tooltip-options='{"position":"right"}'/>
    <span style="font-size: 11px;color:grey;margin-left: 42px;">[Email will be saved as Username]</span>
    </p>
    <?php
    }
    }else{
    ?>               
    <p class="inline-small-label button-height">
    <label for="small-label-1" class="label">Email<font color="red">*</font></label>
    <input type="text"  id="small-label-1" class="input validate[required,custom[email]]" disabled="" value="<?=$details[0]['email_id']?>" value="" data-tooltip-options='{"position":"right"}'/>
    <input type="hidden" value="<?=$details[0]['email_id']?>" name="email"/>
    </p>
    <?php
    }
    ?>
    <?php
    $password_display=$this -> main_model ->ProtectData($details[0]['password'],'DECODE'); 
    if($menu['logged_in']['usertype']=='admin' || $menu['logged_in']['usertype']=='sub_admin' || $menu['logged_in']['usertype']=='account_managers' || $menu['logged_in']['usertype']=='auto_brand')
    {
    ?>
    <p class="inline-small-label button-height">
    <label for="small-label-1" class="label">Password</label>
    <input type="text" name="password" class="input small-margin-right" value=<?=$password_display?> />
    </p>
    <?php
    }else{ 
    ?>              
    <p class="inline-small-label button-height">
    <label for="small-label-1" class="label">Password</label>
    <input type="password" name="password" id="small-label-1" class="input small-margin-right" value=""/>
    </p>
    <?php
    }
    ?>
    <input type="hidden" name="membership" id="small-label-1" class="input small-margin-right" value="<?=$details[0]['usertype']?>"/>
    <div class="field-block button-height">
    <button type="button" class="button glossy mid-margin-right" onclick="back_form();">
    <span class="button-icon green-gradient"><span class="icon-backward"></span></span>
    Back
    </button>
    <button type="submit" class="button glossy mid-margin-right">
    <span class="button-icon"><span class="icon-tick"></span></span>
    Save
    </button>
    <?php
    if($menu['logged_in']['usertype']=='sub_admin'){
    $user_id = $menu['logged_in']['registration_id'];
    $creater_id=$this->main_model->get_createrid($user_id);
    if($creater_id==$details[0]['registration_id'] || $user_id==$details[0]['registration_id']){
    }else{
    ?>
    <button type="button" class="button glossy" onclick="deletepoperty(<?=$details[0]['registration_id']?>)">
    <span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
    Delete
    </button>
    <?php
    }
    }else{
    ?>
    <button type="button" class="button glossy" onclick="deletepoperty(<?=$details[0]['registration_id']?>)">
    <span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
    Delete
    </button>
    <?php   
    }
    ?>
    </div>
    </fieldset> 
    </div>
    </div>
    </div>
    </form>                     
    <?php   
    }
    ?>
    </section>
<!-- Scripts -->
<script src="<?=base_url()?>js/libs/jquery-1.10.2.min.js"></script>
<script src="<?=base_url()?>js/setup.js"></script>
<!-- Template functions -->
<script src="<?=base_url()?>js/developr.input.js"></script>
<script src="<?=base_url()?>js/developr.navigable.js"></script>
<script src="<?=base_url()?>js/developr.notify.js"></script>
<script src="<?=base_url()?>js/developr.scroll.js"></script>
<script src="<?=base_url()?>js/developr.tooltip.js"></script>
<!-- End sidebar/drop-down menu -->
<script src="<?=base_url()?>js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
<script src="<?=base_url()?>js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>
<script src="<?=base_url()?>js/mask.js" type="text/javascript"></script>
<script>
function cancel_form()
{
window.location.href = '<?=base_url()?>dashboard';
exit();
}
var firstname=$('#first_name').val();
var last_name=$('#last_name').val();
var company_name=$('#company_name').val();
var company_logo=$('#company_logo').val();
var validation_email=$('#validation_email').val();
var password=$('#password').val();
var confirm_Password=$('#confirm_Password').val();
var company_phonenumber=$('#company_phonenumber').val();
var company_website=$('#company_website').val();
var masterbrand=$('#masterbrand').val();
var contact_person=$('#contact_person').val();
var address=$('#address').val();
var city=$('#city').val();
var state=$('#state').val();
var country=$('#country').val();
var zipcode=$('#zipcode').val();
$('form').validationEngine();
$('#formedit-login').submit(function(event)
{
var firstname=$('#first_name').val();
var last_name=$('#last_name').val();
var company_name=$('#company_name').val();
var company_logo=$('#company_logo').val();
var validation_email=$('#validation_email').val();
var password=$('#password').val();
var confirm_Password=$('#confirm_Password').val();
var company_phonenumber=$('#company_phonenumber').val();
var company_website=$('#company_website').val();
var masterbrand=$('#masterbrand').val();
var contact_person=$('#contact_person').val();
var address=$('#address').val();
var city=$('#city').val();
var state=$('#state').val();
var country=$('#country').val();
var zipcode=$('#zipcode').val();
if(firstname=='' ||  company_name=='' ||  validation_email==''  ||  password=='' ||  confirm_Password=='' ||  company_phonenumber=='' ||  address=='' ||  zipcode=='' )
{
}
else
{
if(password!=confirm_Password)
{
$('form').validationEngine();
$( "#password" ).removeClass( "input validate" );
$( "#password" ).addClass( "input validate" );
return false;
}
else
{
$('#formedit-login').submit();
}
}
});
function zipcodechange(){
var country=$('#country-select').val();
if(country=='USA'){
$('#postalcodelabel').hide();
$('#zipcodelabel').show();
$('#usa_state').show(); 
$('#canadian_state').hide(); 
}else{
$('#postalcodelabel').show();
$('#zipcodelabel').hide(); 
$('#canadian_state').show(); 
$('#usa_state').hide(); 
}
}
function deletepoperty(property_id)
{
if(confirm('Are you sure ?')){
$.post('<?php echo base_url(); ?>dashboard/delete/'+property_id,function(data) {
if(data=='Done'){
window.location.href='<?php echo base_url(); ?>dashboard';
}
});
}
}
</script>
<script>
jQuery(function($){
// $j is now an alias to the jQuery function; creating the new alias is optional.
$("#company_phonenumber").mask("999-999-9999");
$("#contact_phone_number").mask("999-999-9999");
$("#contact_person").mask("999-999-9999");
$("#post_code").mask("AAA AAA");
});
</script>