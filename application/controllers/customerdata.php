<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Customerdata extends CI_Controller {
       	public function __construct() {
		parent::__construct();
		$this -> load -> helper('url');
		$this -> load -> library('session');
		$this -> load -> helper('form');
		$this -> load -> library('form_validation');
        $this->load->model('login_model'); 
        $this->load->model('main_model'); 
        
        $this->load->library("pagination");
	}
    public function index($dealers_userid='')
    {
        $data['title'] = 'Exclusive Private Sale Inc-View Customer Data';
        $data['menu']=$this->login_model->loginauth();
        if (isset($data['menu']['logged_in']) != '') {
         
              if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin' || $data['menu']['logged_in']['usertype']=='dealership')
            {
                $data['user_details']=$this -> main_model-> customerdata($dealers_userid);
                $data['dealerdashboard']=$dealers_userid; 
                $this->load->view('themes/header',$data);
                $this->load->view('themes/dealerside-bar',$data);
                $this-> load-> view('customerdata-view',$data);
                $this->load->view('themes/footer',$data);
            }
           
       }
       else
       {
           redirect(base_url().'login');
       } 
    }
        public function viewcustometdata($dealers_userid)
    {
        $data['title'] = 'Exclusive Private Sale Inc-View Customer Data';
        $data['menu']=$this->login_model->loginauth();
        if (isset($data['menu']['logged_in']) != '') {
         
              if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin' || $data['menu']['logged_in']['usertype']=='dealership')
            {
                $data['user_details']=$this -> main_model-> customerdatafulldetails($dealers_userid);
                $data['dealerdashboard']=$dealers_userid; 
                $this->load->view('themes/header',$data);
                $this->load->view('themes/dealerside-bar',$data);
                $this-> load-> view('viewfullcustometdata.php',$data);
                $this->load->view('themes/footer',$data);
            }
           
       }
       else
       {
           redirect(base_url().'login');
       } 
    }

    }
    ?>