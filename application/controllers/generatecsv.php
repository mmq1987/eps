<?php

class Generatecsv extends CI_Controller
{

    function Generatecsv()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('csv');
      $this->load->model('settings_model'); 
        $this->load->helper('file');
        $this -> load -> library('zip');
    }

        function create_csv(){
            $query = $this->db->query('SELECT * FROM pbs_customer_data');
            $num = $query->num_fields();
            $var =array();
            $i=0;
            $fname="";
            $feild_name=array("id","dealership_id","buyer_first_name","buyer_last_name","buyer_address","buyer_appartment","buyer_city","buyer_province","buyer_postalcode","buyer_homephone","buyer_businessphone","buyer_cellphone","buyer_email","sold_vehicle_stock","sold_vehicle_VIN","shortened_VIN","new_used","sold_vehicle_year","sold_vehicle_make","sold_vehicle_model");
            while($i <$num){
                $test = $i;
                
                $value =$feild_name[$i];
                if($value != ''){
                        $fname= $fname." ".$value;
                        array_push($var, $value);
                    }
                 $i++;
            }
            $fname = trim($fname);
            $fname=str_replace(' ', ',', $fname);
            $this->db->select($fname);
            $sql_leadlist=("SELECT lead_customer_id FROM  select_customer_leadlist, leadlist_customer_data WHERE select_customer_leadlist.customer_leadlist_id=leadlist_customer_data.customer_leadlist_id AND  select_customer_leadlist.event_id=5");
            $query_leadlist=$this->db->query($sql_leadlist);
            if($query_leadlist -> num_rows() > 0){
            $returnvalue= $query_leadlist->result_array();
            $ij=0; 
            $sql=("select $fname from  pbs_customer_data where(");
            foreach($returnvalue as $values){
            if($values!=''){
             if($ij>0){
                    $sql.="or ";
             } 
                     $sql.="id=$values[lead_customer_id] ";
                     $ij++;  
            }
            }
            $sql.=")";
            
            $quer = $this->db->query($sql);
            query_to_csv($quer,TRUE,'testfolder/Products_'.date('dMy').'.csv');
            
        }
        }
function create_xml(){
        $dom = xml_dom();
     $sql_leadlist=("SELECT lead_customer_id FROM  select_customer_leadlist, leadlist_customer_data WHERE select_customer_leadlist.customer_leadlist_id=leadlist_customer_data.customer_leadlist_id AND  select_customer_leadlist.event_id=14");
    $query_leadlist=$this->db->query($sql_leadlist);
    if($query_leadlist -> num_rows() > 0){
    $returnvalue= $query_leadlist->result_array();
    foreach($returnvalue as $values){
     $sql=("select * from  pbs_customer_data where id=$values[lead_customer_id]");
      $quer = $this->db->query($sql);
      $returnvalue_customer_data= $quer->result_array(); 
     
      foreach($returnvalue_customer_data as $values_customer_data){
        
        $book = xml_add_child($dom, 'book');
         xml_add_child($book, 'title', $values_customer_data['id']);
         xml_print($dom);
        }
       
    }
    
    }
    }
    
function create_txt(){
$filename = 'test-download.txt';
$content = 'BuyerFirstName	BuyerLastName    BuyerAddress	BuyerApartment	BuyerCity	BuyerState	BuyerZip	BuyerHomePhone	BuyerBusinessPhone	BuyerCellPhone	BuyerEmail	SoldVehicleStock	SoldVehicleVIN	NeworUsed     SoldVehicleYear	SoldVehicleMake	SoldVehicleModel';
$sql_leadlist=("SELECT lead_customer_id FROM  select_customer_leadlist, leadlist_customer_data WHERE select_customer_leadlist.customer_leadlist_id=leadlist_customer_data.customer_leadlist_id AND  select_customer_leadlist.event_id=14");
$query_leadlist=$this->db->query($sql_leadlist);
if($query_leadlist -> num_rows() > 0){
    $returnvalue= $query_leadlist->result_array();
    $content1='';
    foreach($returnvalue as $values){
        $sql=("select * from  pbs_customer_data where id=$values[lead_customer_id]");
        $quer = $this->db->query($sql);
        $returnvalue_customer_data= $quer->result_array(); 
        foreach($returnvalue_customer_data as $values_customer_data){
            $content1.="$values_customer_data[buyer_first_name]\t$values_customer_data[buyer_last_name]\t$values_customer_data[buyer_address]$values_customer_data[buyer_appartment]\t$values_customer_data[buyer_city]\t$values_customer_data[buyer_province]\t$values_customer_data[buyer_postalcode]\t$values_customer_data[buyer_homephone]\t$values_customer_data[buyer_businessphone]\t$values_customer_data[buyer_cellphone]\t$values_customer_data[buyer_email]\t$values_customer_data[sold_vehicle_stock]\t$values_customer_data[sold_vehicle_VIN]\t$values_customer_data[new_used]\t$values_customer_data[sold_vehicle_year]\t$values_customer_data[sold_vehicle_make]\t$values_customer_data[sold_vehicle_model]\t \n";   
        }
    }
}
$textfilecontent="$content\n$content1";
$base_path = $this -> config -> item('rootpath');
$handle = fopen($base_path.'downloadreportzip/'.$filename, 'w');
fwrite($handle, $textfilecontent);
fclose($handle);
}
 function write_xml() {
 
	$xml = new DOMDocument("1.0");
	$root = $xml->createElement("data");
    $leadlist_details_get=$this->settings_model->get_leadlist_details_with_event_id(19);
    if($leadlist_details_get!='')
    {
    $i=1;
    $xml->appendChild($root);
    foreach($leadlist_details_get as $values){
       	$id   = $xml->createElement("BuyerFirstName");
    	$idText = $xml->createTextNode($values['buyer_first_name']);
    	$id->appendChild($idText);
        //--------------------------------//
    	$title   = $xml->createElement("BuyerLastName");
    	$titleText = $xml->createTextNode($values['buyer_last_name']);
    	$title->appendChild($titleText);
        //----------------------------------//
        $buyer_address_title   = $xml->createElement("BuyerAddress");
    	$buyer_address_titleText = $xml->createTextNode($values['buyer_address']);
    	$buyer_address_title->appendChild($buyer_address_titleText);
         //----------------------------------//
        $buyer_appartment_title   = $xml->createElement("BuyerApartment");
    	$buyer_appartment_titleText = $xml->createTextNode($values['buyer_appartment']);
    	$buyer_appartment_title->appendChild($buyer_appartment_titleText);
         //----------------------------------//
         $buyer_city_title   = $xml->createElement("BuyerCity");
    	$buyer_city_titleText = $xml->createTextNode($values['buyer_city']);
    	$buyer_city_title->appendChild($buyer_city_titleText);
         //----------------------------------//
        $buyer_province_title   = $xml->createElement("BuyerState");
    	$buyer_province_titleText = $xml->createTextNode($values['buyer_province']);
    	$buyer_province_title->appendChild($buyer_province_titleText);
         //----------------------------------//
          $buyer_postalcode_title   = $xml->createElement("BuyerZip");
    	$buyer_postalcode_titleText = $xml->createTextNode($values['buyer_postalcode']);
    	$buyer_postalcode_title->appendChild($buyer_postalcode_titleText);
         //----------------------------------//
          $buyer_homephone_title   = $xml->createElement("BuyerHomePhone");
    	$buyer_homephone_titleText = $xml->createTextNode($values['buyer_homephone']);
    	$buyer_homephone_title->appendChild($buyer_homephone_titleText);
         //----------------------------------//
         $buyer_businessphone_title   = $xml->createElement("BuyerBusinessPhone");
    	$buyer_businessphone_titleText = $xml->createTextNode($values['buyer_businessphone']);
    	$buyer_businessphone_title->appendChild($buyer_businessphone_titleText);
         //----------------------------------//
         $buyer_cellphone_title   = $xml->createElement("BuyerCellPhone");
    	$buyer_cellphone_titleText = $xml->createTextNode($values['buyer_cellphone']);
    	$buyer_cellphone_title->appendChild($buyer_cellphone_titleText);
         //----------------------------------//
         $buyer_email_title   = $xml->createElement("BuyerEmail");
    	$buyer_email_titleText = $xml->createTextNode($values['buyer_email']);
    	$buyer_email_title->appendChild($buyer_email_titleText);
         //----------------------------------//
         $sold_vehicle_stock_title   = $xml->createElement("SoldVehicleStock");
    	$sold_vehicle_stock_titleText = $xml->createTextNode($values['sold_vehicle_stock']);
    	$sold_vehicle_stock_title->appendChild($sold_vehicle_stock_titleText);
         //----------------------------------//
          $sold_vehicle_VIN_title   = $xml->createElement("SoldVehicleVIN");
    	$sold_vehicle_VIN_titleText = $xml->createTextNode($values['sold_vehicle_VIN']);
    	$sold_vehicle_VIN_title->appendChild($sold_vehicle_VIN_titleText);
         //----------------------------------//
      
          $new_used_title   = $xml->createElement("NeworUsed");
    	$new_used_titleText = $xml->createTextNode($values['new_used']);
    	$new_used_title->appendChild($new_used_titleText);
         //----------------------------------//
          $sold_vehicle_year_title   = $xml->createElement("SoldVehicleYear");
    	$sold_vehicle_year_titleText = $xml->createTextNode($values['sold_vehicle_year']);
    	$sold_vehicle_year_title->appendChild($sold_vehicle_year_titleText);
         //----------------------------------//
          $sold_vehicle_make_title   = $xml->createElement("SoldVehicleMake");
    	$sold_vehicle_make_titleText = $xml->createTextNode($values['sold_vehicle_make']);
    	$sold_vehicle_make_title->appendChild($sold_vehicle_make_titleText);
         //----------------------------------//
          $sold_vehicle_model_title   = $xml->createElement("SoldVehicleModel");
    	$sold_vehicle_model_titleText = $xml->createTextNode($values['sold_vehicle_model']);
    	$sold_vehicle_model_title->appendChild($sold_vehicle_model_titleText);
         //----------------------------------//
    	$book = $xml->createElement('Leadlist'.$i);
    	$book->appendChild($id);
    	$book->appendChild($title);
        $book->appendChild($buyer_address_title);
        $book->appendChild($buyer_appartment_title);
        $book->appendChild($buyer_city_title);
        $book->appendChild($buyer_province_title);
        $book->appendChild($buyer_postalcode_title);
        $book->appendChild($buyer_homephone_title);
        $book->appendChild($buyer_businessphone_title);
        $book->appendChild($buyer_cellphone_title);
        $book->appendChild($buyer_email_title);
        $book->appendChild($sold_vehicle_stock_title);
        $book->appendChild($sold_vehicle_VIN_title);
        $book->appendChild($new_used_title);
        $book->appendChild($sold_vehicle_year_title);
        $book->appendChild($sold_vehicle_make_title);
        $book->appendChild($sold_vehicle_model_title);
        $i++;
        $root->appendChild($book);
    }
	
	$xml->formatOutput = true;
	//echo "<xmp>". $xml->saveXML() ."</xmp>";
	$xml->save("mybooks.xml") or die("Error");
	} 
 
}

/* creates a compressed zip file */
function create_zip() {
$dealer_id=122;
$event_id=5;
$zipname=$dealer_id.'-'.$event_id;
$folder_in_zip = "/"; //root directory of the new zip file
$base_path = $this -> config -> item('rootpath');
$this->zip->archive($base_path.'/downloadreportzip/'.$zipname.'/exclusivereport.zip'); 
//$path = $base_path.'/downloadreportzip/my_backup.zip';
$this->zip->get_files_from_folder($base_path.'/downloadreportzip/'.$zipname.'/', 'exclusivereport.zip/');
$this->zip->download('exclusivereport.zip');
}
}