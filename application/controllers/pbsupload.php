<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Pbsupload extends CI_Controller
 {
 public function __construct() { 
 parent::__construct();
 $this -> load -> helper('url');
 $this -> load -> library('session');
 $this -> load -> helper('form');
 $this->load->model('login_model'); 
 $this->load->library("pagination");
 $this->load->model("main_model");
 $this->load->model("upload_model");
 $this->load->model("settings_model");
 }
public function getcustomerfeildnames()
{ 
    $customer_data_feild_name=array("Buyer Last Name","Buyer First Name","Buyer Address","Buyer Appartment #","Buyer Appartment","Buyer City","Buyer Province/State","Buyer Postal Code/Zip","Buyer Email","Buyer Home Phone","Buyer Business Phone","Buyer Cell Phone","Sold Vehicle Stock","Sold Vehicle VIN","Shortened VIN","New or Used","Sold Vehicle Year","Sold Vehicle Make","Sold Vehicle Model");
    return $customer_data_feild_name;
}
public function getfiancialfeildnames()
{ 
    $financial_data_feild_name=array("BodyDescription","ContractDate","FirstPaymentDate","VehiclePayoffDate","VehicleSalePrice","TotalSaleCredits","TotalCashDownAmount","TotalTax","TotalFinanceAmount","TotalofPayments","MonthlyPayment","ContractTerm","APR","PaymentFrequency","VehicleStock");
    return $financial_data_feild_name;
}
public function customer_data_databese_feildname_related_upload_file($file_feild_name)
{
    if($file_feild_name=='Buyer Last Name')
    {
    $return='buyer_last_name';
    }
    else if($file_feild_name=='BuyerLastName')
    {
    $return='buyer_last_name';
    }
    else if($file_feild_name=='Buyer First Name')
    {
    $return='buyer_first_name';
    }
    else if($file_feild_name=='BuyerFirstName')
    {
    $return='buyer_first_name';
    }
    else if($file_feild_name=='Buyer Address')
    {
    $return='buyer_address';
    }
    else if($file_feild_name=='BuyerAddress')
    {
    $return='buyer_address';
    }
    else if($file_feild_name=='Buyer Appartment #')
    {
    $return='buyer_appartment';
    }
    else if($file_feild_name=='BuyerApartment#')
    {
    $return='buyer_appartment';
    }
    else if($file_feild_name=='Buyer Appartment')
    {
    $return='buyer_appartment';
    }
    else if($file_feild_name=='Buyer City')
    {
    $return='buyer_city';
    }
    else if($file_feild_name=='BuyerCity')
    {
    $return='buyer_city';
    }
    else if($file_feild_name=='Buyer Province/State')
    {
    $return='buyer_province';
    }
    else if($file_feild_name=='BuyerState')
    {
    $return='buyer_province';
    }
    else if($file_feild_name=='Buyer Postal Code/Zip')
    {
    $return='buyer_postalcode';
    }
    else if($file_feild_name=='BuyerZip')
    {
    $return='buyer_postalcode';
    }
    else if($file_feild_name=='Buyer Email')
    {
    $return='buyer_email';
    }
    else if($file_feild_name=='BuyerEmail')
    {
    $return='buyer_email';
    }
    else if($file_feild_name=='Buyer Home Phone')
    {
    $return='buyer_homephone';
    }
    else if($file_feild_name=='BuyerHomePhone')
    {
    $return='buyer_homephone';
    }
    else if($file_feild_name=='Buyer Business Phone')
    {
    $return='buyer_businessphone';
    }
    else if($file_feild_name=='BuyerBusinessPhone')
    {
    $return='buyer_businessphone';
    }
    else if($file_feild_name=='Buyer Cell Phone')
    {
    $return='buyer_cellphone';
    }
    else if($file_feild_name=='BuyerCellPhone')
    {
    $return='buyer_cellphone';
    }
    else if($file_feild_name=='Sold Vehicle Stock')
    {
    $return='sold_vehicle_stock';
    }
    else if($file_feild_name=='SoldVehicleStock')
    {
    $return='sold_vehicle_stock';
    }
    else if($file_feild_name=='SoldVehicleVIN')
    {
    $return='sold_vehicle_VIN';
    }
    else if($file_feild_name=='Sold Vehicle VIN')
    {
    $return='sold_vehicle_VIN';
    }
    else if($file_feild_name=='Shortened VIN')
    {
    $return='shortened_VIN';
    }
    else if($file_feild_name=='ShortenedVIN')
    {
    $return='shortened_VIN';
    }
    else if($file_feild_name=='New or Used')
    {
    $return='new_used';
    }
    else if($file_feild_name=='NeworUsed')
    {
    $return='new_used';
    }
    else if($file_feild_name=='Sold Vehicle Year')
    {
    $return='sold_vehicle_year';
    }
    else if($file_feild_name=='SoldVehicleYear')
    {
    $return='sold_vehicle_year';
    }
    else if($file_feild_name=='Sold Vehicle Make')
    {
    $return='sold_vehicle_make';
    }
    else if($file_feild_name=='SoldVehicleMake')
    {
    $return='sold_vehicle_make';
    }
    else if($file_feild_name=='Sold Vehicle Model')
    {
    $return='sold_vehicle_model';
    }
    else if($file_feild_name=='SoldVehicleModel')
    {
    $return='sold_vehicle_model';
    }
    else
    {
    $return='';
    }
    return $return;
}
//read upload pbs csv files
function pbs_customer_csv_file_read()
{
    $file_name='BillHowichChrysler_Sales.csv';
    $root_path='/home/advantage/pbs/';
    $base_path='/home/advantage/public_html/';
    $file_path=$root_path.$file_name;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0; 
    $comacheck_updates=0;
    $rowcountget=17;
    //read csv rows
    foreach ($csv_file_display->titles as $values_titles){ 
        //$customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_titles);       
        $feile_insert[]=$values_titles;         
   }
   $p=1;  
   //read csv column    
   foreach ($csv_file_display->data as $row){
       $i=0;
       $count=0;
       $insertfeildvalues='';
       $updatefeildvalues='';
       foreach ($row as $line_of_text){  
           $values_get=$feile_insert[$i];
           $customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_get);
           $database_values=$customer_data_realed_database_feild_name;
           if($database_values!=''){
               if($comacheck>0)
               {
                $insertfeildvalues.=', ' ;
               }
               if($comacheck_updates>0)
               {
                 $updatefeildvalues.=' AND ' ;
               }
               $insertfeildvalues.=$database_values.'='."'".addslashes($line_of_text)."'";
               $updatefeildvalues.=$database_values.'='."'".addslashes($line_of_text)."'";
               $comacheck++;
               $count++;
               $comacheck_updates++;
               if($rowcountget==$count){
                   $last_insertid=$this->upload_model ->insert_pbs_customer_csv_files($insertfeildvalues,174,$updatefeildvalues);
                   $insertfeildvalues='';
                   $comacheck=0;
                   $comacheck_updates=0; 
                   break;
               }
           }
           $p++;    
           $i++; 
       }
   }

}
function pbs_customer_csv_file_read_copy()
{
    $file_name='BillHowichChrysler_Sales.csv';
    $root_path='/home/advantage/pbs/';
    $base_path='/home/advantage/public_html/';
    $file_path=$root_path.$file_name;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0; 
    $comacheck_updates=0;
    $rowcountget=17;
    //read csv rows
    foreach ($csv_file_display->titles as $values_titles){ 
        //$customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_titles);       
        $feile_insert[]=$values_titles;         
   }
   $p=1;  
   //read csv column  
   $count_sales_data= count($csv_file_display->data );
   $start_limit=20;
   $checklimit=$start_limit;
   $endlimit=30;
   $pbs_sales_data=$csv_file_display->data;
   for($checklimit=$start_limit;$checklimit<$endlimit;$checklimit++){
   //echo $checklimit."<br />";
   echo '<pre>';
   //print_r($pbs_sales_data[$checklimit]);
   echo '</pre>';
   $i=0;
   $count=0;
   $insertfeildvalues='';
   $updatefeildvalues='';
   foreach ($pbs_sales_data[$checklimit] as $line_of_text){  
        $values_get=$feile_insert[$i];
        $customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_get);
        $database_values=$customer_data_realed_database_feild_name;
        if($database_values!=''){
        if($comacheck>0)
        {
        $insertfeildvalues.=', ' ;
        }
        if($comacheck_updates>0)
        {
        $updatefeildvalues.=' AND ' ;
        }
        $insertfeildvalues.=$database_values.'='."'".addslashes($line_of_text)."'";
        $updatefeildvalues.=$database_values.'='."'".addslashes($line_of_text)."'";
        $comacheck++;
        $count++;
        $comacheck_updates++;
        if($rowcountget==$count){
        // $last_insertid=$this->upload_model ->insert_pbs_customer_csv_files_copy($insertfeildvalues,174,$updatefeildvalues);
         $insertfeildvalues.'<br />';
        $insertfeildvalues='';
        $comacheck=0;
        $comacheck_updates=0; 
        break;
        }
        }
        $p++;    
        $i++;
        
    }
    if($checklimit==$endlimit-1){
        echo $endlimit;
    } 
  }   
}
public function financial_data_databese_feildname_related_upload_file($file_feild_name)
{
    if($file_feild_name=='BodyDescription')
    {
    $return='bodydescription';
    }
    else if($file_feild_name=='ContractDate')
    {
    $return='contract_date';
    }
    else if($file_feild_name=='DeliveryDate')
    {
    $return='delivery_date';
    }
    else if($file_feild_name=='FirstPaymentDate')
    {
    $return='first_payment_date';
    }
    else if($file_feild_name=='VehiclePayoffDate')
    {
    $return='vehicle_payoff_date';
    }
    else if($file_feild_name=='VehicleSalePrice')
    {
    $return='vehiclesale_price';
    }
    else if($file_feild_name=='TotalSaleCredits')
    {
    $return='totalsale_credits';
    }
    else if($file_feild_name=='TotalCashDownAmount')
    {
    $return='total_cash_down_amount';
    }
    else if($file_feild_name=='TotalTax')
    {
    $return='total_tax';
    }
    else if($file_feild_name=='TotalFinanceAmount')
    {
    $return='total_finance_amount';
    }
    else if($file_feild_name=='TotalOfPayments')
    {
    $return='total_of_payments';
    }
    else if($file_feild_name=='MonthlyPayment')
    {
    $return='monthly_payment';
    }
    else if($file_feild_name=='APRRate')
    {
    $return='apr';
    }
    else if($file_feild_name=='APR')
    {
    $return='apr';
    }
    else if($file_feild_name=='PaymentFrequency')
    {
    $return='payment_frequency';
    }
    else if($file_feild_name=='VehicleStock')
    {
    $return='vehicle_stock';
    }
    else if($file_feild_name=='ContractTerm')
    {
    $return='contract_term';
    }
    else {
    $return='';
    }
    return $return;
}
  //read upload pbs csv files

function pbs_financial_csv_file_read()
{
    $file_name='BillHowichChryslerFandI6039.csv';
    $root_path='/home/advantage/pbs/';
    $base_path='/home/advantage/public_html/';
    $file_path=$root_path.$file_name;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0; 
    $comacheck_updates=0;
    print_r($csv_file_display->titles);
    foreach ($csv_file_display->titles as $values_titles){ 
    $feile_insert[]=$values_titles;         
    }
    $p=1;
    $x=1;
    $count_sales_data= count($csv_file_display->data );
    $start_limit=0;
    $checklimit=$start_limit;
    $endlimit=30;
    $pbs_sales_data=$csv_file_display->data;
    for($checklimit=$start_limit;$checklimit<$endlimit;$checklimit++){
        $i=0;
        $count=0;
        $insertfeildvalues='';
        $updatefeildvalues='';
        foreach ($pbs_sales_data[$checklimit]  as $line_of_text){  
            $values_get=$feile_insert[$i];
            $customer_data_realed_database_feild_name=$this -> financial_data_databese_feildname_related_upload_file($values_get);
            $database_values=$customer_data_realed_database_feild_name;
            if($database_values!=''){
                if($comacheck>0)
                {
                    $insertfeildvalues.=', ' ;
                }
                if($comacheck_updates>0)
                {
                 $updatefeildvalues.=' AND ' ;
                }
                $insertfeildvalues.=$database_values.'='."'".addslashes($line_of_text)."'";
                $comacheck++;
                $count++;
                $comacheck_updates++;
                if($rowcountget==$count){
                    $last_insertid=$this->upload_model ->insert_pbs_finacial_csv_files($insertfeildvalues);
                    $insertfeildvalues='';
                    $comacheck=0;
                    $comacheck_updates=0; 
                    break;
                }
            }
             $p++;    
             $i++; 
         }
         $x++;
          if($checklimit==$endlimit-1){
        echo $endlimit;
    } 
     }
}
function pbs_financial_csv_file_read_test()
{
$file_handle = fopen("/home/advantage/pbs/BillHowichChryslerFandI6039.csv", "r");
echo $i =0;
$start=0;
$alreadyProcessed=10;
$count=1;
$line_of_text1 = fgetcsv($file_handle);
print_r($line_of_text1);
while($line_of_text = fgetcsv($file_handle)) 
{
if($count>$alreadyProcessed)
{
exit;
}
if($start==0)
{
if ($i++ < $alreadyProcessed) 
{
    $vehicle_stock=addslashes($line_of_text[65]);
    $bodydescription=addslashes($line_of_text[115]);
    $contract_date=addslashes($line_of_text[4]);
    $first_payment_date=addslashes($line_of_text[44]);
    $vehicle_payoff_date=addslashes($line_of_text[47]);
    $vehiclesale_price=addslashes($line_of_text[11]);
    $totalsale_credits=addslashes($line_of_text[12]);
    $total_cash_down_amount=addslashes($line_of_text[30]);
    $total_tax=addslashes($line_of_text[34]);
    $total_finance_amount=addslashes($line_of_text[38]);
    $total_of_payments=addslashes($line_of_text[45]);
    $monthly_payment=addslashes($line_of_text[43]);
    $contract_term=addslashes($line_of_text[39]);
    $apr=addslashes($line_of_text[40]);
    $payment_frequency=addslashes($line_of_text[46]);
    $delivery_date=addslashes($line_of_text[5]);
    //exclude first row insert to the table 
    if($vehicle_stock=='VehicleStock' || $bodydescription=='BodyDescription' ||  $contract_date=='ContractDate' ||  $first_payment_date=='FirstPaymentDate' ||  $vehicle_payoff_date=='VehiclePayoffDate' ||  $vehiclesale_price=='VehicleSalePrice' ||  $totalsale_credits=='TotalSaleCredit' || $total_cash_down_amount=='TotalCashDownAmount' ||  $total_tax=='TotalTax' || $total_finance_amount=='TotalFinanceAmount' || $total_of_payments=='TotalofPayments' || $monthly_payment=='MonthlyPayment' || $contract_term=='MonthlyPayment' || $contract_term=='ContractTerm' || $apr=='APR' || $payment_frequency=='PaymentFrequency' || $delivery_date=='DeliveryDate') 
    {
    
    }
    else
    {
    $sql_insert=mysql_query("INSERT INTO  pbs_financial_data_copy set
    vehicle_stock='$vehicle_stock',
    bodydescription='$bodydescription',
    contract_date='$contract_date',
    first_payment_date='$first_payment_date',
    vehicle_payoff_date='$vehicle_payoff_date',
    vehiclesale_price='$vehiclesale_price',
    totalsale_credits='$totalsale_credits',
    total_cash_down_amount='$total_cash_down_amount',
    total_tax='$total_tax',
   	total_finance_amount='$total_finance_amount',
    total_of_payments='$total_of_payments',
    monthly_payment='$monthly_payment',
    contract_term='$contract_term',
    apr='$apr',
   	payment_frequency='$payment_frequency',
    delivery_date='$delivery_date'
    ");
    }

}
}
else{
if($count<$i)
{
    
}
else
{
    if ($i++ <$alreadyProcessed) 
    {
        
        //read each row from csv file 
        //$line_of_text = fgetcsv($file_handle, 1000000);
        
        //echo $line_of_text[0] . $line_of_text[1]. $line_of_text[2]. $line_of_text[3] . $line_of_text[4]. $line_of_text[5] . $line_of_text[6] . $line_of_text[7]. $line_of_text[8] .$line_of_text[9] . $line_of_text[10]. $line_of_text[11] ."<BR>";
        $add_make=addslashes($line_of_text[0]);
        $year=addslashes($line_of_text[1]);
        $add_model=addslashes($line_of_text[2]);
        $trim=addslashes($line_of_text[3]);
        $engine=addslashes($line_of_text[4]);
        $add_part_category=addslashes($line_of_text[5]);
        $part_subcategory=addslashes($line_of_text[6]);
        $part_number=addslashes($line_of_text[7]);
        $add_partdes=addslashes($line_of_text[8]);
        $image_id=addslashes($line_of_text[9]);
        $image_name=addslashes($line_of_text[10]);
        //exclude first row insert to the table 
        if($add_make=='Make' || $year=='Year' ||  $add_model=='Model' ||  $trim=='Trim' ||  $engine=='Engine' ||  $add_part_category=='Part Category' ||  $part_subcategory=='Part Subcategory' || $part_number=='Part Number' ||  $add_partdes=='Part Description' || $image_id=='Image Reference No' || $image_name=='Image Filename' ) 
        {
        
        }
    else{
    $sql_insert=mysql_query("INSERT INTO  pbs_financial_data_copy set
    vehicle_stock='$vehicle_stock',
    bodydescription='$bodydescription',
    contract_date='$contract_date',
    first_payment_date='$first_payment_date',
    vehicle_payoff_date='$vehicle_payoff_date',
    vehiclesale_price='$vehiclesale_price',
    totalsale_credits='$totalsale_credits',
    total_cash_down_amount='$total_cash_down_amount',
    total_tax='$total_tax',
   	total_finance_amount='$total_finance_amount',
    total_of_payments='$total_of_payments',
    monthly_payment='$monthly_payment',
    contract_term='$contract_term',
    apr='$apr',
   	payment_frequency='$payment_frequency',
    delivery_date='$delivery_date'
    ");
        }
        
    }
}
}
$count++;
}
}
function read_authenticom_file_from_dealer_folder(){
$sql=("SELECT *
FROM ftp_feed_details
WHERE status = 'incomplete'");
$query=$this->db->query($sql);
if($query -> num_rows() > 0){
    $returnvalue= $query->result_array();
    foreach ($returnvalue as $values){
    $start_limit=$values['start_limt'];
    $end_limit=$values['end_limit'];
    $filename=$values['filename'];
    $source=$values['source'];
    $dealership_id=$values['dealership_id'];
    $this->filereadstatuscheck($filename);  
    }    
}
}
function filereadstatuscheck($filename){
$sql=("SELECT *
FROM ftp_feed_details
WHERE filename = '$filename'");
$query=$this->db->query($sql);
if($query -> num_rows() > 0){
    $returnvalue= $query->result_array();
    foreach ($returnvalue as $values){
    $start_limit=$values['start_limt'];
    $end_limit=$values['end_limit'];
    $filename=$values['filename'];
    $source=$values['source'];
    $status=$values['status'];
    $dealership_id=$values['dealership_id'];
    if($status=='incomplete') {
    $staus_get=$this->authenticom_customer_csv_file_read1($start_limit,$end_limit,$dealership_id,$filename );
   
    } 
    }    
}    
}
function authenticom_customer_csv_file_read1( $start_limit,$end_limit,$dealership_id,$filename )
{   $dealership_id=170;
    $user_details=$this -> main_model-> dealerfulldetails($dealership_id);
    foreach($user_details as $details){
        $folder_name=$details['folder_name'];
    }
    $filename='HIST_wellington_motors_SL.CSV';
    $base_path = $this -> config -> item('rootpath');
    $file_path=$base_path.'clients/'.$folder_name.'/'.$filename;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0;
    $comacheck_finacial=0;
    foreach ($csv_file_display->titles as $values_titles){ 
         if($values_titles!=''){
        $feile_insert[]=$values_titles;         
   }
   }
   $p=1; 
   $rowcountget=count($csv_file_display->titles); 
   $checklimit=$start_limit;
   $pbs_sales_data=$csv_file_display->data; 
   $total_row_count= count($csv_file_display->data);
   if($total_row_count!=''){
       for($checklimit=$start_limit;$checklimit<$end_limit;$checklimit++){
       if($checklimit<$total_row_count)
        {
         $i=0;
            $count=0;
            $insertfeildvalues='';
            $insertfeildvalues_financial='';
            $insertfeildvalues1='';
            if($pbs_sales_data[$checklimit]!=''){
            foreach ($pbs_sales_data[$checklimit] as $line_of_text){  
                if($i<17)
                {
                if($comacheck>0)
                {
                    $insertfeildvalues.=', ' ;
                }
                    $values_get=$feile_insert[$i];
                    $customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_get);
                    $database_values=$customer_data_realed_database_feild_name;
                    $insertfeildvalues.=$database_values.'='."'".trim(addslashes($line_of_text))."'";
                    if($feile_insert[$i]=='SoldVehicleStock'){
                     $insertfeildvalues1.='vehicle_stock='."'".trim(addslashes($line_of_text))."',";   
                     }
                    
                }
                    else{
                    $values_get=$feile_insert[$i];
                    $financial_realed_database_feild_name=$this -> financial_data_databese_feildname_related_upload_file($values_get);
                   
                    
                    if($comacheck_finacial>17)
                    {
                    $insertfeildvalues_financial.=', ' ;
                    }
                      $insertfeildvalues_financial.=$financial_realed_database_feild_name.'='."'".trim(addslashes($line_of_text))."'";
                      
                    }
                    $comacheck++;
                    $comacheck_finacial++;
                    $count++;
                    if($rowcountget==$count){
                        $financialdata=$insertfeildvalues1.$insertfeildvalues_financial;
                        //$last_insertid=$this->upload_model ->insert_pbs_customer_csv_files_authenticom($insertfeildvalues,$dealership_id);
                        //$last_pbsdatainsertid=$this->upload_model ->insert_pbs_finacial_csv_files($financialdata);
                        $insertfeildvalues='';
                        $insertfeildvalues_financial='';
                        $comacheck=0; 
                        $comacheck_finacial=0;
                        break;
                    }
                     $p++;    
                     $i++; 
                 }
                 if($total_row_count!=$checklimit){
                     if($checklimit==$end_limit-1){
                      $start_limit=$end_limit;
                      $endlimit=$end_limit+1000;
                      $updatestatus_status="UPDATE ftp_feed_details set 
                                            start_limt=$start_limit,
                                   	        end_limit=$endlimit,
                                            status='incomplete'
                                            where filename='$filename' and dealership_id=$dealership_id";
                                            $result = $this -> db -> query($updatestatus_status); 
                        $this->authenticom_customer_csv_file_read1($start_limit,$endlimit,$dealership_id,$filename);
                     }
                }
                else{
                     $updatestatus_status="UPDATE ftp_feed_details set 
                     status='complete'
                     where filename='$filename' and dealership_id=$dealership_id";
                     $result = $this -> db -> query($updatestatus_status);    
                }
           }
           }
           }
   }
   else{
               $admin_emailid= $this -> config -> item('admin_address'); 
               $subject='Ftp file move faild';
               $message.= 'Dear Admin,<br/><br/>
               The file '.$file.' is faild to move in to the folder<br/><br/>';
               $message.='Regards,<br/>Exclusive Private Sale.Inc';  
               $this->main_model->HTMLemail($admin_emailid,$admin_emailid,'',$subject,$message); 
       }

}
function authenticom_customer_csv_file_read()
{ 
    $file_name='HIST_twin_motors_SL.CSV';
    $root_path='/home/advantage/authenticom/';
    $base_path='/home/advantage/public_html/';
    $file_path=$root_path.$file_name;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0;
    $comacheck_finacial=0;
    foreach ($csv_file_display->titles as $values_titles){ 
         
        $feile_insert[]=$values_titles;         
   }
    $p=1; 
   $rowcountget=count($csv_file_display->titles);   
   foreach ($csv_file_display->data as $row){
   $i=0;
    $count=0;
    $insertfeildvalues='';
    $insertfeildvalues_financial='';
    $insertfeildvalues1='';
    foreach ($row as $line_of_text){  
    if($i<17)
    {
    if($comacheck>0)
    {
        $insertfeildvalues.=', ' ;
    }
        $values_get=$feile_insert[$i];
        $customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_get);
        $database_values=$customer_data_realed_database_feild_name;
        $insertfeildvalues.=$database_values.'='."'".trim(addslashes($line_of_text))."'";
        if($feile_insert[$i]=='SoldVehicleStock'){
         $insertfeildvalues1.='vehicle_stock='."'".trim(addslashes($line_of_text))."',";   
         
        }
        
    }
        else{
        $values_get=$feile_insert[$i];
        $financial_realed_database_feild_name=$this -> financial_data_databese_feildname_related_upload_file($values_get);
       
        
        if($comacheck_finacial>17)
        {
        $insertfeildvalues_financial.=', ' ;
        }
          $insertfeildvalues_financial.=$financial_realed_database_feild_name.'='."'".trim(addslashes($line_of_text))."'";
          
        }
        $comacheck++;
        $comacheck_finacial++;
         $count++;
         if($rowcountget==$count){
         $financialdata=$insertfeildvalues1.$insertfeildvalues_financial;
        $last_insertid=$this->upload_model ->insert_pbs_customer_csv_files_authenticom($insertfeildvalues,204);
        $last_pbsdatainsertid=$this->upload_model ->insert_pbs_finacial_csv_files($financialdata);
        $insertfeildvalues='';
        $insertfeildvalues_financial='';
         $comacheck=0; 
         $comacheck_finacial=0;
         break;
         }
         
         $p++;    
         $i++; 
         }
    //echo $insertfeildvalues_financial;
    }

}
            function uploadxlx(){
    
                            //it is xlsx file
                         require_once 'uploadfile/reader.php';  
                         $excel = new Spreadsheet_Excel_Reader();
                         //check whether it is customer data or not
                          $base_path = $this -> config -> item('rootpath');
                        $file_path=$base_path;
                         //$excel->read($file_path.$file.'/'.$fileread); 
                          $excel->read($file_path.'/Warranty.xls'); 
                          $x=2;
                          while($x<=$excel->sheets[0]['numRows']) 
                          { // reading row by row 
                              $insert='';
                              $comacheck=0;
                             //read excell cell
                           $y=1;
                            while($y<=$excel->sheets[0]['numCols']) 
                            {
                                if(isset($excel->sheets[0]['cells'][$x][$y]) && ($excel->sheets[0]['cells'][$x][$y])!='')
                                {
                                        
                                             if($comacheck>0)
                                            {
                                                $insert.=', ' ;
                                            }
                                            //$customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($excel->sheets[0]['cells'][1][$y]); 
                                            $insert.=$excel->sheets[0]['cells'][1][$y].'='."'".addslashes($excel->sheets[0]['cells'][$x][$y])."'";
                                            $comacheck++; 
                                        echo $excel->sheets[0]['cells'][$x][$y].'<br />';
                                       
                                }  
                                $y++;   
                            }
                          //$last_insertid=$this->upload_model ->insert_manufacure($insert);
                            $x++;
                         }
                        
                   
 
 }  
 function testcsv()
{
    $file_name='test.csv';
    $root_path='/home/advantage/pbs/';
    $base_path = $this -> config -> item('rootpath');
    $file_path=$base_path.$file_name;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0; 
    
    //print_r($csv_file_display->titles). ' --- entered';
    
   foreach ($csv_file_display->titles as $values_titles){ 
        //$customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_titles);       
        $feile_insert[]=$values_titles;         
   }
     
   //resding csv felid values as row
   $p=1;      
   foreach ($csv_file_display->data as $row){
   //resding csv felid values as column 
   
    $i=0;
    $rowcountget=5;
    $count=0;
    $insertfeildvalues='';
        foreach ($row as $line_of_text){  
        $values_get=$feile_insert[$i];
        
       // $customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_get);
        $database_values=$feile_insert[$i];
        
    if($database_values!=''){
    //echo 'values - '.$database_values.'<br />';
    if($comacheck>0)
    {
    $insertfeildvalues.=', ' ;
    }
    //resding databese chsnge feild values and csv orginal feild values 
     //ckeck feild change null
    $insertfeildvalues.=$database_values.'='."'".trim(addslashes($line_of_text))."'";
    
     //call insert function
     $comacheck++;
     $count++;
     if($rowcountget==$count){
     $last_insertid=$this->upload_model ->insert_manufacure($insertfeildvalues);
     
     $insertfeildvalues='';
     $comacheck=0; 
     break;
     }
     }
       //echo $insertfeildvalues;
        $p++;    
        $i++; 
        }
    
    }
    // echo $p;
    //return $last_insertid; 
} 
   public function testfeildnames()
  { 
    $customer_data_feild_name=array("Buyer Last Name","Buyer First Name","Buyer Address","Buyer Appartment #","Buyer Appartment","Buyer City","Buyer Province/State","Buyer Postal Code/Zip","Buyer Email","Buyer Home Phone","Buyer Business Phone","Buyer Cell Phone","Sold Vehicle Stock","Sold Vehicle VIN","Shortened VIN","New or Used","Sold Vehicle Year","Sold Vehicle Make","Sold Vehicle Model","BodyDescription","ContractDate","FirstPaymentDate","vehicleType","L/100kmCombined","TradeinValue","TotalCashDownAmount","TotalTax","transmissionType","TotalofPayments","MonthlyPayment","ContractTerm","APR");
    return $customer_data_feild_name;
  }
  public function testfeildnames_feildname_related_upload_file($file_feild_name)
  {
    
       if($file_feild_name=='Buyer First Name')
    {
        $return='buyer_first_name';
    }
    else if($file_feild_name=='Buyer Last Name'){
       $return='buyer_last_name'; 
    }
    else if($file_feild_name=='Buyer Address'){
       $return='buyer_address'; 
    }
    else if($file_feild_name=='Buyer Appartment #'){
       $return='buyer_appartment'; 
    }
    else if($file_feild_name=='Buyer City'){
       $return='buyer_city'; 
    }
    else if($file_feild_name=='Buyer Province/State'){
       $return='buyer_province'; 
    }
    else if($file_feild_name=='Buyer Postal Code/Zip'){
       $return='buyer_postalcode'; 
    }
     else if($file_feild_name=='vehicleType')
    {
        $return='vehicletype';
    }
     else if($file_feild_name=='BuyerZip')
    {
        $return='buyer_postalcode';
    }
     else if($file_feild_name=='Buyer Email')
    {
        $return='buyer_email';
    }
     else if($file_feild_name=='BuyerEmail')
    {
        $return='buyer_email';
    }
     else if($file_feild_name=='Buyer Home Phone')
    {
        $return='buyer_homephone';
    }
      else if($file_feild_name=='BuyerHomePhone')
    {
        $return='buyer_homephone';
    }
       else if($file_feild_name=='Buyer Business Phone')
    {
        $return='buyer_businessphone';
    }
         else if($file_feild_name=='BuyerBusinessPhone')
    {
        $return='buyer_businessphone';
    }
       else if($file_feild_name=='Buyer Cell Phone')
    {
        $return='buyer_cellphone';
    }
         else if($file_feild_name=='BuyerCellPhone')
    {
        $return='buyer_cellphone';
    }
        else if($file_feild_name=='Sold Vehicle Stock')
    {
        $return='sold_vehicle_stock';
    }
        else if($file_feild_name=='SoldVehicleStock')
    {
        $return='sold_vehicle_stock';
    }
        else if($file_feild_name=='Sold Vehicle VIN')
    {
        $return='sold_vehicle_VIN';
    }
         else if($file_feild_name=='Sold Vehicle VIN')
    {
        $return='sold_vehicle_VIN';
    }
    else if($file_feild_name=='Shortened VIN')
    {
        $return='shortened_VIN';
    }
     else if($file_feild_name=='ShortenedVIN')
    {
        $return='shortened_VIN';
    }
          else if($file_feild_name=='New or Used')
    {
        $return='new_used';
    }
            else if($file_feild_name=='NeworUsed')
    {
        $return='new_used';
    }
    else if($file_feild_name=='Sold Vehicle Year')
    {
        $return='sold_vehicle_year';
    }
     else if($file_feild_name=='SoldVehicleYear')
    {
        $return='sold_vehicle_year';
    }
    else if($file_feild_name=='Sold Vehicle Make')
    {
        $return='sold_vehicle_make';
    }
    else if($file_feild_name=='SoldVehicleMake')
    {
        $return='sold_vehicle_make';
    }
    else if($file_feild_name=='Sold Vehicle Model')
    {
        $return='sold_vehicle_model';
    }
      else if($file_feild_name=='SoldVehicleModel')
    {
        $return='sold_vehicle_model';
    }
    else if($file_feild_name=='BodyDescription')
    {
        $return='bodydescription';
    }
    else if($file_feild_name=='FirstPaymentDate')
    {
        $return='first_payment_date';
    }
     else if($file_feild_name=='transmissionType')
    {
        $return='transmissiontype';
    }
      else if($file_feild_name=='L/100kmCombined')
    {
        $return='littercombined';
    }
      else if($file_feild_name=='ContractDate')
    {
        $return='contractdate';
    }
        else if($file_feild_name=='TradeinValue')
    {
        $return='tradeinvalue';
    }
    else if($file_feild_name=='FirstPaymentDate')
    {
        $return='1stPayment';
    }
     else if($file_feild_name=='APR')
    {
        $return='APR';
    }
     else if($file_feild_name=='MonthlyPayment')
    {
        $return='monthlypayment';
    }
     
       else if($file_feild_name=='ContractTerm')
    {
        $return='contract_term';
    }
    
    else {
        $return='';
    }
    return $return;
  } 
  function testbuyerfile($field){
    //echo $field;
    if($field=='Buyer First Name')
    {
        $feild_vaue='buyer_first_name';
    }
    else if($field=='Buyer Last Name'){
       $feild_vaue='buyer_last_name'; 
    }
    else if($field=='Buyer Address'){
       $feild_vaue='buyer_address'; 
    }
    else if($field=='Buyer Appartment #'){
       $feild_vaue='buyer_appartment'; 
    }
    else if($field=='Buyer City'){
       $feild_vaue='buyer_city'; 
    }
    else if($field=='Buyer Province/State'){
       $feild_vaue='buyer_province'; 
    }
    else if($field=='Buyer Postal Code/Zip'){
       $feild_vaue='buyer_postalcode'; 
    }
  
    return $feild_vaue;
  }             
function testdataepsupload(){
    $base_path = $this -> config -> item('rootpath');
     require_once 'uploadfile/reader.php';  
     $excel = new Spreadsheet_Excel_Reader();
     $excel->read('/home/advantage/public_html/test-upload.xls'); 
     $x=2;
     while($x<=$excel->sheets[0]['numRows']){ // reading row by row 
     $insert='';
     $comacheck=0;
     $y=1;
     while($y<=$excel->sheets[0]['numCols']){
         if(isset($excel->sheets[0]['cells'][$x][$y]) && ($excel->sheets[0]['cells'][$x][$y])!='') {
             $customer_data_feild_name=$this -> testfeildnames(); 
             //check the feild existsnce 
             
             if(in_array($excel->sheets[0]['cells'][1][$y],$customer_data_feild_name)){
            //echo ($excel->sheets[0]['cells'][1][$y]).'<br />';
                $customer_data_realed_database_feild_name=$this -> testfeildnames_feildname_related_upload_file($excel->sheets[0]['cells'][1][$y]);
                echo $customer_data_realed_database_feild_name;
                if($customer_data_realed_database_feild_name!='')
                {
                if($comacheck>0){
                $insert.=', ' ;
             }
                $insert.=$customer_data_realed_database_feild_name.'='."'".addslashes($excel->sheets[0]['cells'][$x][$y])."'";
             $comacheck++; 
             }
             }
         }  
         $y++;  
        // echo  $insert;
     }
      $last_insertid=$this->upload_model ->insert_pbstest_data($insert);
    $x++;
}
}


function upload_customer_data(){
    $sql=("SELECT  * from pbs_customer_data where dealership_id=204 order by id limit 1000,1000
    ");
    $query1=$this->db->query($sql);
    if($query1 -> num_rows() > 0)
    {
    $returnvalue1= $query1->result_array();
    $i=1;
    foreach($returnvalue1 as $value1){
                $dealership_id=addslashes($value1['dealership_id']);
                $buyer_first_name=addslashes($value1['buyer_first_name']);
                $buyer_last_name=addslashes($value1['buyer_last_name']);
                $buyer_address=addslashes($value1['buyer_address']);
                $buyer_appartment=addslashes($value1['buyer_appartment']);
                $buyer_city=addslashes($value1['buyer_city']);
                $buyer_province=addslashes($value1['buyer_province']);
                $buyer_postalcode=addslashes($value1['buyer_postalcode']);
                $buyer_homephone=addslashes($value1['buyer_homephone']);
                $buyer_businessphone=addslashes($value1['buyer_businessphone']);
                $buyer_cellphone=addslashes($value1['buyer_cellphone']);
                $buyer_email=addslashes($value1['buyer_email']);
                $sold_vehicle_stock=addslashes($value1['sold_vehicle_stock']);
                $sold_vehicle_VIN=addslashes($value1['sold_vehicle_VIN']);
                $shortened_VIN=addslashes($value1['shortened_VIN']);
                $new_used=addslashes($value1['new_used']);
                $sold_vehicle_year=addslashes($value1['sold_vehicle_year']);
                $sold_vehicle_make=addslashes($value1['sold_vehicle_make']);
                $sold_vehicle_model=addslashes($value1['sold_vehicle_model']);
                 
                 $updatestatus_status="UPDATE eps_data set 
                 dealership_id=$dealership_id,
                buyer_first_name='$buyer_first_name',
                buyer_last_name='$buyer_last_name',
                buyer_address='$buyer_address',
                buyer_appartment='$buyer_appartment',
                buyer_city='$buyer_city',
                buyer_province='$buyer_province',
                buyer_postalcode='$buyer_postalcode',
                buyer_homephone='$buyer_homephone',
                buyer_cellphone='$buyer_cellphone',
                buyer_email='$buyer_email',
                buyer_businessphone='$buyer_businessphone',
                sold_vehicle_stock='$sold_vehicle_stock',
                sold_vehicle_VIN='$sold_vehicle_VIN',
                shortened_VIN='$shortened_VIN',
                new_used='$new_used',
                sold_vehicle_year='$sold_vehicle_year',
                sold_vehicle_make='$sold_vehicle_make',
                sold_vehicle_model='$sold_vehicle_model'
                where  customer_id='$value1[id]'";
                
                $result = $this -> db -> query($updatestatus_status); 
                if($result){
                   $i++;
                }  
        }
                 
    }
           
}

function upload_finacial_data(){
    $sql=("SELECT  sold_vehicle_stock,id from pbs_customer_data where dealership_id=204 order by id limit 2000,500
    ");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
    $returnvalue= $query->result_array();
    foreach($returnvalue as $value){
    $sql1=("SELECT * 
    FROM  	pbs_financial_data
    WHERE vehicle_stock = '$value[sold_vehicle_stock]'");
    $query1=$this->db->query($sql1);
    if($query1 -> num_rows() > 0)
    {
    $returnvalue1= $query1->result_array();
    foreach($returnvalue1 as $value1){
                $buyer_first_name=addslashes($value1['bodydescription']);
                $buyer_last_name=addslashes($value1['contract_date']);
                $buyer_address=addslashes($value1['first_payment_date']);
                $buyer_appartment=addslashes($value1['delivery_date']);
                if($buyer_last_name!=''){
                $contract_date_strtime=strtotime($buyer_last_name);
                }
                else{
                  $contract_date_strtime='';  
                }
                if($buyer_address!=''){
                $first_payment_date_strtime=strtotime($buyer_address);
                }
                else{
                  $first_payment_date_strtime='';  
                }
                if($buyer_appartment!=''){
                $delivery_date_strtime=strtotime($buyer_appartment);
                }
                else{
                    $delivery_date_strtime='';
                }
                $buyer_city=addslashes($value1['vehicle_payoff_date']);
                $buyer_province=addslashes($value1['vehiclesale_price']);
                $buyer_postalcode=addslashes($value1['total_finance_amount']);
                $buyer_homephone=addslashes($value1['total_of_payments']);
                $buyer_businessphone=addslashes($value1['monthly_payment']);
                $buyer_cellphone=addslashes($value1['contract_term']);
                $buyer_email=addslashes($value1['apr']);
                $payment_frequency=addslashes($value1['payment_frequency']);
                
                 
                 $updatestatus_status="UPDATE eps_data set 
                bodydescription='$buyer_first_name',
                contract_date='$contract_date_strtime',
                first_payment_date='$first_payment_date_strtime',
                delivery_date='$delivery_date_strtime',
                vehicle_payoff_date='$buyer_city',
                vehiclesale_price='$buyer_province',
                total_finance_amount='$buyer_postalcode',
                total_of_payments='$buyer_homephone',
                contract_term='$buyer_cellphone',
                payment_frequency='$payment_frequency',
                monthly_payment='$buyer_businessphone',
                apr='$buyer_email'
                where  customer_id='$value[id]'";
                $result = $this -> db -> query($updatestatus_status); 
                if($updatestatus_status)
                {
                    echo "Updated";
                }
                  
        }
                 
        }
    }
}
}
function upload_powerratio(){
    $sql=("SELECT eps_data.styleid from eps_data,pbs_customer_data where 
    eps_data.customer_id=pbs_customer_data.id AND 
    pbs_customer_data.dealership_id=204 order by eps_data.id limit 0,1000
            ");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
    $returnvalue= $query->result_array();
    foreach($returnvalue as $value){
    $sql1=("SELECT powerratio,styleid
            FROM  	pbs_engine_data
            WHERE styleId = $value[styleid]");
            $query1=$this->db->query($sql1);
            if($query1 -> num_rows() > 0)
            {
            $returnvalue1= $query1->result_array();
            foreach($returnvalue1 as $value1){
            $updatestatus_status="UPDATE eps_data set 
            powerratio='$value1[powerratio]'
            where styleid=$value[styleid]
            ";
            $result = $this -> db -> query($updatestatus_status); 
            }
        }
}
}
}
function insertmastertable()
{
    $query_master_vehicle=("select  
            pbs_financial_data.bodydescription,
            pbs_customer_data.sold_vehicle_VIN,
            pbs_customer_data.sold_vehicle_year,
            pbs_customer_data.sold_vehicle_make,
            pbs_customer_data.sold_vehicle_model 
            FROM pbs_financial_data ,pbs_customer_data 
            where pbs_financial_data .vehicle_stock=pbs_customer_data.sold_vehicle_stock 
            GROUP BY pbs_customer_data.sold_vehicle_year,
            pbs_customer_data.sold_vehicle_make,
            pbs_customer_data.sold_vehicle_model order by pbs_customer_data.id 
           	");
        $result_master_vehicle=$this->db->query($query_master_vehicle);
        if($result_master_vehicle -> num_rows() > 0)
        {
          
            $returnvalue_master_data=$result_master_vehicle->result_array();
            foreach($returnvalue_master_data as $customerdata) {
            $bodydescription=trim(addslashes($customerdata['bodydescription']));
            $sold_vehicle_make=trim(addslashes($customerdata['sold_vehicle_make']));
            $sold_vehicle_model=trim(addslashes($customerdata['sold_vehicle_model']));
            $sold_vehicle_year=trim($customerdata['sold_vehicle_year']);
            $sold_vehicle_VIN=trim($customerdata['sold_vehicle_VIN']);
            $quantity=$this->settings_model ->getcountofmakemodel($sold_vehicle_year,$sold_vehicle_make,$sold_vehicle_model);
            $query_master_vehicle_exsist=("select  * from eps_master_vehicle where 
            quantity=$quantity and 
            vin='$sold_vehicle_VIN' and
            sold_vehicle_year='$sold_vehicle_year' and
            sold_vehicle_make='$sold_vehicle_make' and
            sold_vehicle_model='$sold_vehicle_model' and
            bodydescription='$bodydescription'
           	");
            $result_query_master_vehicle_exsist=$this->db->query($query_master_vehicle_exsist);
            if($result_query_master_vehicle_exsist -> num_rows() > 0){
            
            }
            else{
             $insert_master_table=("insert into  eps_master_vehicle set quantity=$quantity,
             vin='$sold_vehicle_VIN',
             sold_vehicle_year='$sold_vehicle_year',
             sold_vehicle_make='$sold_vehicle_make',
             sold_vehicle_model='$sold_vehicle_model',
             bodydescription='$bodydescription'
             ");
             $insert_master=$this->db->query($insert_master_table);   
            }
            }
        }
}


function insertintoepsdata(){
$query1=("SELECT sold_vehicle_year,sold_vehicle_make, sold_vehicle_model,id FROM pbs_customer_data  WHERE
dealership_id =204 order by id limit 1000,1000 ");
$result1=$this->db->query($query1);
if($result1 -> num_rows() > 0)
{
$returnvalue1= $result1->result_array();
foreach($returnvalue1 as $value1){
    $id=$value1['id'];
    $year=trim($value1['sold_vehicle_year']);
    $sold_vehicle_make=trim($value1['sold_vehicle_make']);
    $sold_vehicle_model=trim(addslashes($value1['sold_vehicle_model']));
    if($sold_vehicle_make!=''){
    $query2=("SELECT * FROM Vehicle  WHERE
    year =$year  and make='$sold_vehicle_make' and model='$sold_vehicle_model'");
    $result2 = $this -> db -> query($query2);  
    if($result2 -> num_rows() > 0)
    {
        $price='253.214584327527';
        $returnvalue2= $result2->result_array();
        foreach($returnvalue2 as $value2){
         $styleId=$value2['styleId'];  
         $vehicletype=addslashes($value2['vehicleType']); 
         $vehiclestyle=addslashes($value2['vehicleStyle']);
         $vehiclesize=addslashes($value2['vehicleSize']); 
         $vehiclecategory=addslashes($value2['vehicleCategory']); 
         $enginefueltype=addslashes($value2['engineFuelType']) ;
         $drivenwheels=addslashes($value2['drivenWheels']);
         $transmissiontype=addslashes($value2['transmissionType']);
         $numberofdoors=addslashes($value2['numberOfDoors']); 
         $mpgcity=addslashes($value2['mpgCity']); 
         $mpghighway=addslashes($value2['mpgHighway']);
         $mpgcombined=addslashes($value2['mpgCombined']);
         $curbweight=addslashes($value2['curbWeight']);
         $fuel_efficiency=addslashes($value2['fuelCapacity']);
         if($mpgcombined>0 && $mpgcombined!=''){
         $vehicle_combined=$price/$mpgcombined;
         } 
         else{
         $vehicle_combined=''; 
         }
         $littercombined=$vehicle_combined;
         $query3=("SELECT * FROM  eps_data  WHERE
        customer_id =$id");
        $result3 = $this -> db -> query($query3);  
        if($result3 -> num_rows() > 0)
        {
            
        }
        else{
           $insert_master_table=("insert into  eps_data set 
                styleid='$styleId',
                vehicletype='$vehicletype',
                vehiclestyle='$vehiclestyle',
                vehiclesize='$vehiclesize',
                vehiclecategory='$vehiclecategory', 
                enginefueltype='$enginefueltype', 
                drivenwheels='$drivenwheels', 
                transmissiontype='$transmissiontype',
                fuel_efficiency='$fuel_efficiency',
                littercombined='$littercombined',
                customer_id='$id'
        ");
        $insert_master_table1= $this -> db -> query($insert_master_table);  
        if($insert_master_table1){
            echo "insert <br />";
        }
        
        }
}   
}
}
}
}
}

function insertvehicledata(){
$query1=("SELECT sold_vehicle_year,sold_vehicle_make, sold_vehicle_model,id FROM pbs_customer_data  WHERE
dealership_id =204 order by id limit 1000,1000 ");
$result1=$this->db->query($query1);
if($result1 -> num_rows() > 0)
{
$returnvalue1= $result1->result_array();
foreach($returnvalue1 as $value1){
    $id=$value1['id'];
    $year=trim($value1['sold_vehicle_year']);
    $sold_vehicle_make=trim($value1['sold_vehicle_make']);
    $sold_vehicle_model=trim(addslashes($value1['sold_vehicle_model']));
    if($sold_vehicle_make!=''){
    $query2=("SELECT * FROM Vehicle  WHERE
    year =$year  and make='$sold_vehicle_make' and model='$sold_vehicle_model'");
    $result2 = $this -> db -> query($query2);  
    if($result2 -> num_rows() > 0)
    {
        $price='253.214584327527';
        $returnvalue2= $result2->result_array();
        foreach($returnvalue2 as $value2){
         $styleId=$value2['styleId'];  
         $vehicletype=addslashes($value2['vehicleType']); 
         $vehiclestyle=addslashes($value2['vehicleStyle']);
         $vehiclesize=addslashes($value2['vehicleSize']); 
         $vehiclecategory=addslashes($value2['vehicleCategory']); 
         $enginefueltype=addslashes($value2['engineFuelType']) ;
         $drivenwheels=addslashes($value2['drivenWheels']);
         $transmissiontype=addslashes($value2['transmissionType']);
         $numberofdoors=addslashes($value2['numberOfDoors']); 
         $mpgcity=addslashes($value2['mpgCity']); 
         $mpghighway=addslashes($value2['mpgHighway']);
         $mpgcombined=addslashes($value2['mpgCombined']);
         $curbweight=addslashes($value2['curbWeight']);
         $fuel_efficiency=addslashes($value2['fuelCapacity']);
         if($mpgcombined>0 && $mpgcombined!=''){
         $vehicle_combined=$price/$mpgcombined;
         } 
         else{
         $vehicle_combined=''; 
         }
         $littercombined=$vehicle_combined;
         $query3=("SELECT * FROM  pbs_vehicledata_data  WHERE
        customer_id =$id");
        $result3 = $this -> db -> query($query3);  
        if($result3 -> num_rows() > 0)
        {
            
        }
        else{
           $insert_master_table=("insert into  pbs_vehicledata_data set 
                styleId='$styleId',
                vehicletype='$vehicletype',
                vehiclestyle='$vehiclestyle',
                vehiclesize='$vehiclesize',
                vehiclecategory='$vehiclecategory', 
                enginefueltype='$enginefueltype', 
                drivenwheels='$drivenwheels', 
                transmissiontype='$transmissiontype',
                numberofdoors='$numberofdoors', 
                mpgcity='$mpgcity', 
                mpgcombined='$mpgcombined', 
                curbweight='$curbweight', 
                mpghighway='$mpghighway', 
                fuel_efficiency='$fuel_efficiency',
                littercombined='$littercombined',
                customer_id='$id'
        ");
        $insert_master_table1= $this -> db -> query($insert_master_table);  
        if($insert_master_table1){
            echo "insert <br />";
        }
        
        }
}   
}
}
}
}
}

function insert_engin_data(){
    $sql=("SELECT vd.styleId,vd.curbweight from pbs_vehicledata_data as vd,
     pbs_customer_data as cd where vd.customer_id=cd.id and cd.dealership_id=204
     	order by cd.id limit 2000,1000 
    ");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0)
    {
    $returnvalue= $query->result_array();
    foreach($returnvalue as $value){
    $sql1=("SELECT styleId , horsepower,torque ,cylinder ,size 
    FROM Engine
    WHERE styleId = $value[styleId]");
    $query1=$this->db->query($sql1);
    if($query1 -> num_rows() > 0)
    {
    $curbweight= $value['curbweight'];
    $returnvalue1= $query1->result_array();
    foreach($returnvalue1 as $value1){
    $horsepower=$value1['horsepower'];
    if($horsepower!='' && $horsepower>0){
     $powerratio_value=$curbweight/$horsepower;   
    }
    else
    {
     $powerratio_value='';   
    }
    $powerratio=$powerratio_value;
    $data=array(
    'styleId'=>$value1['styleId'],
    'horsepower'=>$value1['horsepower'],
    'torque'=>$value1['torque'],
    'cylinder'=>$value1['cylinder'],
    'powerratio'=>$powerratio,
    'size'=>$value1['size']              
    );
    $insert=$this->db->insert("pbs_engine_data", $data);  
    if($insert){
            echo "insert <br />";
        }
    }
                 
    }
    }
}
}


function get_sql_combined_data(){
$query=("SELECT pbs_vehicledata_data.id as vehicle,
pbs_vehicledata_data.customer_id as customer_id,
pbs_vehicledata_data.vehicletype as vehicletype,
pbs_vehicledata_data.vehiclesize as vehiclesize,
pbs_vehicledata_data.vehiclestyle as vehiclestyle,
pbs_vehicledata_data.vehiclecategory as vehiclecategory,
pbs_vehicledata_data.enginefueltype as enginefueltype,
pbs_vehicledata_data.drivenwheels as drivenwheels,
pbs_vehicledata_data.transmissiontype as transmissiontype,
pbs_vehicledata_data.fuel_efficiency as fuel_efficiency,
pbs_vehicledata_data.littercombined as littercombined,
pbs_vehicledata_data.styleId as styleId
FROM pbs_vehicledata_data ,pbs_customer_data
WHERE pbs_vehicledata_data.customer_id=pbs_customer_data.id
AND pbs_customer_data.dealership_id='202'
");    
}

function update_eps_dates_table()
{
 $sql1=("SELECT id,contract_date,first_payment_date,delivery_date
    FROM  eps_data_copy
    order by id limit 20000,10000");
    $query1=$this->db->query($sql1);
    if($query1 -> num_rows() > 0)
    {
    $returnvalue1= $query1->result_array();
    foreach($returnvalue1 as $value1){
                
                $contract_date=addslashes($value1['contract_date']);
                $first_payment_date=addslashes($value1['first_payment_date']);
                $delivery_date=addslashes($value1['delivery_date']);
                $contract_date_strtime=strtotime($contract_date);
                $first_payment_date_strtime=strtotime($first_payment_date);
                $delivery_date_strtime=strtotime($delivery_date);
                $updatestatus_status="UPDATE eps_data set 
                contract_date='$contract_date_strtime',
                first_payment_date='$first_payment_date_strtime',
                delivery_date='$delivery_date_strtime'
                where  id='$value1[id]'";
                $result = $this -> db -> query($updatestatus_status); 
                if($updatestatus_status)
                {
                    echo "Updated";
                }
                  
        }
                 
        }
}

}
?>