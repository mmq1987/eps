<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Minedata extends CI_Controller 
{
public function __construct() 
    {
    parent::__construct();
    $this -> load -> helper('url');
    $this -> load -> library('session');
    $this -> load -> helper('form');
    $this -> load -> library('form_validation');
    $this->load->model('login_model'); 
    $this->load->model('main_model'); 
    $this->load->model('settings_model'); 
    $this->load->library("pagination");
    $this->load->library('session');
    $this->load->database();
    $this->load->helper('xml');
    $this->load->library('email');
    $this -> load -> library('zip');
   	}
    public function index($dealer_id_pass='')
    {
        $data['menu']=$this->login_model->loginauth();
        $data['title'] = 'Exclusive Private Sale Inc-Campaign';
        if (isset($data['menu']['logged_in']) != '') 
        {
        $dealers_userid=$data['menu']['logged_in']['registration_id'];
        $data['dealerdashboard']=$dealers_userid;
        $data['dealer_id_upload_data']=$dealer_id_pass;
       $event_insert_get=$this->settings_model->insert_event_minedata($dealer_id_pass);
        $this -> session -> set_userdata('event_id_get', $event_insert_get);
         $data['event_insert_id']=$this->session->userdata('event_id_get'); 
        $this->load->view('themes/header',$data);
        if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin' || $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
        $this->load->view('themes/side-bar',$data);
        }else{
        $this->load->view('themes/dealerside-bar',$data); 
        }
        
        $this-> load-> view('minedata-view',$data); 
        $this->load->view('themes/footer',$data); 
        }
        else
        {
        redirect(base_url().'login');
        }
        
    }
 
    
    //customerlist
      public function customerlist($dealer_id_pass='')
    {
        $data['title'] = 'Exclusive Private Sale Inc-Campaign';
        $data['menu']=$this->login_model->loginauth();
        $data['campaine_step']='capaine_step_complete';
        $lead_mining_presets_insert_get=$this->epsadvantage_campaign_fiststep($this->session->userdata('event_id_get'));
        $lead_mining_presets=$this -> main_model -> get_lead_mining_presets($this->session->userdata('event_id_get'));
        $data['event_insert_id']=$this->session->userdata('event_id_get');
        $data['event_insert_id_minedata']=$this->session->userdata('event_id_get');
        if (isset($data['menu']['logged_in']) != '') 
        { 
        $dealers_userid=$data['menu']['logged_in']['registration_id'];
        $data['dealerdashboard']=$dealers_userid;
         $data['dealer_id_upload_data']=$dealer_id_pass;
         $data['member_type']=$this->input->post('member_type'); 
        $this->load->view('themes/header',$data);
         if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin'|| $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
         $this->load->view('themes/side-bar',$data);
         }else{
         $this->load->view('themes/dealerside-bar',$data);
         }
         if($lead_mining_presets!='custom_campaign'){
         $this-> load-> view('minedata-customer-list',$data);
         }
         else{
         
          $this-> load-> view('minedata-advanced-option-report-list',$data);  
         }
         $this->load->view('themes/footer',$data);    
               
        }
         else
         {
         redirect(base_url().'login');
         }
    }
    //mailout
public function mailoutoption($dealer_id_pass=''){
    $data['title'] = 'Exclusive Private Sale Inc-Campaign';
    $data['menu']=$this->login_model->loginauth();
    $data['lead_step']='leadlist_step_complete';
    $data['campaine_step']='capaine_step_complete';
    $equity_scrap=$this->input->post('equity_scrap');
    $model_break_down=$this->input->post('model_break_down');
    $fuel_effciency=$this->input->post('fuel_effciency');
    $wrranty_scrap=$this->input->post('wrranty_scrap');
    $custom_campain=$this->input->post('custom_campain');
    $fuelreopr6=$this->input->post('fuelreopr6');
    $lead_mining_presets=$this->input->post('lead_mining_presets');
    $equity_scrap_customer_id=$this->input->post('checkedpr1');
    $modebreakdown_customer_id=$this->input->post('checkedprmodebreakdown');
    $fuelefficiency_customer_id=$this->input->post('checkedpr_fuelefficiency');
    $warrantyscrape_customer_id=$this->input->post('checkedprwarrantyscrape');
    $customcampaign_customer_id=$this->input->post('checkedpcustomcampaign');
    $fuelreopr6_customer_id=$this->input->post('checkedfuelgroup');
    if (isset($data['menu']['logged_in']) != '') { 
    $data['event_insert_id']=$this->session->userdata('event_id_get');
    $data['dealerdashboard']=$dealer_id_pass;
    $data['dealer_id_upload_data']=$dealer_id_pass;
     $mailout_selection=$this -> settings_model -> mailout_option_select($this->session->userdata('event_id_get')); 
    
    $this->load->view('themes/header',$data);
    if($equity_scrap!='' || $model_break_down!='' || $fuel_effciency!='' || $wrranty_scrap!='' || $custom_campain!='')
    {  
     $lead_delete_cusomer_data=$this -> settings_model -> leadlistdelete($this->session->userdata('lead_list_id'));   
    }
    
    if($equity_scrap!=''){
       if(!empty($equity_scrap_customer_id)){
       foreach ($equity_scrap_customer_id as $value_equity_scrap){
        $lead_cusomer_data_section=$this -> settings_model -> lead_customer_data_insert($this->session->userdata('lead_list_id'),$equity_scrap,$value_equity_scrap);  
        }
        }
    }

    if($model_break_down!=''){
        if(!empty($modebreakdown_customer_id)){
        foreach ($modebreakdown_customer_id as $value_modebreakdown){
        $lead_cusomer_data_section=$this -> settings_model -> lead_customer_data_insert($this->session->userdata('lead_list_id'),$model_break_down,$value_modebreakdown);  
        }
        }  
    }
     if($fuel_effciency!=''){
        if(!empty($fuelefficiency_customer_id)){
        foreach ($fuelefficiency_customer_id as $value_fuelefficiency){
        $lead_cusomer_data_section=$this -> settings_model -> lead_customer_data_insert($this->session->userdata('lead_list_id'),$fuel_effciency,$value_fuelefficiency);  
        }
        } 
        } 
     if($wrranty_scrap!=''){
         if(!empty($warrantyscrape_customer_id)){
        foreach ($warrantyscrape_customer_id as $value_wrranty_scrap){
        $lead_cusomer_data_section=$this -> settings_model -> lead_customer_data_insert($this->session->userdata('lead_list_id'),$wrranty_scrap,$value_wrranty_scrap);  
        }
        } 
        }
     if($custom_campain!=''){
     if(!empty($customcampaign_customer_id)){
       foreach ($customcampaign_customer_id as $value_customcampaign){
        $lead_cusomer_data_section=$this -> settings_model -> lead_customer_data_insert($this->session->userdata('lead_list_id'),$custom_campain,$value_customcampaign);  
        }
        }
    }
      if($fuelreopr6!=''){
     if(!empty($fuelreopr6_customer_id)){
       foreach ($fuelreopr6_customer_id as $value_fuelreopr6){
        $lead_cusomer_data_section=$this -> settings_model -> lead_customer_data_insert($this->session->userdata('lead_list_id'),$fuelreopr6,$value_fuelreopr6);  
        }
        }
    }
    //create folder and create csv file
    $companyname_show='';
    $get_dealer_company_name=$this -> main_model -> dealercompanynameget($dealer_id_pass);
    if(isset($get_dealer_company_name) && $get_dealer_company_name!=''){
     foreach($get_dealer_company_name as $value_dealer_company_name){
      $companyname_get=substr(trim($value_dealer_company_name['company_name']),0,10); 
      $companyname_show=strtolower(str_replace(" ","-",$companyname_get));
     }   
    }
    
    $report_download_time=date('m.d.y',time());
    $foldername=($companyname_show.'-eventreport-'.$report_download_time.'-'.$this->session->userdata('event_id_get'));
     $base_path = $this -> config -> item('rootpath');
     $targetPath=$base_path.'/downloadreportzip/'.$foldername.'/';
     $file_path=$base_path.'/downloadreportzip/'.$foldername.'/';
     if(is_dir($targetPath))
     {
         $dir = opendir($base_path.'/downloadreportzip/'.$foldername);
       while (($file = readdir($dir)) !== false){
        
        if ($file != "."  && $file != "..")
            {
               
               unlink($base_path.'/downloadreportzip/'.$foldername.'/'.$file); 
            }
        }
        
     }else{
     mkdir($file_path, 0755);   
     }
    
    //create folder and create csv file
    if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin'|| $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
    $this->load->view('themes/side-bar',$data);
    }else{
        $this->load->view('themes/dealerside-bar',$data);
    }
    
    $lead_mining_presets_select='';
   $lead_mining_presets_select=$this -> main_model -> get_lead_mining_presets($this->session->userdata('event_id_get'));
   $get_selected_lead_group=$this->settings_model->leadsection_select($this->session->userdata('event_id_get'));
   foreach($get_selected_lead_group as $values_mindata_group){
    if($lead_mining_presets_select=='custom_campaign'){
    $group_1_details=$this -> settings_model -> getgroupname_advanced_option($this->session->userdata('event_id_get'),1);
   if($values_mindata_group['equity_scrap']!='0'){
   $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'1');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_1_details,1); 
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_1_details,1);
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_1_details,1);
    }
    }
    $group_2_details=$this -> settings_model -> getgroupname_advanced_option($this->session->userdata('event_id_get'),2);
    if($values_mindata_group['model_break_down']!='0'){
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'2');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_2_details,2); 
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_2_details,2); 
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_2_details,2);
    }
    }
    $group_3_details=$this -> settings_model -> getgroupname_advanced_option($this->session->userdata('event_id_get'),3);
    if($values_mindata_group['fuel_effciency']!='0'){
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'3');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_3_details,3);
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_3_details,3);  
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_3_details,3);
    }
    }
    $group_4_details=$this -> settings_model -> getgroupname_advanced_option($this->session->userdata('event_id_get'),4);
    if($values_mindata_group['wrranty_scrap']!='0'){
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'4');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_4_details,4); 
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_4_details,4); 
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_4_details,4);
    }
    }
    $group_5_details=$this -> settings_model -> getgroupname_advanced_option($this->session->userdata('event_id_get'),5);
   if($values_mindata_group['custom_campain']!='0'){
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'5');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_5_details,5); 
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_5_details,5); 
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_5_details,5);
    }
    }
    }
    else{
   
    $group_name=$this -> settings_model -> getleadgrouptitle_report($lead_mining_presets_select);
    if(isset($group_name)&& $group_name!=''){
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'1');
    if($get_selected_lead_group!=0){
    $firstgroup=strtolower(str_replace(" ","-",$group_name[0]));
     $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$firstgroup,1); 
     $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$firstgroup,1);
     $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$firstgroup,1);
     
    }
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'2');
    if($get_selected_lead_group!=0){
     $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[1],2);
     $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[1],2);
     $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[1],2);
     
    }
    
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'3');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[2],3);
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[2],3);
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[2],3);
    
    }

    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'4');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[3],4);
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[3],4);
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[3],4);
    
    }
    
  
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'5');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[4],5);
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[4],5);
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[4],5);
    
    }
    $get_selected_lead_group=$this->settings_model->getleadcount($this->session->userdata('event_id_get'),'6');
    if($get_selected_lead_group!=0){
    $this->create_csv($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[5],6);
    $this->create_txt($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[5],6);
    $this->create_xml($this->session->userdata('event_id_get'),$foldername,$dealer_id_pass,$group_name[5],6);
    }
    }
    }
    $this->zip->archive($base_path.'/downloadreportzip/'.$foldername.'/'.$foldername.'.zip'); 
  //$path = $base_path.'/downloadreportzip/my_backup.zip';
    $this->zip->get_files_from_folder($base_path.'/downloadreportzip/'.$foldername.'/',$foldername.'.zip/',$foldername);
    $this->zip->download($foldername.'.zip');
    }
    }
    else
    {
    redirect(base_url().'login');
    }
    }
    function insertcustomer_lead_list()
    {
        $equity_scrap=$this->input->post('equity_scrap');
        $model_break_down=$this->input->post('model_break_down');
        $fuel_effciency=$this->input->post('fuel_effciency');
        $wrraenty_scrap=$this->input->post('wrranty_scrap');
        $custom_campain=$this->input->post('custom_campain');
        $fuel_report6=$this->input->post('fuel_report6');
        $data['event_insert_id']=$this->session->userdata('event_id_get');
        if($equity_scrap!='' || $model_break_down!='' || $fuel_effciency!='' || $wrraenty_scrap!='' || $custom_campain!='' || $fuel_report6!='')
        {
         $leadlist_insert_id=$this->settings_model->insertleadlistselction($this->session->userdata('event_id_get')); 
          $this -> session -> set_userdata('lead_list_id', $leadlist_insert_id);            
            $data['lead_list_id']=$this->session->userdata('lead_list_id');
         echo "Done";  
         
        }
    }
    function checkadvanceinsert(){
     $event_id=$this->input->post('event_id');  
     $sql=("Select * from  advance_options_group_selection where event_id=$event_id");
    $query=$this->db->query($sql);
    if($query -> num_rows() > 0){ 
    echo "Done";
   }
   } 
   
    function epsadvantage_campaign_fiststep()
    {
        $data['menu']=$this->login_model->loginauth();
        $campaine_insert_id=$this->settings_model->insert_campaign_step1_mine_data($this->session->userdata('event_id_get'));
        return $campaine_insert_id;
         
        
    }
    function epsadvantage_campaign_advanced_option()
    {
          $data['menu']=$this->login_model->loginauth();
          $event_id=$this->input->post('event_insert_id');
          $get_campign_id=$this->settings_model->get_campign_id($event_id);
          if($get_campign_id!=0){
          $campine_advanced_option_updates=$this->settings_model->update_campaign_step1($get_campign_id);
         echo 'Done';
         }
    }
    //edit events
   function editcampign($event_id,$dealer_id){
   $data['menu']=$this->login_model->loginauth();
   $this->session->unset_userdata('event_id_get');
   $this->session->unset_userdata('incompete_event_set');
   $dealers_userid=$dealer_id;
   $data['dealerdashboard']=$dealer_id;
   $data['dealer_id_upload_data']=$dealer_id;
  if (isset($data['menu']['logged_in']) != ''){
       if($event_id!=''){ 
           $incomple=1;
           $this -> session -> set_userdata('incompete_event_set', $incomple);
           $data['incompete_events']=$this->session->userdata('incompete_event_set');
           $this -> session -> set_userdata('event_id_get', $event_id);
        $data['event_insert_id']=$this->session->userdata('event_id_get');
       }
       $data['campign_status']='edit';
         $lead_selection=$this -> settings_model -> leadsection_select($this->session->userdata('event_id_get')); 
        if(!empty($lead_selection)){
            $incomple=1;
            $this -> session -> set_userdata('leadlist', $incomple);
            $data['leadlist']=$this->session->userdata('leadlist');
        }
         $mailout_selection=$this -> settings_model -> mailout_option_select($this->session->userdata('event_id_get')); 
         if(!empty($mailout_selection)){
            $incomple=1;
            $this -> session -> set_userdata('mailout', $incomple);
            $data['mailout']=$this->session->userdata('mailout');
        }
       $this->load->view('themes/header',$data);
       if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin'|| $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
       $this->load->view('themes/side-bar',$data);
       }else{
            $this->load->view('themes/dealerside-bar',$data);
        }
        $this->load->view('campaignpage-sidebar-view',$data);
        $this->load->view('newcampign-view',$data);
        $this->load->view('themes/footer',$data);  
      
    }
     else
    {
    redirect(base_url().'login');
    }
   }
       //function to go to step 1
    public function linkto_step1($event_id,$dealer_id,$getstep){
        if($event_id!=0){
        $data['menu']=$this->login_model->loginauth();
        $data['title'] = 'Exclusive Private Sale Inc-Campaign';
        if (isset($data['menu']['logged_in']) != '') 
            {
            $dealers_userid=$data['menu']['logged_in']['registration_id'];
            $data['dealerdashboard']=$dealers_userid;
            $data['dealer_id_upload_data']=$dealer_id;
            $this -> session -> set_userdata('event_id_get', $event_id);
            $this->session->userdata('event_id_get');           
            $data['event_insert_id']=$event_id;
            $incomple=1;
            $this -> session -> set_userdata('incompete_event_set', $incomple);
            $data['incompete_events']=$this->session->userdata('incompete_event_set');
            $data['campign_status']='edit';
            $mailout_selection=$this -> settings_model -> mailout_option_select($this->session->userdata('event_id_get')); 
            $get_campign_id=$this->settings_model->get_campign_id($event_id);
            $this -> session -> set_userdata('campain_insert_id', $get_campign_id);            
         $data['campaine_insert_get']=$this->session->userdata('campain_insert_id');
                if(!empty($mailout_selection)){
                $incomple=1;
                $this -> session -> set_userdata('mailout', $incomple);
                $data['mailout']=$this->session->userdata('mailout');
                }
            $lead_selection=$this -> settings_model -> leadsection_select($this->session->userdata('event_id_get')); 
                if($lead_selection!=0){
                $incomple=1;
                $this -> session -> set_userdata('leadlist', $incomple);
                $data['leadlist']=$this->session->userdata('leadlist');
                }         
            $data['editted_step']=$getstep;      
            $this->load->view('themes/header',$data);
                if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin' || $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
                    $this->load->view('themes/side-bar',$data);
                }else{
                    $this->load->view('themes/dealerside-bar',$data); 
                }
            $this->load->view('campaignpage-sidebar-view',$data);
            $this-> load-> view('campaign-view',$data); 
            
            }
        }else{
                        
            $this->index($dealer_id);             
            
        }
}    
    public function sale_leadlist($event_id,$dealer_id){
        
    $data['title'] = 'Exclusive Private Sale Inc-Campaign';
    $data['menu']=$this->login_model->loginauth();
    
    $data['campaine_step']='capaine_step_complete';
        if (isset($data['menu']['logged_in']) != '') 
        { 
          $this -> session -> set_userdata('event_insert_id', $event_id);
            $data['event_insert_id']=$this->session->userdata('event_insert_id');
             $lead_selection=$this -> settings_model -> leadsection_select($event_id); 
            if($lead_selection!=0 && $event_id!=0){
            $incomple=1;
            $this -> session -> set_userdata('leadlist', $incomple);
            $data['leadlist']=$this->session->userdata('leadlist');
            
            $dealers_userid=$data['menu']['logged_in']['registration_id'];
          
            $data['dealerdashboard']=$dealers_userid;
            $data['dealer_id_upload_data']=$dealer_id;
            $data['editted_step']='lead_step1';
            $mailout_selection=$this -> settings_model -> mailout_option_select($event_id); 
                if(!empty($mailout_selection)){
                    $incomple=1;
                    $this -> session -> set_userdata('mailout', $incomple);
                    $data['mailout']=$this->session->userdata('mailout');
                }
            $this->load->view('themes/header',$data);
                if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin'|| $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
                    $this->load->view('themes/side-bar',$data);
                }else{
                    $this->load->view('themes/dealerside-bar',$data);
                }
            $this-> load-> view('campaignpage-sidebar-view',$data);
            $this-> load-> view('target-customer-list',$data);
            $this->load->view('themes/footer',$data);   
            
       
        }else{
             $this->index($dealer_id);    
            } 
            }
    }
    public function linkto_maileroption($event_id,$dealer_id,$step_no){
        
            $data['title'] = 'Exclusive Private Sale Inc-Campaign';
            $data['menu']=$this->login_model->loginauth();
            $data['lead_step']='leadlist_step_complete';
            $data['campaine_step']='capaine_step_complete';
            $this -> session -> set_userdata('event_insert_id', $event_id);
            $data['event_insert_id']=$this->session->userdata('event_insert_id');
            if (isset($data['menu']['logged_in']) != '') { 
                $mailout_selection=$this -> settings_model -> mailout_option_select($event_id); 
                if($mailout_selection!=0 && $event_id!=0){
                
                $data['dealerdashboard']=$dealer_id;
                $data['dealer_id_upload_data']=$dealer_id;
                $data['editted_step']=$step_no;
                
                 if(!empty($mailout_selection)){
                        $incomple=1;
                        $this -> session -> set_userdata('mailout', $incomple);
                        $data['mailout']=$this->session->userdata('mailout');
                    }
                    
                $this->load->view('themes/header',$data);
                    if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin'|| $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
                    $this->load->view('themes/side-bar',$data);
                    }else{
                        $this->load->view('themes/dealerside-bar',$data);
                    }
                $this-> load-> view('campaignpage-sidebar-view',$data);
                $this-> load-> view('mailout-options-view',$data);
                $this->load->view('themes/footer',$data);  
                }else{
              $this->index($dealer_id);  
        }
        }else
        {
            redirect(base_url().'login');
        }
        
    }
    public function advertising_option($event_id,$dealer_id_upload_data){
        if($event_id!=0){
        $data['title'] = 'Exclusive Private Sale Inc-Campaign';
        $data['menu']=$this->login_model->loginauth();
        $this->session->unset_userdata('event_id_get');
        $this->session->unset_userdata('incompete_event_set');
        
        if (isset($data['menu']['logged_in']) != '') 
        { 
        $dealers_userid=$data['menu']['logged_in']['registration_id'];
        $data['dealerdashboard']=$dealers_userid;
        $data['dealer_id_upload_data']=$dealer_id_upload_data;
        $data['member_type']=$this->input->post('member_type'); 
        $this->load->view('themes/header',$data);
        if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin' || $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
        $this->load->view('themes/side-bar',$data);
        }else{
        $this->load->view('themes/dealerside-bar',$data); 
        }
        $this->editcampign($event_id,$dealer_id_upload_data);
        //check incompete events for the dealer
        
        }  
        }else{
            $this->index($dealer_id_upload_data);
        }
    }
        //view customer lead list
        public function viewlist($event_id,$dealer_id_upload_data){
        $data['title'] = 'Exclusive Private Sale Inc-viewlist';
        $data['menu']=$this->login_model->loginauth();
        if (isset($data['menu']['logged_in']) != '') {
        $dealers_userid=$data['menu']['logged_in']['registration_id'];
        $data['dealerdashboard']=$dealer_id_upload_data;
        $data['dealer_id_upload_data']=$dealer_id_upload_data;
        $data['event_insert_id']=$event_id;
        $data['event_id']=$event_id;
        $this->load->view('themes/header',$data);
        if($data['menu']['logged_in']['usertype']=='admin' || $data['menu']['logged_in']['usertype']=='sub_admin' || $data['menu']['logged_in']['usertype']=='account_managers' || $data['menu']['logged_in']['usertype']=='auto_brand'){
        $this->load->view('themes/side-bar',$data);
        }else{
        $this->load->view('themes/dealerside-bar',$data); 
        }
        $this-> load-> view('view-customer-lead-list',$data);
        //check incompete events for the dealer
       }
        else{
        redirect(base_url().'login');   
        }  
        }
        //get pbs customer details
        public function get_customer_details($customer_id,$foldername){
        $data['get_customerdetails']=$this -> main_model -> customerdatafulldetails($customer_id);
        $this->load->view('customer-details-view',$data);
    }
        public function get_customer_details_with_customer_id($customer_id){
        $data['get_customerdetails']=$this -> main_model -> customerdatafulldetails($customer_id);
        $this->load->view('customer-details-view',$data);
    }

      //create csv file
    function create_csv($event_id,$foldername,$dealer_id,$reportname,$report_id){
    $base_path = $this -> config -> item('rootpath');
    $companyname_show='';
    $get_dealer_company_name=$this -> main_model -> dealercompanynameget($dealer_id);
    if(isset($get_dealer_company_name) && $get_dealer_company_name!=''){
    foreach($get_dealer_company_name as $value_dealer_company_name){
      $companyname_get=substr(trim($value_dealer_company_name['company_name']),0,10); 
      $companyname_show=strtolower(str_replace(" ","-",$companyname_get));
        
    }   
    }
    $lead_mining_presets_select='';
    $reoprt_name_show='';
    $lead_mining_presets_select=$this -> main_model -> get_lead_mining_presets($event_id);
    if($lead_mining_presets_select=='custom_campaign'){
    if(isset($reportname) && $reportname!=''){
    foreach($reportname as $value_report_name){
    $reoprt_name_show_get=$this -> settings_model -> getreporttype_report_generation_type($value_report_name['report_type']); 
    $reoprt_name_show='Report'.$report_id.'-'.$reoprt_name_show_get;
    }    
    }
    }
    else{
    $reoprt_name_show=$reportname;     
    }
    $report_download_time=date('m.d.y',time());
    $csvfilename=($companyname_show.'-'.trim($reoprt_name_show).'-'.$report_download_time);
    $query = $this->db->query("SELECT buyer_first_name,buyer_last_name,buyer_address,buyer_appartment,buyer_city,buyer_province,buyer_postalcode,buyer_homephone,buyer_businessphone,sold_vehicle_year,sold_vehicle_make,sold_vehicle_model FROM eps_data where dealership_id=$dealer_id");
    $num = $query->num_fields();
    $var =array();
    $i=0;
    $fname="";
    $feild_name=array("dealership_id","buyer_first_name","buyer_last_name","buyer_address","buyer_appartment","buyer_city","buyer_province","buyer_postalcode","buyer_homephone","buyer_businessphone","sold_vehicle_year","sold_vehicle_make","sold_vehicle_model");
    while($i <$num){
        $test = $i;
        $value =$feild_name[$i];
        if($value != ''){
            $fname= $fname." ".$value;
            array_push($var, $value);
        }
        $i++;
    }
    $fname = trim($fname);
    $fname=str_replace(' ', ',', $fname);
    $this->db->select($fname);
    $sql_leadlist=("SELECT cd.dealership_id,
    cd.buyer_first_name,
    cd.buyer_last_name,
    cd.buyer_address, 
    cd.buyer_appartment, 
    cd.buyer_city, 
    cd.buyer_province, 
    cd.buyer_postalcode,
    cd.buyer_homephone, 
    cd.buyer_businessphone,
    cd.sold_vehicle_year,
    cd.sold_vehicle_make, 
    cd.sold_vehicle_model,
    cd.sold_vehicle_stock
    FROM eps_data cd, leadlist_customer_data lc, select_customer_leadlist sl
    WHERE lc.lead_customer_id = cd.id AND 
    lc.lead_type = $report_id AND 
    cd.dealership_id  = $dealer_id AND 
    sl.customer_leadlist_id=lc.customer_leadlist_id 	
    AND sl.event_id =$event_id order by lc.lead_customer_id asc");
    $quer=$this->db->query($sql_leadlist);
    $numrow=$quer -> num_rows();
  if(file_exists($base_path.'downloadreportzip/'.$foldername.'/'.$csvfilename.'.csv')){
    $csvfilename=$csvfilename.'-'.$numrow;
  }
  else{
    $csvfilename=$csvfilename;
  }

$this -> query_to_csv($quer,TRUE,'downloadreportzip/'.$foldername.'/'.$csvfilename.'.csv',$dealer_id);

 }
     //create and save textfile
    function create_txt($event_id,$foldername,$dealer_id,$reportname,$report_id){  
    $companyname_show='';
    $purchase_date_get='';
    $get_dealer_company_name=$this -> main_model -> dealercompanynameget($dealer_id);
    if(isset($get_dealer_company_name) && $get_dealer_company_name!=''){
     foreach($get_dealer_company_name as $value_dealer_company_name){
      $companyname_get=substr(trim($value_dealer_company_name['company_name']),0,10); 
      $companyname_show=strtolower(str_replace(" ","-",$companyname_get));
     }   
    }
    $lead_mining_presets_select='';
    $reoprt_name_show='';
    $lead_mining_presets_select=$this -> main_model -> get_lead_mining_presets($event_id);
    if($lead_mining_presets_select=='custom_campaign'){
    if(isset($reportname) && $reportname!=''){
    foreach($reportname as $value_report_name){
    $reoprt_name_show_get=$this -> settings_model -> getreporttype_report_generation_type($value_report_name['report_type']); 
    $reoprt_name_show='Report'.$report_id.'-'.$reoprt_name_show_get;
    }    
    }
    }
    else{
    $reoprt_name_show=$reportname;     
    }
    $report_download_time=date('m.d.y',time());
    $zipname=($companyname_show.'-'.trim($reoprt_name_show).'-'.$report_download_time);   
    $dealership_name='';
    
    $content = 'Dealership Name    FirstName	LastName    Address	Apartment	City	State	Zip	 HomePhone	BusinessPhone	 Year	Make  Model   Purchase Date Trade In Value';
    $sql_leadlist=("SELECT cd.dealership_id,
    cd.buyer_first_name,
    cd.buyer_last_name,
    cd.buyer_address, 
    cd.buyer_appartment, 
    cd.buyer_city, 
    cd.buyer_province, 
    cd.buyer_postalcode,
    cd.buyer_homephone, 
    cd.buyer_businessphone,
    cd.sold_vehicle_year,
    cd.sold_vehicle_make, 
    cd.sold_vehicle_model,
    cd.sold_vehicle_stock
    FROM eps_data cd, leadlist_customer_data lc, select_customer_leadlist sl
    WHERE lc.lead_customer_id = cd.id AND 
    lc.lead_type = $report_id AND 
    cd.dealership_id  = $dealer_id AND 
    sl.customer_leadlist_id=lc.customer_leadlist_id 	
    AND sl.event_id =$event_id order by lc.lead_customer_id asc");
    $query_leadlist=$this->db->query($sql_leadlist);
    $content1='';
    $numrow=$query_leadlist -> num_rows();
    if($query_leadlist -> num_rows() > 0){
        $returnvalue= $query_leadlist->result_array();
        
        foreach($returnvalue as $values_customer_data){
        
             $sql=("SELECT  company_name 	
            FROM  registration
            WHERE registration_id ='$values_customer_data[dealership_id]' 
            ");
            
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
            $returnvalue_delership_name= $query->result_array();
            foreach($returnvalue_delership_name as $values_dealership){
             $dealership_name=trim(ucfirst($values_dealership['company_name'])).' (#'.$values_customer_data['dealership_id'].')';    
            }
            }   
            $sql_puchasedate=("select contract_date from  pbs_financial_data where vehicle_stock='$values_customer_data[sold_vehicle_stock]'");
            $query_puchasedate = $this->db->query($sql_puchasedate);
            $purchasedate= $query_puchasedate->result_array();
            foreach($purchasedate as $purchase_date_display){ 
                if($purchase_date_display['contract_date']!=''){
                $purchase_date_get=$purchase_date_display['contract_date'];
            }
            else{
                $purchase_date_get='N/A';
            }
            }
                $content1.="$dealership_name\t$values_customer_data[buyer_first_name]\t$values_customer_data[buyer_last_name]\t$values_customer_data[buyer_address]$values_customer_data[buyer_appartment]\t$values_customer_data[buyer_city]\t$values_customer_data[buyer_province]\t$values_customer_data[buyer_postalcode]\t$values_customer_data[buyer_homephone]\t$values_customer_data[buyer_businessphone]\t$values_customer_data[sold_vehicle_year]\t$values_customer_data[sold_vehicle_make]\t$values_customer_data[sold_vehicle_model]\t$purchase_date_get\t \n";   
            }
            
        
    }
    $textfilecontent="$content\n$content1";
    $base_path = $this -> config -> item('rootpath');
    if(file_exists($base_path.'downloadreportzip/'.$foldername.'/'.$zipname.'.txt')){
    $zipname=$zipname.'-'.$numrow;
  }
  else{
    $zipname=$zipname;
  }
    $handle = fopen($base_path.'downloadreportzip/'.$foldername.'/'.$zipname.'.txt', 'w');
    fwrite($handle, $textfilecontent);
    fclose($handle);
    
    }
     function create_xml($event_id,$foldername,$dealer_id,$reportname,$report_id) {
    $companyname_show='';
    $purchase_date_get='';
    $base_path = $this -> config -> item('rootpath');
    $get_dealer_company_name=$this -> main_model -> dealercompanynameget($dealer_id);
    if(isset($get_dealer_company_name) && $get_dealer_company_name!=''){
     foreach($get_dealer_company_name as $value_dealer_company_name){
      $companyname_get=substr(trim($value_dealer_company_name['company_name']),0,10); 
      $companyname_show=strtolower(str_replace(" ","-",$companyname_get));
     }   
    }
    $lead_mining_presets_select='';
    $reoprt_name_show='';
    $lead_mining_presets_select=$this -> main_model -> get_lead_mining_presets($event_id);
    if($lead_mining_presets_select=='custom_campaign'){
    if(isset($reportname) && $reportname!=''){
    foreach($reportname as $value_report_name){
    $reoprt_name_show_get=$this -> settings_model -> getreporttype_report_generation_type($value_report_name['report_type']);
    $reoprt_name_show='Report'.$report_id.'-'.$reoprt_name_show_get;
    }    
    }
    }
    else{
    $reoprt_name_show=$reportname;     
    }
    $report_download_time=date('m.d.y',time());
    $zipname=($companyname_show.'-'.trim($reoprt_name_show).'-'.$report_download_time); 
    $xml = new DOMDocument("1.0");
	$root = $xml->createElement("data");
    $dealership_name='';
    $leadlist_details_get=$this->settings_model->get_leadlist_details_with_event_id($event_id);
    if($leadlist_details_get!='')
    {
    $i=1;
    $xml->appendChild($root);
       $sql_leadlist=("SELECT cd.dealership_id,
    cd.buyer_first_name,
    cd.buyer_last_name,
    cd.buyer_address, 
    cd.buyer_appartment, 
    cd.buyer_city, 
    cd.buyer_province, 
    cd.buyer_postalcode,
    cd.buyer_homephone, 
    cd.buyer_businessphone,
    cd.sold_vehicle_year,
    cd.sold_vehicle_make, 
    cd.sold_vehicle_model,
    cd.sold_vehicle_stock
    FROM eps_data cd, leadlist_customer_data lc, select_customer_leadlist sl
    WHERE lc.lead_customer_id = cd.id AND 
    lc.lead_type = $report_id AND 
    cd.dealership_id  = $dealer_id AND 
    sl.customer_leadlist_id=lc.customer_leadlist_id 	
    AND sl.event_id =$event_id order by lc.lead_customer_id asc");
    $query_leadlist=$this->db->query($sql_leadlist);
    $content1='';
    $numrow=$query_leadlist -> num_rows();
    if($query_leadlist -> num_rows() > 0){
    $returnvalue= $query_leadlist->result_array();
    if(file_exists($base_path.'downloadreportzip/'.$foldername.'/'.$zipname.'.xml')){
    $zipname=$zipname.'-'.$numrow;
  }
  else{
    $zipname=$zipname;
  }
    foreach($returnvalue as $values){

            $sql=("SELECT  company_name 	
            FROM  registration
            WHERE registration_id ='$values[dealership_id]' 
            ");
            
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
            $returnvalue_delership_name= $query->result_array();
            foreach($returnvalue_delership_name as $values_dealership){
             $dealership_name=trim(ucfirst($values_dealership['company_name'])).' (#'.$values['dealership_id'].')';  ;   
            }
            }   
        $sql_puchasedate=("select contract_date from  pbs_financial_data where vehicle_stock='$values[sold_vehicle_stock]'");
        $query_puchasedate = $this->db->query($sql_puchasedate);
        $purchasedate= $query_puchasedate->result_array();
        foreach($purchasedate as $purchase_date_display){
            if($purchase_date_display['contract_date']!=''){
                $purchase_date_get=$purchase_date_display['contract_date'];
            }
            else{
                $purchase_date_get='N/A';
            }
        }
        $id_dealership_name   = $xml->createElement("DealershipName");
    	$id_dealership_nameText = $xml->createTextNode($dealership_name);
    	$id_dealership_name ->appendChild($id_dealership_nameText);
        //-----------------------------------------------//
        	$id   = $xml->createElement("FirstName");
    	$idText = $xml->createTextNode($values['buyer_first_name']);
    	$id->appendChild($idText);
        //--------------------------------//
    	$title   = $xml->createElement("LastName");
    	$titleText = $xml->createTextNode($values['buyer_last_name']);
    	$title->appendChild($titleText);
        //----------------------------------//
        $buyer_address_title   = $xml->createElement("Address");
    	$buyer_address_titleText = $xml->createTextNode($values['buyer_address']);
    	$buyer_address_title->appendChild($buyer_address_titleText);
         //----------------------------------//
        $buyer_appartment_title   = $xml->createElement("Apartment");
    	$buyer_appartment_titleText = $xml->createTextNode($values['buyer_appartment']);
    	$buyer_appartment_title->appendChild($buyer_appartment_titleText);
         //----------------------------------//
         $buyer_city_title   = $xml->createElement("City");
    	$buyer_city_titleText = $xml->createTextNode($values['buyer_city']);
    	$buyer_city_title->appendChild($buyer_city_titleText);
         //----------------------------------//
        $buyer_province_title   = $xml->createElement("State");
    	$buyer_province_titleText = $xml->createTextNode($values['buyer_province']);
    	$buyer_province_title->appendChild($buyer_province_titleText);
         //----------------------------------//
          $buyer_postalcode_title   = $xml->createElement("Zip");
    	$buyer_postalcode_titleText = $xml->createTextNode($values['buyer_postalcode']);
    	$buyer_postalcode_title->appendChild($buyer_postalcode_titleText);
         //----------------------------------//
          $buyer_homephone_title   = $xml->createElement("HomePhone");
    	$buyer_homephone_titleText = $xml->createTextNode($values['buyer_homephone']);
    	$buyer_homephone_title->appendChild($buyer_homephone_titleText);
         //----------------------------------//
         $buyer_businessphone_title   = $xml->createElement("WorkPhone");
    	$buyer_businessphone_titleText = $xml->createTextNode($values['buyer_businessphone']);
    	$buyer_businessphone_title->appendChild($buyer_businessphone_titleText);
         //----------------------------------//
       
        $sold_vehicle_year_title   = $xml->createElement("Year");
    	$sold_vehicle_year_titleText = $xml->createTextNode($values['sold_vehicle_year']);
    	$sold_vehicle_year_title->appendChild($sold_vehicle_year_titleText);
         //----------------------------------//
          $sold_vehicle_make_title   = $xml->createElement("Make");
    	$sold_vehicle_make_titleText = $xml->createTextNode($values['sold_vehicle_make']);
    	$sold_vehicle_make_title->appendChild($sold_vehicle_make_titleText);
         //----------------------------------//
          $sold_vehicle_model_title   = $xml->createElement("Model");
    	$sold_vehicle_model_titleText = $xml->createTextNode($values['sold_vehicle_model']);
    	$sold_vehicle_model_title->appendChild($sold_vehicle_model_titleText);
        
         //----------------------------------//
         $buyer_cellphone_title   = $xml->createElement("PurchaseDate");
    	$buyer_cellphone_titleText = $xml->createTextNode($purchase_date_get);
    	$buyer_cellphone_title->appendChild($buyer_cellphone_titleText);
         //----------------------------------//
    	$book = $xml->createElement('Leadlist'.$i);
        $book->appendChild($id_dealership_name);
    	$book->appendChild($id);
    	$book->appendChild($title);
        $book->appendChild($buyer_address_title);
        $book->appendChild($buyer_appartment_title);
        $book->appendChild($buyer_city_title);
        $book->appendChild($buyer_province_title);
        $book->appendChild($buyer_postalcode_title);
        $book->appendChild($buyer_homephone_title);
        $book->appendChild($buyer_businessphone_title);
        $book->appendChild($sold_vehicle_year_title);
        $book->appendChild($sold_vehicle_make_title);
        $book->appendChild($sold_vehicle_model_title);
        $book->appendChild($buyer_cellphone_title);
        $i++;
        $root->appendChild($book);
    
	$xml->formatOutput = true;
	//echo "<xmp>". $xml->saveXML() ."</xmp>";
       
 
	$xml->save($base_path.'downloadreportzip/'.$foldername.'/'.$zipname.'.xml') or die("Error");
    
	} 
    }
 }
} 
 function array_to_csv($array, $download = ""){
    //if ($download != "")
    //{    
    // header('Content-Type: application/csv');
    //header('Content-Disposition: attachement; filename="' . $download . '"');
    // }        
    ob_start();
    $f = fopen($download, 'w') or show_error("Can't open php://output");
    $n = 0;        
    foreach ($array as $line){
        $n++;
        if ( ! fputcsv($f, $line)){
            show_error("Can't write line $n: $line");
        }
    }
    fclose($f) or show_error("Can't close php://output");
    $str = ob_get_contents();
    ob_end_clean();
    }
function query_to_csv($query, $headers = TRUE, $download = "",$dealer_id){
    
if ( ! is_object($query) OR ! method_exists($query, 'list_fields')){
    show_error('invalid query');
}
$array = array();
if ($headers){
$line = array();
$customer_data_feild_name=array("Dealership Name","First Name","Last Name","Address","Apartment #","City","Province/State","Postal Code/Zip","Home Phone","Work Phone","Year","Make","Model","Purchase Date","Trade In Value");
    foreach ($customer_data_feild_name as $name){
        $line[] = $name;
    }
    $array[] = $line;
    
}
$i=1;
foreach ($query->result_array() as $row){
    $line = array();
    $p=1;
    foreach ($row as $item){
    
    if($i==14){
        
        $sql=("SELECT  contract_date 	
        FROM pbs_financial_data
        WHERE vehicle_stock = '$item' 
         ");
        
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
        $returnvalue= $query->result_array();
        foreach($returnvalue as $values){
         $purchase_date=trim($values['contract_date']);   
        }
        }
        else
        {
        $purchase_date='N/A';
        }
        $line[] = $purchase_date; 
    }
    else if($i==1){
      $sql=("SELECT  company_name 	
        FROM  registration
        WHERE registration_id = '$item' 
         ");
        
        $query=$this->db->query($sql);
        if($query -> num_rows() > 0)
        {
        $returnvalue= $query->result_array();
        foreach($returnvalue as $values){
         $dealership_name=trim(ucfirst($values['company_name'])).' (#'.$item.')';   
        }
        }
        else
        {
        $dealership_name='N/A';
        }
        $line[] = $dealership_name;   
    }
    else if($i==2){
      if($item!=''){
        $firstname=trim($item);
      }  
      else{
       $firstname='N/A'; 
      }
      $line[] = $firstname; 
    }
    else{
    $line[] = trim($item);  
    }
    $i++;
    $p++;
    }
    
    $i=1;
    $array[] = $line;
}
$this ->  array_to_csv($array, $download);;
//echo array_to_csv($array, $download);
}
public function create_pdf($event_id,$dealer_id) {
$companyname_show='';
$get_dealer_company_name=$this -> main_model -> dealercompanynameget($dealer_id);
if(isset($get_dealer_company_name) && $get_dealer_company_name!=''){
foreach($get_dealer_company_name as $value_dealer_company_name){
$companyname_get=substr(trim($value_dealer_company_name['company_name']),0,10); 
$companyname_show=strtolower(str_replace(" ","-",$companyname_get));
}   
}
$report_download_time=date('m.d.y',time());
$zipname=($companyname_show.'-eventreport-'.$report_download_time.'-'.$event_id);
$base_path = $this -> config -> item('rootpath');
$destination = $base_path.'downloadreportzip';
$base_path = $this -> config -> item('rootpath');
$this->zip->archive($base_path.'/downloadreportzip/'.$zipname.'/'.$zipname.'.zip'); 
//$path = $base_path.'/downloadreportzip/my_backup.zip';
$this->zip->get_files_from_folder($base_path.'/downloadreportzip/'.$zipname.'/',$zipname.'.zip/',$zipname);
$this->zip->download($zipname.'.zip');
    
     //============================================================+
    // END OF FILE
    //============================================================+
 }
  
}
?>