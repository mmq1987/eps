<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');

class Login extends CI_Controller {

    	public function __construct() {

		parent::__construct();

		$this -> load -> helper('url');

		$this -> load -> library('session');

		$this -> load -> helper('form');

		$this->load->model('login_model'); 

        $this->load->library("pagination");
        $this->load->model('main_model'); 
	}

	/*function to call login page */

	public function index() 
    {
	   $data['menu']=$this->login_model->loginauth();
         if (isset($data['menu']['logged_in']) == '') 
        {
        		$this -> load -> view('login-view');
        }
        else
        {
            
           
            if($data['menu']['logged_in']['usertype']=='admin')
            {
                redirect(base_url().'dashboard');
            }else{
                 redirect(base_url().'dashboard/dealershipdashbaord');
            }
        }
	}

    //login process

     public function loginprocess(){

       $emailid = $this -> input -> post('mail');

	 $password_text = $this -> input -> post('password');
        
         $encrpt_password=$this -> main_model ->ProtectData($password_text,'ENCODE'); 
         
		$result = $this -> login_model -> logincheck($emailid, $encrpt_password); 

		if ($result) {

			if (is_array($result)) {

				$this -> load -> library('session');

				//$result['profilemark'] = $this -> main_model -> get_profile_mark($result);

				$user_id = $result['registration_id'];

				$this -> session -> set_userdata('logged_in', $result);

				echo "Done";

			} else {

				print_r($result);

			}

		} else {

		  echo "Notdone";

		}

    }

  //login process

  //register

   public function register()

   {

      $data['menu']=$this->login_model->loginauth();

        if (isset($data['menu']['logged_in']) != '')

        {

    	   $this -> load -> view('register-view');

        }

       else

       {

           redirect(base_url().'login');

       }

    }

     //register

    public function registerprocess(){

       $emailid = $this -> input -> post('email_address');

		$password = $this -> input -> post('password');

		$result = $this -> login_model -> logincheck($emailid, $password); 

		if ($result) {

			if (is_array($result)) {

				$this -> load -> library('session');

				$result['profilemark'] = $this -> main_model -> get_profile_mark($result);

				$user_id = $result['user_id'];

				$this -> session -> set_userdata('logged_in', $result);

				echo "Done";

			} else {

				print_r($result);

			}

		} else {

			echo "Please check your username and password";

		}

    }

     	  function logout(){

	    $this->load->library('session');

         $this->session->unset_userdata('logged_in');

         $this->session->sess_destroy();        

      //$data['logout'] = 'You have been logged out.';

      redirect(base_url().'login');

    }

    }

    ?>