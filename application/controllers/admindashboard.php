<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admindashboard extends CI_Controller 
{
    public function __construct() 
    {
    parent::__construct();
    $this -> load -> helper('url');
    $this -> load -> library('session');
    $this -> load -> helper('form');
    $this -> load -> library('form_validation');
    $this->load->model('login_model'); 
    $this->load->model('main_model'); 
    $this->load->model('settings_model'); 
    $this->load->library("pagination");
   	}
    public function index(){
        $this->load->view('themes/header');
        $this->load->view('admin-new-dashboard');
    }
    }
    ?>