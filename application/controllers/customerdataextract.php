<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Customerdataextract extends CI_Controller 
{
    public function __construct() 
    {
    parent::__construct();
    $this -> load -> helper('url');
    $this -> load -> library('session');
    $this -> load -> helper('form');
    $this -> load -> library('form_validation');
    $this->load->model('login_model'); 
    $this->load->model('main_model'); 
    $this->load->model('settings_model'); 
    $this->load->library("pagination");
    $this->load->library('session');
    $this->load->database();
    $this->load->helper('csv');
    $this->load->helper('xml');
   	}
    public function index(){
    $sql_leadlist=("SELECT pbs_customer_data.id as customer_id, Vehicle.* FROM  Vehicle, pbs_customer_data WHERE Vehicle.year=pbs_customer_data.sold_vehicle_year AND  Vehicle.make=pbs_customer_data.sold_vehicle_make AND  Vehicle.model=pbs_customer_data.sold_vehicle_model AND Vehicle.vehicleType = 'car'");
    $query_leadlist=$this->db->query($sql_leadlist);
    if($query_leadlist -> num_rows() > 0){
    $returnvalue= $query_leadlist->result_array();
      foreach($returnvalue as $name){
      $data=array('styleId'=>$name['styleId'],
                'vehicletype'=>$name['vehicleType'],
                'vehiclesize'=>$name['vehicleSize'],
                'vehiclestyle'=>$name['vehicleStyle'],
                'vehiclecategory'=>$name['vehicleCategory'],
                'enginefueltype'=>$name['engineFuelType'],
                'drivenwheels'=>$name['drivenWheels'],
                'transmissiontype'=>$name['transmissionType'],
                'numberofdoors'=>$name['numberOfDoors'],
                'mpgcity'=>$name['mpgCity'],
                'mpghighway'=>$name['mpgHighway'],
                'mpgcombined'=>$name['mpgCombined'],
                'curbweight'=>$name['curbWeight'],
                'customer_id'=>$name['customer_id']
                );
                $this->db->insert('pbs_vehicledata_data',$data);
       
    
   } 
   }
 
}
 function insert_customer_data1(){
    $sql_leadlist=("SELECT pbs_customer_data.id as customer_id, Vehicle.* FROM  Vehicle, pbs_customer_data WHERE Vehicle.year=pbs_customer_data.sold_vehicle_year AND  Vehicle.make=pbs_customer_data.sold_vehicle_make AND  Vehicle.model=pbs_customer_data.sold_vehicle_model AND Vehicle.vehicleType = 'SUV'");
    $query_leadlist=$this->db->query($sql_leadlist);
    if($query_leadlist -> num_rows() > 0){
    $returnvalue= $query_leadlist->result_array();
      foreach($returnvalue as $name){
      $data=array('styleId'=>$name['styleId'],
                            'vehicletype'=>$name['vehicleType'],
                            'vehiclesize'=>$name['vehicleSize'],
                            'vehiclestyle'=>$name['vehicleStyle'],
                            'vehiclecategory'=>$name['vehicleCategory'],
                            'enginefueltype'=>$name['engineFuelType'],
                            'drivenwheels'=>$name['drivenWheels'],
                            'transmissiontype'=>$name['transmissionType'],
                            'numberofdoors'=>$name['numberOfDoors'],
                            'mpgcity'=>$name['mpgCity'],
                            'mpghighway'=>$name['mpgHighway'],
                            'mpgcombined'=>$name['mpgCombined'],
                            'curbweight'=>$name['curbWeight'],
                            'customer_id'=>$name['customer_id']
                            );
                $this->db->insert('pbs_vehicledata_data',$data);
       
    
   } 
   }
 
}
 function insert_customer_data2(){
    $sql_leadlist=("SELECT pbs_customer_data.id as customer_id, Vehicle.* FROM  Vehicle, pbs_customer_data WHERE Vehicle.year=pbs_customer_data.sold_vehicle_year AND  Vehicle.make=pbs_customer_data.sold_vehicle_make AND  Vehicle.model=pbs_customer_data.sold_vehicle_model AND Vehicle.vehicleType = 'Truck'");
    $query_leadlist=$this->db->query($sql_leadlist);
    if($query_leadlist -> num_rows() > 0){
    $returnvalue= $query_leadlist->result_array();
      foreach($returnvalue as $name){
      $data=array('styleId'=>$name['styleId'],
                            'vehicletype'=>$name['vehicleType'],
                            'vehiclesize'=>$name['vehicleSize'],
                            'vehiclestyle'=>$name['vehicleStyle'],
                            'vehiclecategory'=>$name['vehicleCategory'],
                            'enginefueltype'=>$name['engineFuelType'],
                            'drivenwheels'=>$name['drivenWheels'],
                            'transmissiontype'=>$name['transmissionType'],
                            'numberofdoors'=>$name['numberOfDoors'],
                            'mpgcity'=>$name['mpgCity'],
                            'mpghighway'=>$name['mpgHighway'],
                            'mpgcombined'=>$name['mpgCombined'],
                            'curbweight'=>$name['curbWeight'],
                            'customer_id'=>$name['customer_id']
                            );
                $this->db->insert('pbs_vehicledata_data',$data);
       
    
   } 
   }
   }
        function insert_customer_data3(){
    $sql_leadlist=("SELECT pbs_customer_data.id as customer_id, Vehicle.* FROM  Vehicle, pbs_customer_data WHERE Vehicle.year=pbs_customer_data.sold_vehicle_year AND  Vehicle.make=pbs_customer_data.sold_vehicle_make AND  Vehicle.model=pbs_customer_data.sold_vehicle_model AND  Vehicle.vehicleType = 'Minivan'");
    $query_leadlist=$this->db->query($sql_leadlist);
    if($query_leadlist -> num_rows() > 0){
    $returnvalue= $query_leadlist->result_array();
      foreach($returnvalue as $name){
      $data=array('styleId'=>$name['styleId'],
                            'vehicletype'=>$name['vehicleType'],
                            'vehiclesize'=>$name['vehicleSize'],
                            'vehiclestyle'=>$name['vehicleStyle'],
                            'vehiclecategory'=>$name['vehicleCategory'],
                            'enginefueltype'=>$name['engineFuelType'],
                            'drivenwheels'=>$name['drivenWheels'],
                            'transmissiontype'=>$name['transmissionType'],
                            'numberofdoors'=>$name['numberOfDoors'],
                            'mpgcity'=>$name['mpgCity'],
                            'mpghighway'=>$name['mpgHighway'],
                            'mpgcombined'=>$name['mpgCombined'],
                            'curbweight'=>$name['curbWeight'],
                            'customer_id'=>$name['customer_id']
                            );
                $this->db->insert('pbs_vehicledata_data',$data);
       
    
   } 
   }
   }
}
?>