<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
    <link rel="stylesheet" href="<?=base_url()?>js/libs/formValidator/developr.validationEngine.css?v=1">
    <!-- Button to open/hide menu -->
	<a href="#" id="open-menu"><span>Menu</span></a>
	<!-- Button to open/hide shortcuts -->
	<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>
	<!-- Main content -->
	<section role="main" id="main">
		<hgroup id="main-title" class="thin" style="text-align: left;">
			<h1>Settings</h1>
		</hgroup>
        <?php
if(isset($error)){
    echo $error;
}if(isset($success)){
?>    
    <div style="color: green;"><?php echo $success;?></div>
<?
}
?>
<style>
.select{
    width: 261px;
}
.drop-down{
  text-align: left;
    width: 256px; 
    height:76px 
}
</style>
<?php 
                         
                          
                                /*Dealer details*/
                                $selected='';
                                  $dealer_details=$this -> settings_model -> assigned_dealer_with_alldealers($user_id);
                                if(isset($dealer_details) && is_array($dealer_details))
                                {
                                    foreach($dealer_details as $value)
                                    { 
                                            ?>
                                            <?php
                                                    $dealer_details=$this -> settings_model ->managers_assigned_dealers($user_id);
                                                    if(isset($dealer_details) && $dealer_details!='')
                                                    {
                                                       if(in_array($value['registration_id'],$dealer_details))
                                                       {
                                                        $selected='selected';
                                                       }
                                                       else
                                                       {
                                                        $selected='';
                                                       }
                                                    }
                                            ?>                                
                                                                           
                                            <?php
                                    }
                                }
                            ?>
        <form method="post" action="<?php echo base_url()?>settings/setting_uploadprocess" title="Registration"  id="form-login">
        <input type="hidden" value="<?=$user_id?>" name="user_id"/>
		<div class="with-padding" style="margin-top: 15px;">
			<div class="columns">
				<div class="six-columns twelve-columns-tablet" style=" margin-left:236px">
			        <fieldset class="fieldset">
					   <legend class="legend">Add Dealer</legend> 
                       <p class="inline-small-label button-height">
						<label for="small-label-1" class="label">Dealers<br /><span style="font-size: 11px;font-weight: normal;">(Ctrl+click for multiple selection of Dealers)</span></label>
                        <select id="validation-select" name="dealers[]" class="select" multiple>
                             <option value="">Please Select</option>
                            <?php 
                          
                                /*Dealer details*/
                                $selected='';
                                  $dealer_details=$this -> settings_model -> assigned_dealer_with_alldealers($user_id);
                                if(isset($dealer_details) && is_array($dealer_details))
                                {
                                    foreach($dealer_details as $value)
                                    { 
                                            ?>
                                            <?php
                                                    $dealer_details=$this -> settings_model ->managers_assigned_dealers($user_id);
                                                    if(isset($dealer_details) && $dealer_details!='')
                                                    {
                                                       if(in_array($value['registration_id'],$dealer_details))
                                                       {
                                                        $selected='selected';
                                                       }
                                                       else
                                                       {
                                                        $selected='';
                                                       }
                                                    }
                                            ?>                                
                                                <option value="<?=$value['registration_id']?>" <?=$selected?>><?=$value['first_name'] ?></option>                                
                                            <?php
                                    }
                                }else{
                                   ?>                                
                                                <option value="" >No dealers Found</option>                                
                                            <?php
                                }
                                ?>
                                </select>
                                
                        </p>                   
                          
                        <div class="field-block button-height">
    						<button type="submit" class="button glossy mid-margin-right">
    							<span class="button-icon"><span class="icon-tick"></span></span>
    							Save
    						</button>
    						<button type="submit" class="button glossy">
    							<span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
    							Cancel
    						</button>
    					</div>
                    </fieldset> 
                    </div>
                  </div>
             </div>
        </form>
   </section>
                                        	<!-- Scripts -->
	<script src="<?=base_url()?>js/libs/jquery-1.10.2.min.js"></script>
	<script src="<?=base_url()?>js/setup.js"></script>
	<!-- Template functions -->
	<script src="<?=base_url()?>js/developr.input.js"></script>
	<script src="<?=base_url()?>js/developr.navigable.js"></script>
	<script src="<?=base_url()?>js/developr.notify.js"></script>
	<script src="<?=base_url()?>js/developr.scroll.js"></script>
	<script src="<?=base_url()?>js/developr.tooltip.js"></script>
<!-- End sidebar/drop-down menu -->
	<script src="<?=base_url()?>js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
	<script src="<?=base_url()?>js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>