<?php
class Main_model extends CI_Model {
	public function __construct(){
		$this->load->database();
	}
    /*Function to send mails*/
    public function email_send($from_email,$subject,$email,$emaildata){
        $this->load->library('email');
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $config['protocol'] = 'sendmail';
        $this->email->initialize($config);
        $this->email->from($from_email,'Exclusive Private Sale.Inc');
        $this->email->to($email);
        $this->email->subject($subject);
        $html_email = $this->load->view('email-views.php', $emaildata, true);
        $this->email->message($html_email);
        $this->email->send();
    }
   
  public function resize_image($pic,$loc,$new_name,$new_w,$new_h,$image_type)
	{
		if ($image_type == 'image/jpeg' || $image_type == 'image/jpg' || $image_type == 'image/pjpeg') {
		$im = imagecreatefromjpeg($pic);
		} elseif ($image_type == 'image/gif') {
		$im = imagecreatefromgif($pic);
		} elseif ($image_type == 'image/png' || $image_type == 'image/x-png') {
		$im = imagecreatefrompng($pic);
		}
		//$im = imagecreatefromjpeg($pic);
		$orwidth=imagesx($im); // get original width
		$orheight=imagesy($im); //get original height
		if($orwidth>$orheight)
		{
		if($orwidth>$new_w){
		$ratio = $orwidth/$orheight; // calc image ratio
		$new_h = round($new_w/$ratio,0); // calc new height keeping ratio of original
		}
		else{
		$new_w=$orwidth;
		$new_h=$orheight;
		}
		$new_image=ImageCreateTrueColor($new_w,$new_h);
		imagecopyresampled($new_image,$im,0,0,0,0,$new_w,$new_h,$orwidth,$orheight);
		imagejpeg($new_image,$loc.$new_name,100);
		imagedestroy($new_image);
		}
		else{
		if($orheight>$new_h){
		$ratio = $orheight/$orwidth; // calc image ratio
		$new_w = round($new_h/$ratio,0); // calc new height keeping ratio of original
		}
		else{
		$new_w=$orwidth;
		$new_h=$orheight;
		}
		$new_image=ImageCreateTrueColor($new_w,$new_h);
		imagecopyresampled($new_image,$im,0,0,0,0,$new_w,$new_h,$orwidth,$orheight);
		imagejpeg($new_image,$loc.$new_name,100);
		imagedestroy($new_image);
		}
    }
    public function user_data($user_id){
    	$this -> load -> helper('url');
    	$this -> db -> select('*');
    	$this -> db -> from('registration');
    	$this -> db -> where('registration_id = ' . "'" . $user_id . "'");
    	$result= $this -> db -> get();
    		if ($result -> num_rows() > 0) {
			$retrieved = $result -> result_array();
			return $retrieved;
		} else {
			return FALSE;
		}
    }
    /*Function to fetch user details*/
     public function alluserdetails(){
        $this->load->helper('url');
        $this -> db -> select('*');
        $this -> db -> from('registration');
        $this -> db -> order_by('registration_id','desc');
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	 return $retrieved;
        }
        else{
            return FALSE;
        }
    }
        public function property_delete($property_id){
        $query = $this->db->where('registration_id',$property_id);       
        $query = $this->db->delete('registration');
    }


    //Function for country selectoin
    public function CountrySelection ()
    {
    $CountryArray = array("Afghanistan"=>"Afghanistan","Albania"=>"Albania","Algeria"=>"Algeria","AmericanSamoa"=>"American Samoa","Andorra"=>"Andorra","Angola"=>"Angola","Anguilla"=>"Anguilla","Antigua"=>"Antigua","Barbuda"=>"Barbuda","Argentina"=>"Argentina","Armenia"=>"Armenia","Aruba"=>"Aruba","Australia"=>"Australia","Austria"=>"Austria","Azerbaijan"=>"Azerbaijan","Bahamas"=>"Bahamas","Bahrain"=>"Bahrain","Bangladesh"=>"Bangladesh","Barbados"=>"Barbados","Belarus"=>"Belarus","Belgium"=>"Belgium","Belize"=>"Belize","Benin"=>"Benin","Bermuda"=>"Bermuda","Bhutan"=>"Bhutan","Bolivia"=>"Bolivia","BosniaAndHerzegovina"=>"Bosnia & Herzegovina","Botswana"=>"Botswana","Brazil"=>"Brazil","BruneiDarussalam"=>"Brunei Darussalam","Bulgaria"=>"Bulgaria","BurkinaFaso"=>"Burkina Faso","Burundi"=>"Burundi","Cambodia"=>"Cambodia","Cameroon"=>"Cameroon","Canada"=>"Canada","CapeVerde"=>"Cape Verde","CaymanIslands"=>"Cayman Islands","CentralAfricanRepublic"=>"Central African Republic","Chad"=>"Chad","Chile"=>"Chile","China"=>"China","Colombia"=>"Colombia","Comoros"=>"Comoros","Congo"=>"Congo","CongoDRC"=>"Congo (DRC)","Cook"=>"Cook","Islands"=>"Islands","CostaRica"=>"Costa Rica","CotedIvoire"=>"C&ocirc;te d'Ivoire","Croatia"=>"Croatia","Cuba"=>"Cuba","Cyprus"=>"Cyprus","CzechRepublic"=>"Czech Republic","Denmark"=>"Denmark","Djibouti"=>"Djibouti","Dominica"=>"Dominica","DominicanRepublic"=>"Dominican Republic","EastTimor"=>"East Timor","Ecuador"=>"Ecuador","Egypt"=>"Egypt","ElSalvador"=>"El Salvador","EquatorialGuinea"=>"Equatorial Guinea","Eritrea"=>"Eritrea","Estonia"=>"Estonia","Ethiopia"=>"Ethiopia","FaeroeIslands"=>"Faeroe Islands","FalklandIslands"=>"Falkland Islands","Fiji"=>"Fiji","Finland"=>"Finland","France"=>"France","FrenchGuiana"=>"French Guiana","French"=>"French","Polynesia"=>"Polynesia","Gabon"=>"Gabon","Gambia"=>"Gambia","Georgia"=>"Georgia","Germany"=>"Germany","Ghana"=>"Ghana","Gibraltar"=>"Gibraltar","Greece"=>"Greece","Greenland"=>"Greenland","Grenada"=>"Grenada","Guadeloupe"=>"Guadeloupe","Guam"=>"Guam","Guatemala"=>"Guatemala","Guinea"=>"Guinea","Guinea-Bissau"=>"Guinea-Bissau","Guyana"=>"Guyana","Haiti"=>"Haiti","HolySee"=>"Holy See","Honduras"=>"Honduras","HongKong"=>"Hong Kong","Hungary"=>"Hungary","Iceland"=>"Iceland","India"=>"India","Indonesia"=>"Indonesia","Iran"=>"Iran","Iraq"=>"Iraq","Ireland"=>"Ireland","Israel"=>"Israel","Italy"=>"Italy","Jamaica"=>"Jamaica","Japan"=>"Japan","Jordan"=>"Jordan","Kazakhstan"=>"Kazakhstan","Kenya"=>"Kenya","Kiribati"=>"Kiribati","KoreaDPR"=>"Korea DPR","KoreaRepublic"=>"Korea Republic","Kuwait"=>"Kuwait","Kyrgyzstan"=>"Kyrgyzstan","Laos"=>"Laos","Latvia"=>"Latvia","Lebanon"=>"Lebanon","Lesotho"=>"Lesotho","Liberia"=>"Liberia","Libya"=>"Libya","Liechtenstein"=>"Liechtenstein","Lithuania"=>"Lithuania","Luxembourg"=>"Luxembourg","Macau"=>"Macau","Macedonia"=>"Macedonia","Madagascar"=>"Madagascar","Malawi"=>"Malawi","Malaysia"=>"Malaysia","Maldives"=>"Maldives","Mali"=>"Mali","Malta"=>"Malta","MarshallIslands"=>"Marshall Islands","Martinique"=>"Martinique","Mauritania"=>"Mauritania","Mauritius"=>"Mauritius","Mexico"=>"Mexico","Micronesia"=>"Micronesia","Moldova"=>"Moldova","Monaco"=>"Monaco","Mongolia"=>"Mongolia","Montserrat"=>"Montserrat","Morocco"=>"Morocco","Mozambique"=>"Mozambique","Myanmar"=>"Myanmar","Namibia"=>"Namibia","Nauru"=>"Nauru","Nepal"=>"Nepal","Netherlands"=>"Netherlands","NetherlandsAntilles"=>"Netherlands Antilles","NewCaledonia"=>"New Caledonia","NewZealand"=>"New Zealand","Nicaragua"=>"Nicaragua","Niger"=>"Niger","Nigeria"=>"Nigeria","Niue"=>"Niue","NorfolkIsland"=>"Norfolk Island","Northern"=>"Northern","MarianaIsland"=>"Mariana Island","Norway"=>"Norway","Oman"=>"Oman","Pakistan"=>"Pakistan","Palau"=>"Palau","PalestinianTerritory"=>"Palestinian Territory","Panama"=>"Panama","PapuaNewGuinea"=>"Papua NewGuinea","Paraguay"=>"Paraguay","Peru"=>"Peru","Philippines"=>"Philippines","Pitcairn"=>"Pitcairn","Poland"=>"Poland","Portugal"=>"Portugal","PuertoRico"=>"Puerto Rico","Qatar"=>"Qatar","R&eacute;union"=>"R&eacute;union","Romania"=>"Romania","RussianFederation"=>"Russian Federation","Rwanda"=>"Rwanda","StHelena"=>"St Helena","StKittsAndNevis"=>"St Kitts and Nevis","StLucia"=>"St Lucia","StPierreMiquelon"=>"St Pierre Miquelon","StVincentGrenadines"=>"St Vincent Grenadines","Samoa"=>"Samoa","SanMarino"=>"San Marino","SaoTomePrincipe"=>"Sao Tome Principe","SaudiArabia"=>"Saudi Arabia","Senegal"=>"Senegal","Seychelles"=>"Seychelles","SierraLeone"=>"Sierra Leone","Singapore"=>"Singapore","Slovakia"=>"Slovakia","Slovenia"=>"Slovenia","SolomonIslands"=>"Solomon Islands","Somalia"=>"Somalia","SouthAfrica"=>"South Africa","Spain"=>"Spain","SriLanka"=>"Sri Lanka","Sudan"=>"Sudan","Suriname"=>"Suriname","Swaziland"=>"Swaziland","Sweden"=>"Sweden","Switzerland"=>"Switzerland","Syria"=>"Syria","TaiwanProvinceOfChina"=>"Taiwan Province of China","Tajikistan"=>"Tajikistan","Tanzania"=>"Tanzania","Thailand"=>"Thailand","Togo"=>"Togo","Tokelau"=>"Tokelau","Tonga"=>"Tonga","TrinidadTobagoTunisia"=>"Trinidad Tobago Tunisia","Turkey"=>"Turkey","Turkmenistan"=>"Turkmenistan","TurksCaicosIslands"=>"Turks Caicos Islands","Tuvalu"=>"Tuvalu","Uganda"=>"Uganda","Ukraine"=>"Ukraine","UnitedArabEmirates"=>"United Arab Emirates","UnitedKingdom"=>"United Kingdom","USA"=>"USA","Uruguay"=>"Uruguay","Uzbekistan"=>"Uzbekistan","Vanuatu"=>"Vanuatu","Venezuela"=>"Venezuela","VietNam"=>"Viet Nam","Virgin"=>"Virgin","IslandsBritish"=>"Islands British","VirginIslands"=>"Virgin Islands","WallisFutunaIslands"=>"Wallis Futuna Islands","WesternSahara"=>"Western Sahara","Yemen"=>"Yemen","Yugoslavia"=>"Yugoslavia","Zambia"=>"Zambia","Zimbabwe"=>"Zimbabwe");
    
    return $CountryArray;
    }
    public function random_generator($digits){
        srand ((double) microtime() * 10000000);
        //Array of alphabets
        $input = array ("A", "B", "C", "D", "E","F","G","H","I","J","K","L","M","N","O","P","Q",
        "R","S","T","U","V","W","X","Y","Z");
        $random_generator="";// Initialize the string to store random numbers
        for($i=1;$i<$digits+1;$i++){ // Loop the number of times of required digits
        if(rand(1,2) == 1){// to decide the digit should be numeric or alphabet
        // Add one random alphabet
        $rand_index = array_rand($input);
        $random_generator .=$input[$rand_index]; // One char is added
        }else{
        // Add one numeric digit between 1 and 10
        $random_generator .=rand(1,10); // one number is added
        } // end of if else
        } // end of for loop
        return $random_generator;
        }
        
         public function dealerdetails($user_type,$user_id){
        $this->load->helper('url');
        $this -> db -> select('*');
        $this -> db -> from('registration');
        $this -> db -> where('registration_id = ' . "'" . $user_id . "'");
        $this -> db -> where('usertype = ' . "'" . $user_type . "'");
        $this -> db -> order_by('registration_id','desc');
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
        $retrieved=$result->result_array();
      	 return $retrieved;
        }
        else{
            return FALSE;
        }
    }
        
    }
?>