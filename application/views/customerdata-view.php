<script type="text/javascript">
function deletepoperty(property_id)
{
  if(confirm('Are you sure ?')){
                $.post('<?php echo base_url(); ?>dashboard/delete/'+property_id,function(data) {
                if(data=='Done'){
                window.location.reload('<?php echo base_url(); ?>dashboard');
                }
                });
                }
}
function select_member_type()
{

   $('#form-login').submit();
  
}
function membership_sort_hide()
{
  
    $('.sorting_asc').hide();
    $('.sorting_desc').hide();
}
</script>
<style>
.button{
    font-size: 11px;
   
}

.list > li, .list-link {
    padding: 0px 0;
    }
    .topsort{
         float: left;
    margin-top: 131px;
    position: absolute;
    top: 22px;
    width: 893px;
    z-index: 14;
    }
  #select_member{
    width:120px;
  }  
</style>
	<!-- Button to open/hide menu -->
	<a href="#" id="open-menu"><span>Menu</span></a>
	<!-- Button to open/hide shortcuts -->
	<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>
	<!-- Main content -->
	<section role="main" id="main">
		<hgroup id="main-title" class="thin" style="text-align:left;">
        
         	<h1>View Customer Data</h1>
          
		</hgroup>
          <?php
          $count='0';
         if(isset($menu['logged_in']) && $menu['logged_in']!='')
                {
                    
                    $count=count($user_details);
            ?>
		<div class="with-padding">
			<p class="wrapped left-icon icon-info-round">
			Customer Data
			</p>
              <?php
               //for selecting sort class and also check details ang get details depend on membership
           
              ?>
           
			<table class="table responsive-table" id="sorting-advanced">
				<thead>
					<tr>
						<th scope="col" style="width: 3%;" class="align-center hide-on-mobile">SI No</th>
						<th scope="col" style="width: 18%;" class="align-center hide-on-mobile"> Name</th>
						<th scope="col" width="13%" class="align-center hide-on-mobile"> Email</th>
                        <th scope="col" width="13%" class="align-center hide-on-mobile"> City</th>
						<th scope="" width="13%" class="align-center hide-on-mobile-portrait">Zip
                                      </th>
                                      
						<th scope="col" width="10%" class="lign-center hide-on-mobile-portrait">Condition</th>
						<th scope="col" width="10%" class="lign-center hide-on-mobile-portrait">Action</th>
					</tr>
				</thead>
				<tfoot>
           
					<tr>
						<td colspan="7">
							<?=$count?> entries found
						</td>
					</tr>
				</tfoot>
				<tbody>  
                <?php
                
                if(isset($user_details) && is_array($user_details))
                {
                    
                    $i=1;
              $account_manger_count=1;
              $sub_admin_det=1;
                foreach($user_details as $value){
                if($value['buyer_first_name']!='')
                {
                    $buyer_first_name=ucfirst(strtolower($value['buyer_first_name']));
                }
                else
                {
                   $buyer_first_name='N/A'; 
                }
                if($value['buyer_email']!='')
                {
                    $buyer_email=$value['buyer_email'];
                }
                else
                {
                    $buyer_email='N/A';
                }
                if($value['buyer_city']!='')
                {
                    
                
                $buyer_city=ucfirst(strtolower($value['buyer_city']));
                }
                else
                {
                   $buyer_city='N/A';     
                }
                if($value['buyer_postalcode']!='')
                {
                    $buyer_postalcode 	=$value['buyer_postalcode'];
                }
                else
                {
                    $buyer_postalcode='N/A';   
                }
                if($value['new_used']=='U')
                {
                  $new_used='Used';  
                }
                else
                {
                    $new_used='New'; 
                    
                }
                    
              ?>          
					<tr>
						<th scope="row" class="align-center hide-on-mobile" style="text-align: center;"><?=$i?></th>
                         <?php
                       
                          
                            ?>
                            <td style="width:220px"><?=$buyer_first_name?> </td>
                         
                        <td class="checkbox-cell" class="align-center hide-on-mobile" style="width:220px"><?=$buyer_email?></td>
                   
						<td class="checkbox-cell" class="align-center hide-on-mobile" ><label><?=$buyer_city?></label></td>
                    
                        <td class="align-center hide-on-mobile"><?=$buyer_postalcode;?></td>
						<td class="align-center hide-on-mobile"><?= $new_used;?></td>
                      
                        <td class="align-center hide-on-mobile" style="width: 281px;">
							<span class="button-group compact">
                          
                              	<a href="<?=base_url()?>customerdata/viewcustometdata/<?=$value['id']?>" class="button compact with-tooltip" title="View Customer Details">View Details</a>
                               
							</span>
						</td>
					</tr>
                    <?
                  
                    $i++;
                    }
                    
                    }
                    else
                    {
                    ?>
                    	<tr>
						<td colspan="8">No data found</td>
					</tr>
                    <?php
                    }
                  
                  
                    ?>
				</tbody>
			</table>
       <?php
      
            }
            ?>
		</div>
      
	</section>
	<!-- End sidebar/drop-down menu -->
	<!-- JavaScript at the bottom for fast page loading -->
	<!-- Scripts -->
	<script src="<?=base_url()?>js/libs/jquery-1.10.2.min.js"></script>
	<script src="<?=base_url()?>js/setup.js"></script>
	<!-- Template functions -->
	<script src="<?=base_url()?>js/developr.input.js"></script>
	<script src="<?=base_url()?>js/developr.navigable.js"></script>
	<script src="<?=base_url()?>js/developr.notify.js"></script>
	<script src="<?=base_url()?>js/developr.scroll.js"></script>
	<script src="<?=base_url()?>js/developr.tooltip.js"></script>
	<script src="<?=base_url()?>js/developr.table.js"></script>
	<!-- Plugins -->
	<script src="<?=base_url()?>js/libs/jquery.tablesorter.min.js"></script>
	<script src="<?=base_url()?>js/libs/DataTables/jquery.dataTables.min.js"></script>
	<script>
		// Call template init (optional, but faster if called manually)
		$.template.init();
		// Table sort - DataTables
		var table = $('#sorting-advanced');
		table.dataTable({
			'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [ 0, 5 ] }
			],
			'sPaginationType': 'full_numbers',
			'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
			'fnInitComplete': function( oSettings )
			{
				// Style length select
				table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
				tableStyled = true;
			}
            
		});
        
		// Table sort - styled
		$('#sorting-example1').tablesorter({
			headers: {
				0: { sorter: false },
				5: { sorter: false }
			}
		}).on('click', 'tbody td', function(event)
		{
			// Do not process if something else has been clicked
			if (event.target !== this)
			{
				return;
			}
			var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;
			// If click on a special row
			if (tr.hasClass('row-drop'))
			{
				return;
			}
			// If there is already a special row
			if (row.length > 0)
			{
				// Un-style row
				tr.children().removeClass('anthracite-gradient glossy');
				// Remove row
				row.remove();
				return;
			}
			// Remove existing special rows
			rows = tr.siblings('.row-drop');
			if (rows.length > 0)
			{
				// Un-style previous rows
				rows.prev().children().removeClass('anthracite-gradient glossy');
				// Remove rows
				rows.remove();
			}
			// Style row
			tr.children().addClass('anthracite-gradient glossy');
			// Add fake row
			$('<tr class="row-drop">'+
				'<td colspan="'+tr.children().length+'">'+
					'<div class="float-right">'+
						'<button type="submit" class="button glossy mid-margin-right">'+
							'<span class="button-icon"><span class="icon-mail"></span></span>'+
							'Send mail'+
						'</button>'+
						'<button type="submit" class="button glossy">'+
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>'+
							'Remove'+
						'</button>'+
					'</div>'+
					'<strong>Name:</strong> John Doe<br>'+
					'<strong>Account:</strong> admin<br>'+
					'<strong>Last connect:</strong> 05-07-2011<br>'+
					'<strong>Email:</strong> john@doe.com'+
				'</td>'+
			'</tr>').insertAfter(tr);
		}).on('sortStart', function()
		{
			var rows = $(this).find('.row-drop');
			if (rows.length > 0)
			{
				// Un-style previous rows
				rows.prev().children().removeClass('anthracite-gradient glossy');
				// Remove rows
				rows.remove();
			}
		});
		// Table sort - simple
	   
	</script>