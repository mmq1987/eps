<?php
	Class Login_model extends CI_Model
	{
	   public function __construct()
	{
		$this->load->database();
	}
    //check login
	 function logincheck($emailid,$password)
     {
        
	   $this->load->helper('url'); 
	   $this -> db -> select('*');
	   $this -> db -> from('registration');
	   $this -> db -> where('email_id = ' . "'" . $emailid . "'");
	   $this -> db -> where('password = ' . "'" . $password . "'");
       $this -> db -> limit(1);
	   $query = $this -> db -> get();
       if($query -> num_rows() == 1)
       	   {
       	    $returnvalue= $query->row_array();
                $userid=$returnvalue['registration_id'];
                //$this->update_user_logged_in_time($userid);
                return $returnvalue;
	       }
	   else
	   {
	     return false;
	   }
	 }
    //function to insert account manager details
    function managerregistration_insert($password_text){
        $emailid=$this->input->post('email');
        $this -> db -> select('registration_id');
        $this -> db -> from('registration');
        $this -> db -> where('email_id = ' . "'" . $emailid . "'");
        $this -> db -> limit(1);
        $result=$this -> db -> get();
            if($result -> num_rows() >0){
                return 0;
            }
            else{
                
                 $data=array(
                    'first_name'=>$this->input->post('first_name'),
                    'last_name'=>$this->input->post('last_name'),
                    'email_id'=>$this->input->post('email'),
                    'usertype'=>$this->input->post('membership'),
                    'contact_phone_number'=>$this->input->post('contact_phoneno'),
                    'password'=>$password_text,
                    'created_id'=>$this->input->post('created_id')
                    );
                $this->db->insert("registration", $data);
                $last_id = $this->db->insert_id();
                return $last_id;
            }
        }
     //registraion insert
   	 function registration_insert($password_text)
       {
        $emailid=$this->input->post('email');
        $this -> db -> select('registration_id');
        $this -> db -> from('registration');
        $this -> db -> where('email_id = ' . "'" . $emailid . "'");
        $this -> db -> limit(1);
        $result=$this -> db -> get();
        if($result -> num_rows() >0){
			return 0;
        }
        else{
             if($this->input->post('membership')=='auto_brand' || $this->input->post('membership')=='dealership'){
               
		      $insertfeildvalues=$this->input->post('masterbrand');
              
              $comacheck=0;
              $masterbrand='';
                if(is_array($insertfeildvalues) && $insertfeildvalues!='')
              {
               foreach ($insertfeildvalues as $values)
                { 
               if($comacheck>0)
               {
               $masterbrand.=',' ;
               }
               $masterbrand.=$values;
               $comacheck++;
               }
		      }
              }
              else{
		         $masterbrand='';
		      		          
		      }
             if($this->input->post('membership')=='dealership'){
               
                $dealer_email=$this->input->post('dealership_email');
                
             }else{
                $dealer_email='';
             }
             $country=$this->input->post('country');
             if($country=='Canada'){
          $state=$this->input->post('canadastate');  
        }else{
            $state=$this->input->post('state');
        }
            $data=array(
        		'first_name'=>$this->input->post('first_name'),
        		'last_name'=>$this->input->post('last_name'),
        		'email_id'=>$this->input->post('email'),
                'company_website'=>$this->input->post('company_website'),
        		'company_phonenumber'=>$this->input->post('company_phonenumber'),
        		'password'=>$password_text,
                'company_name'=>$this->input->post('company_name'),
        		'city'=>$this->input->post('city'),
        		'state'=>$state,
        		'country'=>$this->input->post('country'),
        		'address'=>$this->input->post('address'),
        		'zipcode'=>$this->input->post('zipcode'),
        		'contact_person'=>$this->input->post('contact_person'),
        		'usertype'=>$this->input->post('membership'),
                'contact_phone_number'=>$this->input->post('contact_phoneno'),
                'masterbrand'=>$masterbrand,
    	        'status'=>'VERIFIED',
                'created_id'=>$this->input->post('created_id'),
                'job_position'=>$this->input->post('job_position'),
                'dealership_email'=>$dealer_email,
                'data_source'=>$this->input->post('data_source')
        	);
     
        $this->db->insert("registration", $data);
            $last_id = $this->db->insert_id();
            //Create folder for user
            if($last_id!=''){
                $date=date('m-d-Y',time());
                $first_name=trim($this->input->post('first_name'));
                $foldername=($first_name.'-'.$last_id.'-'.$date);
                $base_path = $this -> config -> item('rootpath');
                $targetPath=$base_path.'/clients/'.$foldername.'/';
                $file_path=$base_path.'/clients/'.$foldername.'/';
               if(is_dir($targetPath))
                  {
                    }else{
                     mkdir($file_path, 0755);   
                    }
            }
			return $last_id;
        }
     }
     
    public function loginauth(){
       $this->load->library('session');
       return $this->session->userdata;
    }
    public function details_save($user_id)
	{
	   if($this->input->post('password')!=''){
		    	$passwordinfo='Ok';
		    }else{
		    $passwordinfo='Invalid';
		    }
            $password=$this -> main_model ->ProtectData($this->input->post('password'),'ENCODE');
            //function to check the folder exixt
            if($this->input->post('membership')=='dealership'){
                if($user_id!=''){
                    $this -> db -> select('*');
                    $this -> db -> from('registration');
                    $this -> db -> where('registration_id = ' . "'" . $user_id . "'");
                    $result=$this -> db -> get();
                    if ($result -> num_rows() > 0) {
                        $returnvalue= $result->row_array();
                        $date=strtotime($returnvalue['registratation_timestamp']);
                        $first_name=$returnvalue['first_name'];
                        $last_id=$user_id;
                        $new_firstname=$this->input->post('first_name');
                        
                        $first_name=trim($this->input->post('first_name'));
                        $foldername=($first_name.'-'.$last_id.'-'.$date);
                        $newfoldername=($new_firstname.'-'.$last_id.'-'.$date);
                        $base_path = $this -> config -> item('rootpath');
                        $targetPath=$base_path.'/clients/'.$foldername.'/';
                        $file_path=$base_path.'/clients/'.$foldername.'/';
                        $toname=$base_path.'/clients/'.$newfoldername.'/';
                        
                            if(is_dir($targetPath))
                            {
                                rename("$file_path", $toName);
                            }else{
                            
                            }
                        }
                    }
                }
		if($passwordinfo=='Ok'){
	if($this->input->post('membership')=='auto_brand' || $this->input->post('membership')=='dealership'){

		      $insertfeildvalues=$this->input->post('masterbrand');
              $comacheck=0;
              $masterbrand='';
               foreach ($insertfeildvalues as $values)
                { 
               if($comacheck>0)
               {
               $masterbrand.=',' ;
               }
               $masterbrand.=$values;
               $comacheck++;
               }
		      }else{
		         $masterbrand='';
		      		          
		      }
              if($this->input->post('membership')=='dealership'){
               
                $dealer_email=$this->input->post('dealership_email');
                
             }else{
                $dealer_email='';
             }
             if($this->input->post('country')=='Canada'){
                $state=$this->input->post('canadastate');
             }else{
              $state=$this->input->post('state');  
             }
	$data=array(
		'first_name'=>$this->input->post('first_name'),
		'last_name'=>$this->input->post('last_name'),
		'email_id'=>$this->input->post('email'),
        'company_website'=>$this->input->post('company_website'),
		'company_phonenumber'=>$this->input->post('company_phonenumber'),
		'password'=>$password,
        'company_name'=>$this->input->post('company_name'),
		'city'=>$this->input->post('city'),
		'state'=>$state,
		'country'=>$this->input->post('country'),
		'address'=>$this->input->post('address'),
		'zipcode'=>$this->input->post('zipcode'),
		'contact_person'=>$this->input->post('contact_person'),
		'masterbrand'=>$masterbrand,
        'usertype'=>$this->input->post('membership'),
        'contact_phone_number'=>$this->input->post('contact_phone_number'),
         'job_position'=>$this->input->post('job_position'),
	       'dealership_email'=>$dealer_email,
           'data_source'=>$this->input->post('data_source')
	);
    }
    else{
        if($this->input->post('membership')=='auto_brand' || $this->input->post('membership')=='dealership'){
		      $masterbrand=$this->input->post('masterbrand');
		      }else{
		         $masterbrand='';
		      		          
		      }
              if($this->input->post('membership')=='dealership'){
               
                $dealer_email=$this->input->post('dealership_email');
                
             }else{
                $dealer_email='';
             }
             if($this->input->post('country')=='Canada'){
                $state=$this->input->post('canadastate');
             }else{
              $state=$this->input->post('state');  
             }
        $data=array(
        'first_name'=>$this->input->post('first_name'),
		'last_name'=>$this->input->post('last_name'),
		'email_id'=>$this->input->post('email'),
        'company_website'=>$this->input->post('company_website'),
		'company_phonenumber'=>$this->input->post('company_phonenumber'),
	    'company_name'=>$this->input->post('company_name'),
		'city'=>$this->input->post('city'),
		'state'=>$this->input->post('state'),
		'country'=>$this->input->post('country'),
		'address'=>$this->input->post('address'),
		'zipcode'=>$this->input->post('zipcode'),
		'contact_person'=>$this->input->post('contact_person'),
		'masterbrand'=>$masterbrand,
        'usertype'=>$this->input->post('membership'),
        'contact_phone_number'=>$this->input->post('contact_phone_number'),
        'dealership_email'=>$dealer_email,
        'data_source'=>$this->input->post('data_source')
        );
    }
	$this->db->where('registration_id',$user_id);
	$this->db->update('registration',$data);
	return $user_id;
    
}
 	public function activate_user($id){
  		$this -> db -> select('status');
        $this -> db -> from('registration');
        $this -> db -> where('registration_id = ' . "'" . $id . "'");
        $this -> db -> limit(1);
        $result=$this -> db -> get();
        if ($result -> num_rows() > 0) {
				$returnvalue = $result -> result_array();
				foreach ($returnvalue as $value) {
					if($value['status']=='NOT-VERIFIED'){
						$insert_status="UPDATE registration set status='VERIFIED' where registration_id=$id";
						$result = $this -> db -> query($insert_status);
						return TRUE;
					}else{
    return TRUE;
					}
			}

			} else {

			}
  	}
    public function select_postcode($state,$city){
        $this -> db -> select('zip');
        $this -> db -> from('cities');
        $this -> db -> where('state = ' . "'" . $state . "'");
        $this -> db -> where('city = ' . "'" . $city . "'");
        $this -> db -> limit(1);
        $result=$this -> db -> get();
        if ($result -> num_rows() > 0) {
            $returnvalue= $result->row_array();
                $postcode=$returnvalue['zip'];
                return $postcode;
            }
    }
}