<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Pbsupload extends CI_Controller
 {
    public function __construct() { 
        parent::__construct();
        $this -> load -> helper('url');
        $this -> load -> library('session');
        $this -> load -> helper('form');
        $this->load->model('login_model'); 
        $this->load->library("pagination");
        $this->load->model("main_model");
        $this->load->model("upload_model");
        $this->load->model("settings_model");
    }

    public function financial_data_databese_feildname_related_upload_file($file_feild_name)
  {
    if($file_feild_name=='BodyDescription')
    {
        $return='bodydescription';
    }
   
    else if($file_feild_name=='ContractDate')
    {
        $return='contract_date';
    }
      else if($file_feild_name=='DeliveryDate')
    {
        $return='delivery_date';
    }
    
     else if($file_feild_name=='FirstPaymentDate')
    {
        $return='first_payment_date';
    }
     else if($file_feild_name=='VehiclePayoffDate')
    {
        $return='vehicle_payoff_date';
    }
      else if($file_feild_name=='VehicleSalePrice')
    {
        $return='vehiclesale_price';
    }
      else if($file_feild_name=='TotalSaleCredits')
    {
        $return='totalsale_credits';
    }
        else if($file_feild_name=='TotalCashDownAmount')
    {
        $return='total_cash_down_amount';
    }
    else if($file_feild_name=='TotalTax')
    {
        $return='total_tax';
    }
     else if($file_feild_name=='TotalFinanceAmount')
    {
        $return='total_finance_amount';
    }
     else if($file_feild_name=='TotalofPayments')
    {
        $return='total_of_payments';
    }
       else if($file_feild_name=='MonthlyPayment')
    {
        $return='monthly_payment';
    }
    
        else if($file_feild_name=='APRRate')
    {
        $return='apr';
    }
         else if($file_feild_name=='APR')
    {
        $return='apr';
    }
    
    else if($file_feild_name=='PaymentFrequency')
    {
        $return='payment_frequency';
    }
         else if($file_feild_name=='VehicleStock')
    {
        $return='vehicle_stock';
    }
   
   
    else if($file_feild_name=='ContractTerm')
    {
        $return='contract_term';
    }
    
    else {
        $return='';
    }
    return $return;
  }
  //read upload pbs csv files

 function pbs_financial_csv_file_read()
{
    
    $file_name='HIST_wellington_motors_SL.CSV';
    $root_path='/home/advantage/public_html/';
    $base_path='/home/advantage/public_html/';
    $file_path=$root_path.$file_name;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0; 
    $comacheck_updates=0;
    foreach ($csv_file_display->titles as $values_titles){ 
    $feile_insert[]=$values_titles;         
   }
   $p=1;
   
  $rowcountget=16 ;
   //read csv column    
   foreach ($csv_file_display->data as $row){
   $i=0;
    $count=0;
    $insertfeildvalues='';
    $updatefeildvalues='';
    foreach ($row as $line_of_text){  
    $values_get=$feile_insert[$i];
    $customer_data_realed_database_feild_name=$this -> financial_data_databese_feildname_related_upload_file($values_get);
    $database_values=$customer_data_realed_database_feild_name;
    if($database_values!=''){
    if($comacheck>0)
    {
    $insertfeildvalues.=', ' ;
    }
    if($comacheck_updates>0)
    {
    $updatefeildvalues.=' AND ' ;
    }
    $insertfeildvalues.=$database_values.'='."'".addslashes($line_of_text)."'";
    $comacheck++;
    $count++;
    $comacheck_updates++;
    if($rowcountget==$count){
    //$last_insertid=$this->upload_model ->insert_pbs_customer_csv_files($insertfeildvalues,174,$updatefeildvalues);
     echo $insertfeildvalues.'<br />';
     $insertfeildvalues='';
     $comacheck=0;
     $comacheck_updates=0; 
     break;
     }
     }
       //echo $insertfeildvalues;
        $p++;    
        $i++; 
        }
    
    }
 
}
function authenticom_customer_csv_file_read()
{
    $file_name='HIST_wellington_motors_SL.CSV';
    $root_path='/home/gspedia/public_html/exclusiveprivatesale/';
    $base_path='/home/gspedia/public_html/exclusiveprivatesale/';
    $file_path=$root_path.$file_name;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0;
    $comacheck_finacial=0;
    foreach ($csv_file_display->titles as $values_titles){ 
         
        $feile_insert[]=$values_titles;         
   }
    $p=1; 
    $rowcountget=count($csv_file_display->titles);     
   foreach ($csv_file_display->data as $row){
   
    $i=0;
    
    $count=0;
    $insertfeildvalues='';
    $insertfeildvalues_financial='';
    $insertfeildvalues1='';
        foreach ($row as $line_of_text){  
      
        if($i<17)
        {
             if($comacheck>0)
        {
        $insertfeildvalues.=', ' ;
        }
        $values_get=$feile_insert[$i];
        $customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_get);
        $database_values=$customer_data_realed_database_feild_name;
        $insertfeildvalues.=$database_values.'='."'".trim(addslashes($line_of_text))."'";
        if($feile_insert[$i]=='SoldVehicleStock'){
         $insertfeildvalues1.='vehicle_stock='."'".trim(addslashes($line_of_text))."',";   
        }
        
        }
        else{
        $values_get=$feile_insert[$i];
        $financial_realed_database_feild_name=$this -> financial_data_databese_feildname_related_upload_file($values_get);
       
        
        if($comacheck_finacial>17)
        {
        $insertfeildvalues_financial.=', ' ;
        }
          $insertfeildvalues_financial.=$financial_realed_database_feild_name.'='."'".trim(addslashes($line_of_text))."'";
          
        }
        $comacheck++;
        $comacheck_finacial++;
         $count++;
         if($rowcountget==$count){
             $financialdata=$insertfeildvalues1.$insertfeildvalues_financial;
         $last_insertid=$this->upload_model ->insert_pbs_customer_csv_files($insertfeildvalues,177);
        $last_pbsdatainsertid=$this->upload_model ->insert_pbs_finacial_csv_files($financialdata);
        
        
        $insertfeildvalues='';
        $insertfeildvalues_financial='';
         $comacheck=0; 
         $comacheck_finacial=0;
         break;
         }
         
         $p++;    
         $i++; 
         };
    //echo $insertfeildvalues_financial;
    }

}
            function uploadxlx(){
    
                            //it is xlsx file
                         require_once 'uploadfile/reader.php';  
                         $excel = new Spreadsheet_Excel_Reader();
                         //check whether it is customer data or not
                          $base_path = $this -> config -> item('rootpath');
                        $file_path=$base_path;
                         //$excel->read($file_path.$file.'/'.$fileread); 
                          $excel->read($file_path.'/Warranty.xls'); 
                          $x=2;
                          while($x<=$excel->sheets[0]['numRows']) 
                          { // reading row by row 
                              $insert='';
                              $comacheck=0;
                             //read excell cell
                           $y=1;
                            while($y<=$excel->sheets[0]['numCols']) 
                            {
                                if(isset($excel->sheets[0]['cells'][$x][$y]) && ($excel->sheets[0]['cells'][$x][$y])!='')
                                {
                                        
                                             if($comacheck>0)
                                            {
                                                $insert.=', ' ;
                                            }
                                            //$customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($excel->sheets[0]['cells'][1][$y]); 
                                            $insert.=$excel->sheets[0]['cells'][1][$y].'='."'".addslashes($excel->sheets[0]['cells'][$x][$y])."'";
                                            $comacheck++; 
                                        echo $excel->sheets[0]['cells'][$x][$y].'<br />';
                                       
                                }  
                                $y++;   
                            }
                          //$last_insertid=$this->upload_model ->insert_manufacure($insert);
                            $x++;
                         }
                        
                   
 
 }  
 function testcsv()
{
    $file_name='test.csv';
    $root_path='/home/advantage/pbs/';
    $base_path = $this -> config -> item('rootpath');
    $file_path=$base_path.$file_name;
    require_once $base_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0; 
    
    //print_r($csv_file_display->titles). ' --- entered';
    
   foreach ($csv_file_display->titles as $values_titles){ 
        //$customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_titles);       
        $feile_insert[]=$values_titles;         
   }
     
   //resding csv felid values as row
   $p=1;      
   foreach ($csv_file_display->data as $row){
   //resding csv felid values as column 
   
    $i=0;
    $rowcountget=5;
    $count=0;
    $insertfeildvalues='';
        foreach ($row as $line_of_text){  
        $values_get=$feile_insert[$i];
        
       // $customer_data_realed_database_feild_name=$this -> customer_data_databese_feildname_related_upload_file($values_get);
        $database_values=$feile_insert[$i];
        
    if($database_values!=''){
    //echo 'values - '.$database_values.'<br />';
    if($comacheck>0)
    {
    $insertfeildvalues.=', ' ;
    }
    //resding databese chsnge feild values and csv orginal feild values 
     //ckeck feild change null
    $insertfeildvalues.=$database_values.'='."'".trim(addslashes($line_of_text))."'";
    
     //call insert function
     $comacheck++;
     $count++;
     if($rowcountget==$count){
     $last_insertid=$this->upload_model ->insert_manufacure($insertfeildvalues);
     
     $insertfeildvalues='';
     $comacheck=0; 
     break;
     }
     }
       //echo $insertfeildvalues;
        $p++;    
        $i++; 
        }
    
    }
    // echo $p;
    //return $last_insertid; 
} 
   public function testfeildnames()
  { 
    $customer_data_feild_name=array("Buyer Last Name","Buyer First Name","Buyer Address","Buyer Appartment #","Buyer Appartment","Buyer City","Buyer Province/State","Buyer Postal Code/Zip","Buyer Email","Buyer Home Phone","Buyer Business Phone","Buyer Cell Phone","Sold Vehicle Stock","Sold Vehicle VIN","Shortened VIN","New or Used","Sold Vehicle Year","Sold Vehicle Make","Sold Vehicle Model","BodyDescription","ContractDate","FirstPaymentDate","vehicleType","L/100kmCombined","TradeinValue","TotalCashDownAmount","TotalTax","transmissionType","TotalofPayments","MonthlyPayment","ContractTerm","APR");
    return $customer_data_feild_name;
  }
  public function testfeildnames_feildname_related_upload_file($file_feild_name)
  {
    
       if($file_feild_name=='Buyer First Name')
    {
        $return='buyer_first_name';
    }
    else if($file_feild_name=='Buyer Last Name'){
       $return='buyer_last_name'; 
    }
    else if($file_feild_name=='Buyer Address'){
       $return='buyer_address'; 
    }
    else if($file_feild_name=='Buyer Appartment #'){
       $return='buyer_appartment'; 
    }
    else if($file_feild_name=='Buyer City'){
       $return='buyer_city'; 
    }
    else if($file_feild_name=='Buyer Province/State'){
       $return='buyer_province'; 
    }
    else if($file_feild_name=='Buyer Postal Code/Zip'){
       $return='buyer_postalcode'; 
    }
     else if($file_feild_name=='vehicleType')
    {
        $return='vehicletype';
    }
     else if($file_feild_name=='BuyerZip')
    {
        $return='buyer_postalcode';
    }
     else if($file_feild_name=='Buyer Email')
    {
        $return='buyer_email';
    }
     else if($file_feild_name=='BuyerEmail')
    {
        $return='buyer_email';
    }
     else if($file_feild_name=='Buyer Home Phone')
    {
        $return='buyer_homephone';
    }
      else if($file_feild_name=='BuyerHomePhone')
    {
        $return='buyer_homephone';
    }
       else if($file_feild_name=='Buyer Business Phone')
    {
        $return='buyer_businessphone';
    }
         else if($file_feild_name=='BuyerBusinessPhone')
    {
        $return='buyer_businessphone';
    }
       else if($file_feild_name=='Buyer Cell Phone')
    {
        $return='buyer_cellphone';
    }
         else if($file_feild_name=='BuyerCellPhone')
    {
        $return='buyer_cellphone';
    }
        else if($file_feild_name=='Sold Vehicle Stock')
    {
        $return='sold_vehicle_stock';
    }
        else if($file_feild_name=='SoldVehicleStock')
    {
        $return='sold_vehicle_stock';
    }
        else if($file_feild_name=='Sold Vehicle VIN')
    {
        $return='sold_vehicle_VIN';
    }
         else if($file_feild_name=='Sold Vehicle VIN')
    {
        $return='sold_vehicle_VIN';
    }
    else if($file_feild_name=='Shortened VIN')
    {
        $return='shortened_VIN';
    }
     else if($file_feild_name=='ShortenedVIN')
    {
        $return='shortened_VIN';
    }
          else if($file_feild_name=='New or Used')
    {
        $return='new_used';
    }
            else if($file_feild_name=='NeworUsed')
    {
        $return='new_used';
    }
    else if($file_feild_name=='Sold Vehicle Year')
    {
        $return='sold_vehicle_year';
    }
     else if($file_feild_name=='SoldVehicleYear')
    {
        $return='sold_vehicle_year';
    }
    else if($file_feild_name=='Sold Vehicle Make')
    {
        $return='sold_vehicle_make';
    }
    else if($file_feild_name=='SoldVehicleMake')
    {
        $return='sold_vehicle_make';
    }
    else if($file_feild_name=='Sold Vehicle Model')
    {
        $return='sold_vehicle_model';
    }
      else if($file_feild_name=='SoldVehicleModel')
    {
        $return='sold_vehicle_model';
    }
    else if($file_feild_name=='BodyDescription')
    {
        $return='bodydescription';
    }
    else if($file_feild_name=='FirstPaymentDate')
    {
        $return='first_payment_date';
    }
     else if($file_feild_name=='transmissionType')
    {
        $return='transmissiontype';
    }
      else if($file_feild_name=='L/100kmCombined')
    {
        $return='littercombined';
    }
      else if($file_feild_name=='ContractDate')
    {
        $return='contractdate';
    }
        else if($file_feild_name=='TradeinValue')
    {
        $return='tradeinvalue';
    }
    else if($file_feild_name=='FirstPaymentDate')
    {
        $return='1stPayment';
    }
     else if($file_feild_name=='APR')
    {
        $return='APR';
    }
     else if($file_feild_name=='MonthlyPayment')
    {
        $return='monthlypayment';
    }
     
       else if($file_feild_name=='ContractTerm')
    {
        $return='contract_term';
    }
    
    else {
        $return='';
    }
    return $return;
  } 
  function testbuyerfile($field){
    //echo $field;
    if($field=='Buyer First Name')
    {
        $feild_vaue='buyer_first_name';
    }
    else if($field=='Buyer Last Name'){
       $feild_vaue='buyer_last_name'; 
    }
    else if($field=='Buyer Address'){
       $feild_vaue='buyer_address'; 
    }
    else if($field=='Buyer Appartment #'){
       $feild_vaue='buyer_appartment'; 
    }
    else if($field=='Buyer City'){
       $feild_vaue='buyer_city'; 
    }
    else if($field=='Buyer Province/State'){
       $feild_vaue='buyer_province'; 
    }
    else if($field=='Buyer Postal Code/Zip'){
       $feild_vaue='buyer_postalcode'; 
    }
  
    return $feild_vaue;
  }             
function testdataepsupload(){
    $base_path = $this -> config -> item('rootpath');
     require_once 'uploadfile/reader.php';  
     $excel = new Spreadsheet_Excel_Reader();
     $excel->read('/home/advantage/public_html/test-upload.xls'); 
     $x=2;
     while($x<=$excel->sheets[0]['numRows']){ // reading row by row 
     $insert='';
     $comacheck=0;
     $y=1;
     while($y<=$excel->sheets[0]['numCols']){
         if(isset($excel->sheets[0]['cells'][$x][$y]) && ($excel->sheets[0]['cells'][$x][$y])!='') {
             $customer_data_feild_name=$this -> testfeildnames(); 
             //check the feild existsnce 
             
             if(in_array($excel->sheets[0]['cells'][1][$y],$customer_data_feild_name)){
            //echo ($excel->sheets[0]['cells'][1][$y]).'<br />';
                $customer_data_realed_database_feild_name=$this -> testfeildnames_feildname_related_upload_file($excel->sheets[0]['cells'][1][$y]);
                echo $customer_data_realed_database_feild_name;
                if($customer_data_realed_database_feild_name!='')
                {
                if($comacheck>0){
                $insert.=', ' ;
             }
                $insert.=$customer_data_realed_database_feild_name.'='."'".addslashes($excel->sheets[0]['cells'][$x][$y])."'";
             $comacheck++; 
             }
             }
         }  
         $y++;  
        // echo  $insert;
     }
      $last_insertid=$this->upload_model ->insert_pbstest_data($insert);
    $x++;
}
}
function upload_engin_data(){
    $sql=("SELECT styleId from pbs_vehicledata_data
            ");
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
                $returnvalue= $query->result_array();
                foreach($returnvalue as $value){
                $sql1=("SELECT styleId , horsepower,torque ,cylinder ,size 
            FROM Engine
            WHERE styleId = $value[styleId]");
            $query1=$this->db->query($sql1);
            if($query1 -> num_rows() > 0)
            {
                 $returnvalue1= $query1->result_array();
                  foreach($returnvalue1 as $value1){
                    $data=array(
                'styleId'=>$value1['styleId'],
                'horsepower'=>$value1['horsepower'],
                'torque'=>$value1['torque'],
                'cylinder'=>$value1['cylinder'],
                'size'=>$value1['size']              
           );
        $this->db->insert("pbs_engine_data", $data);  
        }
                 
              }
              }
}
}
function updatelittercombined(){
 $sql=("SELECT id,mpgcombined from pbs_vehicledata_data
            ");
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
                $price='253.214584327527';
                $returnvalue= $query->result_array();
                foreach($returnvalue as $value){ 
                if($value['mpgcombined']!=''){
                $vehicle_combined=$value['mpgcombined']; 
                $result_get=$price/$vehicle_combined;
                $result_save=round($result_get, 1);
                $updatestatus_status="UPDATE pbs_vehicledata_data set 
                littercombined='$result_save'
                where  	id=$value[id]";
                $result = $this -> db -> query($updatestatus_status);   
            }
            }
            }
}
function updatepowerration(){
    $sql=("SELECT styleId,curbweight from pbs_vehicledata_data 
            ");
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
                $returnvalue= $query->result_array();
                foreach($returnvalue as $value){
                    $curb=$value['curbweight'];
                $sql1=("SELECT horsepower
            FROM pbs_engine_data
            WHERE styleId = $value[styleId]");
            $query1=$this->db->query($sql1);
            if($query1 -> num_rows() > 0)
            {
                 $returnvalue1= $query1->result_array();
                  foreach($returnvalue1 as $value1){
                    $horse=$value1['horsepower'];
                  $result_get= ($curb/$horse); 
                  $result_save=round($result_get, 2); 
                  $updatestatus_status="UPDATE pbs_engine_data set 
                 	powerratio='$result_save'
                    where  	styleId=$value[styleId]";
                $result = $this -> db -> query($updatestatus_status);     
        }
                 
              }
              }
}
}
function test(){
 $sql1=("SELECT id
            FROM pbs_engine_data
            ");
            $query1=$this->db->query($sql1);
            if($query1 -> num_rows() > 0)
            {
                 $returnvalue1= $query1->result_array();
                  foreach($returnvalue1 as $value1){
                  $result_get= ($value['curbweight']/$value1['horsepower']); 
                  $result_save=round($result_get, 2); 
                  $updatestatus_status="UPDATE pbs_engine_data set 
                 	powerratio=''
                    where  	id=$value1[id]";
                $result = $this -> db -> query($updatestatus_status);     
        }   
}
}

 function pbs_customer_dummydata()
{
    $file_name='pbs_vehicledata_data.csv';
    $root_path=$this -> config -> item('rootpath');
    $file_path=$root_path.$file_name;
    require_once $root_path.'uploadfile/parsecsv.lib.php';
    $csv_file_display = new parseCSV();
    $csv_file_display->auto($file_path);
    $comacheck=0; 
    
    //print_r($csv_file_display->titles). ' --- entered';
    
   foreach ($csv_file_display->titles as $values_titles){ 
            
        $feile_insert[]=$values_titles;         
   }
       
   foreach ($csv_file_display->data as $row){
   $i=0;
   $rowcountget=count($feile_insert);
   $count=0;
    $insertfeildvalues='';
        foreach ($row as $line_of_text){  
        $values_get=$feile_insert[$i];
    
    if($comacheck>0)
    {
    $insertfeildvalues.=', ' ;
    }
   $insertfeildvalues.=$feile_insert[$i].'='."'".addslashes($line_of_text)."'";
     //call insert function
     $comacheck++;
     $count++;
     if($rowcountget==$count){
     $last_insertid=$this->upload_model ->insert_pbs_customer_dummyfile($insertfeildvalues);
     $insertfeildvalues='';
     $comacheck=0; 
     break;
     }
     
       //echo $insertfeildvalues;
           
        $i++; 
        }
    
    }
    // echo $p;
    //return $last_insertid; 
}
function upload_dummy_customer_data(){
    $sql=("SELECT  sold_vehicle_stock from eps_data
            ");
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
                $returnvalue= $query->result_array();
                foreach($returnvalue as $value){
                $sql1=("SELECT * 
            FROM  	pbs_financial_data
            WHERE vehicle_stock = '$value[sold_vehicle_stock]'");
            $query1=$this->db->query($sql1);
            if($query1 -> num_rows() > 0)
            {
                 $returnvalue1= $query1->result_array();
                  foreach($returnvalue1 as $value1){
                   $buyer_first_name=addslashes($value1['bodydescription']);
                $buyer_last_name=addslashes($value1['contract_date']);
                $buyer_address=addslashes($value1['first_payment_date']);
                $buyer_appartment=addslashes($value1['delivery_date']);
                $buyer_city=addslashes($value1['vehicle_payoff_date']);
                $buyer_province=addslashes($value1['vehiclesale_price']);
                $buyer_postalcode=addslashes($value1['total_finance_amount']);
                $buyer_homephone=addslashes($value1['total_of_payments']);
                $buyer_businessphone=addslashes($value1['monthly_payment']);
                $buyer_cellphone=addslashes($value1['contract_term']);
                $buyer_email=addslashes($value1['apr']);
                $payment_frequency=addslashes($value1['payment_frequency']);
                
                 
                  $updatestatus_status="UPDATE eps_data set 
                bodydescription='$buyer_first_name',
                contract_date='$buyer_last_name',
                 first_payment_date='$buyer_address',
                delivery_date='$buyer_appartment',
                vehicle_payoff_date='$buyer_city',
                vehiclesale_price='$buyer_province',
                total_finance_amount='$buyer_postalcode',
                total_of_payments='$buyer_homephone',
                contract_term='$buyer_cellphone',
                payment_frequency='$payment_frequency',
                monthly_payment='$buyer_businessphone',
                apr='$buyer_email'
                where  sold_vehicle_stock='$value1[vehicle_stock]'";
                $result = $this -> db -> query($updatestatus_status); 
                  
        }
                 
              }
              }
}
}
function upload_dummy_powerratio(){
    $sql=("SELECT styleid from eps_data
            ");
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
                $returnvalue= $query->result_array();
                foreach($returnvalue as $value){
                $sql1=("SELECT powerratio
            FROM  	pbs_engine_data
            WHERE styleId = $value[styleid]");
            $query1=$this->db->query($sql1);
            if($query1 -> num_rows() > 0)
            {
                 $returnvalue1= $query1->result_array();
                foreach($returnvalue1 as $value1){
                    $updatestatus_status="UPDATE eps_data set 
                powerratio='$value1[powerratio]'
                where styleid=$value[styleid]
                ";
                  $result = $this -> db -> query($updatestatus_status); 
                    }
}
}
}
}
function insertmastertable()
{
    $query_master_vehicle=("select  
            pbs_financial_data.bodydescription,
            pbs_customer_data.sold_vehicle_VIN,
            pbs_customer_data.sold_vehicle_year,
            pbs_customer_data.sold_vehicle_make,
            pbs_customer_data.sold_vehicle_model 
            FROM pbs_financial_data ,pbs_customer_data 
            where pbs_financial_data .vehicle_stock=pbs_customer_data.sold_vehicle_stock 
            GROUP BY pbs_customer_data.sold_vehicle_year,
            pbs_customer_data.sold_vehicle_make,
            pbs_customer_data.sold_vehicle_model order by pbs_customer_data.id 
           	");
        $result_master_vehicle=$this->db->query($query_master_vehicle);
        if($result_master_vehicle -> num_rows() > 0)
        {
          
            $returnvalue_master_data=$result_master_vehicle->result_array();
            foreach($returnvalue_master_data as $customerdata) {
            $bodydescription=trim(addslashes($customerdata['bodydescription']));
            $sold_vehicle_make=trim(addslashes($customerdata['sold_vehicle_make']));
            $sold_vehicle_model=trim(addslashes($customerdata['sold_vehicle_model']));
            $sold_vehicle_year=trim($customerdata['sold_vehicle_year']);
            $sold_vehicle_VIN=trim($customerdata['sold_vehicle_VIN']);
            $quantity=$this->settings_model ->getcountofmakemodel($sold_vehicle_year,$sold_vehicle_make,$sold_vehicle_model);
            $query_master_vehicle_exsist=("select  * from eps_master_vehicle where 
            quantity=$quantity and 
            vin='$sold_vehicle_VIN' and
            sold_vehicle_year='$sold_vehicle_year' and
            sold_vehicle_make='$sold_vehicle_make' and
            sold_vehicle_model='$sold_vehicle_model' and
            bodydescription='$bodydescription'
           	");
            $result_query_master_vehicle_exsist=$this->db->query($query_master_vehicle_exsist);
            if($result_query_master_vehicle_exsist -> num_rows() > 0){
            
            }
            else{
             $insert_master_table=("insert into  eps_master_vehicle set quantity=$quantity,
             vin='$sold_vehicle_VIN',
             sold_vehicle_year='$sold_vehicle_year',
             sold_vehicle_make='$sold_vehicle_make',
             sold_vehicle_model='$sold_vehicle_model',
             bodydescription='$bodydescription'
             ");
             $insert_master=$this->db->query($insert_master_table);   
            }
            }
        }
}

function uploadtestepsdata(){
 $sql=("SELECT * from test_eps_data
            ");
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
            $returnvalue= $query->result_array();
            foreach($returnvalue as $value){
                $buyer_first_name=addslashes($value['buyer_first_name']);
                $buyer_last_name=addslashes($value['buyer_last_name']);
                $buyer_address=addslashes($value['buyer_address']);
                $buyer_appartment=addslashes($value['buyer_appartment']);
                $buyer_city=addslashes($value['buyer_city']);
                $buyer_province=addslashes($value['buyer_province']);
                $buyer_postalcode=addslashes($value['buyer_postalcode']);
                $buyer_homephone=addslashes($value['buyer_homephone']);
                $buyer_businessphone=addslashes($value['buyer_businessphone']);
                $buyer_cellphone=addslashes($value['buyer_cellphone']);
                $buyer_email=addslashes($value['buyer_email']);
                $sold_vehicle_stock=addslashes($value['sold_vehicle_stock']);
                $sold_vehicle_VIN=addslashes($value['sold_vehicle_VIN']);
                $shortened_VIN=addslashes($value['shortened_VIN']);
                $new_used=addslashes($value['new_used']);
                $sold_vehicle_year=addslashes($value['sold_vehicle_year']);
                $sold_vehicle_make=addslashes($value['sold_vehicle_make']);
                $sold_vehicle_model=addslashes($value['sold_vehicle_model']);
                $monthlypayment=addslashes($value['monthlypayment']);
                $littercombined=addslashes($value['littercombined']);
                $vehicletype=addslashes($value['vehicletype']);
                $bodydescription=addslashes($value['bodydescription']);
                $APR=addslashes($value['APR']);
                $first_payment_date=addslashes($value['first_payment_date']);
                $contractdate=addslashes($value['contractdate']);
                $transmissiontype=addslashes($value['transmissiontype']);
                $tradeinvalue=addslashes($value['tradeinvalue']);
                $numberOfdoors=addslashes($value['numberOfdoors']);
                $contract_term=addslashes($value['contract_term']);
                $insert_master_table=("insert into  eps_data set dealership_id=198,
                buyer_first_name='$buyer_first_name',
                buyer_last_name='$buyer_last_name',
                buyer_address='$buyer_address',
                buyer_appartment='$buyer_appartment',
                buyer_city='$buyer_city',
                buyer_province='$buyer_province',
                buyer_postalcode='$buyer_postalcode',
                buyer_homephone='$buyer_homephone',
                buyer_businessphone='$buyer_businessphone',
                buyer_cellphone='$buyer_cellphone',
                buyer_email='$buyer_email',
                sold_vehicle_stock='$sold_vehicle_stock',
                sold_vehicle_VIN='$sold_vehicle_VIN',
                shortened_VIN='$shortened_VIN',
                new_used='$new_used',
                sold_vehicle_year='$sold_vehicle_year',
                sold_vehicle_make='$sold_vehicle_make',
                sold_vehicle_model='$sold_vehicle_model',
                vehicletype='$vehicletype',
                bodydescription='$bodydescription',
                contract_date='$contractdate',
                first_payment_date='$first_payment_date',
                apr='$APR',
                transmissiontype='$transmissiontype',
                 	tradeinvalue='$tradeinvalue',
                contract_term='$contract_term',
                monthly_payment='$monthlypayment',
                littercombined='$littercombined'
                
             ");
             $insert_master=$this->db->query($insert_master_table);  
                
               
}
}
}


function uploadepsdummytabledata(){
 $sql=("SELECT * from  eps_data where dealership_id='198' order by id  limit 0,1000
            ");
            $query=$this->db->query($sql);
            if($query -> num_rows() > 0)
            {
            $returnvalue= $query->result_array();
            foreach($returnvalue as $value){
                $dealership_id=trim($value['dealership_id']);
                $buyer_first_name=trim(addslashes($value['buyer_first_name']));
                $buyer_last_name=addslashes($value['buyer_last_name']);
                $buyer_address=trim(addslashes($value['buyer_address']));
                $buyer_appartment=trim(addslashes($value['buyer_appartment']));
                $buyer_city=trim(addslashes($value['buyer_city']));
                $buyer_province=trim(addslashes($value['buyer_province']));
                $buyer_postalcode=trim(addslashes($value['buyer_postalcode']));
                $buyer_homephone=trim(addslashes($value['buyer_homephone']));
                $buyer_businessphone=trim(addslashes($value['buyer_businessphone']));
                $buyer_cellphone=trim(addslashes($value['buyer_cellphone']));
                $buyer_email=trim(addslashes($value['buyer_email']));
                $sold_vehicle_stock=trim(addslashes($value['sold_vehicle_stock']));
                $sold_vehicle_VIN=trim(addslashes($value['sold_vehicle_VIN']));
                $shortened_VIN=trim(addslashes($value['shortened_VIN']));
                $new_used=trim(addslashes($value['new_used']));
                $sold_vehicle_year=trim(addslashes($value['sold_vehicle_year']));
                $sold_vehicle_make=trim(addslashes($value['sold_vehicle_make']));
                $sold_vehicle_model=trim(addslashes($value['sold_vehicle_model']));
                $monthlypayment=trim(addslashes($value['monthly_payment']));
                $littercombined=trim(addslashes($value['littercombined']));
                $vehicletype=trim(addslashes($value['vehicletype']));
                $bodydescription=trim(addslashes($value['bodydescription']));
                $APR=trim(addslashes($value['apr']));
                $first_payment_date=trim(addslashes($value['first_payment_date']));
                $contractdate=trim(addslashes($value['contract_date']));
                $transmissiontype=trim(addslashes($value['transmissiontype']));
                $tradeinvalue=trim(addslashes($value['tradeinvalue']));
                $contract_term=trim(addslashes($value['contract_term']));
                $vehiclesize=trim(addslashes($value['vehiclesize']));
                $vehiclestyle=trim(addslashes($value['vehiclestyle']));
                $vehiclecategory=trim(addslashes($value['vehiclecategory']));
                $enginefueltype=trim(addslashes($value['enginefueltype']));
                $drivenwheels=trim(addslashes($value['drivenwheels']));
                $fuel_efficiency=trim(addslashes($value['fuel_efficiency']));
                $powerratio=trim(addslashes($value['powerratio']));
                $delivery_date=trim(addslashes($value['delivery_date']));
                $vehicle_payoff_date=trim(addslashes($value['vehicle_payoff_date']));
                $vehiclesale_price=trim(addslashes($value['vehiclesale_price']));
                $total_finance_amount=trim(addslashes($value['total_finance_amount']));
                $total_of_payments=addslashes($value['total_of_payments']);
                $payment_frequency=addslashes($value['payment_frequency']);
                $customer_id=addslashes($value['customer_id']);
                $styleid=addslashes($value['styleid']);
                $vehicle=addslashes($value['vehicle']);
                $sql_eps=("SELECT * from  eps_data_dummy where  
                buyer_first_name='$buyer_first_name' and 
                buyer_last_name='$buyer_last_name'and
                buyer_address='$buyer_address'and
                sold_vehicle_year='$sold_vehicle_year'and
                sold_vehicle_make='$sold_vehicle_make'and
                sold_vehicle_model='$sold_vehicle_model'
              ");
                $query_eps=$this->db->query($sql_eps);
                if($query_eps -> num_rows() > 0)
                {
                }
                else{
                $insert_master_table=("insert into  eps_data_dummy set dealership_id=$dealership_id,
                buyer_first_name='$buyer_first_name',
                buyer_last_name='$buyer_last_name',
                buyer_address='$buyer_address',
                buyer_appartment='$buyer_appartment',
                buyer_city='$buyer_city',
                buyer_province='$buyer_province',
                buyer_postalcode='$buyer_postalcode',
                buyer_homephone='$buyer_homephone',
                buyer_businessphone='$buyer_businessphone',
                buyer_cellphone='$buyer_cellphone',
                buyer_email='$buyer_email',
                sold_vehicle_stock='$sold_vehicle_stock',
                sold_vehicle_VIN='$sold_vehicle_VIN',
                shortened_VIN='$shortened_VIN',
                new_used='$new_used',
                sold_vehicle_year='$sold_vehicle_year',
                sold_vehicle_make='$sold_vehicle_make',
                sold_vehicle_model='$sold_vehicle_model',
                vehicletype='$vehicletype',
                bodydescription='$bodydescription',
                contract_date='$contractdate',
                first_payment_date='$first_payment_date',
                apr='$APR',
                transmissiontype='$transmissiontype',
               	tradeinvalue='$tradeinvalue',
                contract_term='$contract_term',
                vehiclesize='$vehiclesize',
                monthly_payment='$monthlypayment',
                vehiclestyle='$vehiclestyle',
                vehiclecategory='$vehiclecategory',
                enginefueltype='$enginefueltype',
                drivenwheels='$drivenwheels',
                fuel_efficiency='$fuel_efficiency',
                powerratio='$powerratio',
                delivery_date='$delivery_date',
                vehicle_payoff_date='$vehicle_payoff_date',
                vehiclesale_price='$vehiclesale_price',
                total_finance_amount='$total_finance_amount',
                total_of_payments='$total_of_payments',
                payment_frequency='$payment_frequency',
                customer_id='$customer_id',
                styleid='$styleid',
                vehicle='$vehicle',
                littercombined='$littercombined'
                
             ");
              $insert_master=$this->db->query($insert_master_table); 
              if($insert_master_table){
                echo "insert";
              }
             }
             
                
               
}
}
}
}
?>